
"""
Created on Wed Sep 11 12:41:09 2013

@author: kkonan
"""

import urllib2
import urllib
import re
import os
import sys
import json



from TTServer import  currentMessage

Test_platform = "PDT"

LOCAL_MODE = True

####    Local storage without spear

def getPnrObject(filePath="../etc/Saved_PNRs/pnr_local.json"):
    """

    returns a dictionary of PNR objects, with key as scenario id (case insensitive) 
    """ 
    with open(filePath) as fp:
        return json.load(fp)

def writePnrObject(data, filePath="../etc/Saved_PNRs/pnr_local.json"):
    """
    writes a dictionary of PNR objects, with key as scenario id (case insensitive) 
    """ 
    with open(filePath, 'w') as fp:
        json.dump(data, fp)
    



"""
Save a PNR image in a the Rail PNR RLOC database
"""

def savePnrDecorated(func):
    def helper(**kwargs):
        if not LOCAL_MODE:
            func(**kwargs)
        else:
            print "Local version called", func.__name__
            pnrObject = getPnrObject()
            print type(pnrObject)
            pnrObject[kwargs['scenario_id'].encode('utf-8')] = {
                'rloc': kwargs['rloc'].encode('utf-8'),
                'from_phase': kwargs['from_phase'].encode('utf-8')
                }

            print pnrObject
            writePnrObject(pnrObject)
            return currentMessage.TTS_MATCH_COMPARISON_OK


    
    return helper



def consumePnrDecorated(func):
    def helper(*args, **kwargs):
        if not LOCAL_MODE:
            return func(*args, **kwargs)
        else:
            print "Local version called", func.__name__
            pnrObject = getPnrObject()

            # check if passed with or without keyword args
            if len(args) == 0:
                key = kwargs['scenario_id']
            else:
                key = args[0]

            return pnrObject[key]['rloc'].encode('utf-8')
    
    return helper


def getOutwardDatetimeDecorated():
    pass



def getInwardDatetimeDecorated():
    pass


def extractDatetimeDecorated():
    pass








@savePnrDecorated
def savePnr(scenario_id,rloc,from_phase,savepnr,user_id="TEST",comment="NA",datein="NA",dateout="NA"):
    try:
        
        if savepnr.upper() == "TRUE":
            appurl = 'http://172.16.137.237:8088/railpnrapp/savepnr'
        
            values = {}
        
            values['scenario_id'] = scenario_id
            values['from_phase'] = from_phase
            #values['to_phase'] = to_phase
            values['user_id'] = user_id
            values['rloc'] = rloc
            values['comment'] = comment
            
            if dateout <> "NA":
                values['datetimeout'] = dateout
            
            if datein <> "NA":
                values['datetimein'] = datein
        
            input_data = urllib.urlencode(values)
            req = urllib2.Request(appurl, input_data)
            response = urllib2.urlopen(req).read()
        
    except Exception:
        pass
        
    return currentMessage.TTS_MATCH_COMPARISON_OK   
     
@consumePnrDecorated        
def consumePNR(scenario_id,phase,user_id="TEST"):
    print getPnrObject()

    try:
        appurl = 'http://172.16.137.237:8088/railpnrapp/consumepnr'
        values = {}
        
        values['scenario_id'] = scenario_id
        values['phase'] = phase
        values['user_id'] = user_id
        
        input_data = urllib.urlencode(values)
        req = urllib2.Request(appurl, input_data)
        response = urllib2.urlopen(req).read()
        return response
    except Exception:
        pass

def get_outward_datetime(scenario_id, from_phase, user_id="TEST"):
    try:
        appurl = 'http://172.16.137.237:8088/railpnrapp/getdatetimeout'
        values = {}
        values['scenario_id'] = scenario_id
        values['from_phase'] = from_phase
        values['user_id'] = user_id
        
        input_data = urllib.urlencode(values)
        
        req = urllib2.Request(appurl, input_data)
        
        response = urllib2.urlopen(req).read()
        return response
    except Exception:
        pass
    
def extract_date_time(datetime):
    try:
        if datetime <> None and datetime <> "NA":
            date, time = datetime.split("T")
            return (date.replace('-',''),time.replace(':','')[:4])
    except Exception:
        pass
  
def get_inward_datetime(scenario_id, from_phase, user_id="TEST"):
    try:
        appurl = 'http://172.16.137.237:8088/railpnrapp/getdatetimein'
        values = {}
        values['scenario_id'] = scenario_id
        values['from_phase'] = from_phase
        values['user_id'] = user_id
        
        input_data = urllib.urlencode(values)
        
        req = urllib2.Request(appurl, input_data)
        
        response = urllib2.urlopen(req).read()
        return response
    except Exception:
        pass
    
def injectPnr_from_file( basedir, filename, phase, user_id="TEST"):
    try:
   
        appurl = 'http://172.16.137.237:8088/railpnrapp/injectpnr'
        values = {}
        
        
        values['to_phase'] = phase
        values['user_id'] = user_id
        print os.getcwd()
        
        filepath = os.path.join(basedir, filename)
        print filepath
        if os.path.isfile(filepath):
            f = open(filepath,'r')
            edi_message = f.read()
            f.close()
            values['edi_message'] = edi_message
            regex = "RCI\+\:(\w{6})"
            res = re.search(regex,edi_message)
            if res:
                rloc = res.group(1)
                values['rloc'] = rloc
                data = urllib.urlencode(values)
                req = urllib2.Request(appurl, data)
                response = urllib2.urlopen(req).read()
                return response
            else:
                return "ERROR"
        else:
          print "NO FILE FOUND"
       
    except Exception:
       return "ERROR"
        
def delete_pnr(scenario_id,  user_id="TEST"):
    try:
        appurl = 'http://172.16.137.237:8088/railpnrapp/deletepnr'
        values = {}
        
        
        values['scenario_id'] = scenario_id
        values['user_id'] = user_id
        
        data = urllib.urlencode(values)
        req = urllib2.Request(appurl,data)
        response = urllib2.urlopen(req).read()
        return response
        
    except Exception:
        return "ERROR"
       

