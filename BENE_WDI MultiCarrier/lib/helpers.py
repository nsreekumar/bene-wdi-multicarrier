import random
import string
import paramiko
import os
import re
import sys
import time
import datetime

#PATH_MAGIC = "python ~/tools/Magicstick/latest/1.X/build.py "
PATH_MAGIC = "python ~/tools/Magicstick/latest/1.X_fabien/build.py "

class SSHConnection:

    def __init__(self):
        self.ssh = paramiko.SSHClient()
        
    def __del__(self):
        self.ssh.close()
        
    def connect(self, log_machine, user, passwd):
        try:
            self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            self.ssh.connect(log_machine, username=user, password=passwd)
        except(KeyboardInterrupt, SystemExit,AttributeError,KeyError, IOError,socket.error):
            astring =  "connect: " + " Error: " + str(sys.exc_info()[0]) + str(sys.exc_info()[1]) + str(sys.exc_info()[2])
            print astring
	
    def connect_railpta(self, machine):
		user = "railpta"
		locpass = "jkz8XCV+"
		#log_machine = "lgs" + phase.lower() + "rdp.muc.amadeus.net"
		self.connect(machine,user,locpass)

    def runCommand(self, cmd_str):
        try:
            print "Running ssh command...."
            print "Command : " + cmd_str
            ssh_stdin, ssh_stdout, ssh_stderr = self.ssh.exec_command(cmd_str)
            return (ssh_stdin, ssh_stdout, ssh_stderr)
        except (KeyboardInterrupt, SystemExit,AttributeError,KeyError, IOError,socket.error):
            astring =  "runCommand: " + " Error: " + str(sys.exc_info()[0]) + str(sys.exc_info()[1]) + str(sys.exc_info()[2])
            print astring
	
def generate_correlationID(size=50, chars=string.ascii_uppercase + string.digits + string.ascii_lowercase):
    return ''.join(random.choice(chars) for _ in range(size))

def auto_stick(*args):
    print "USE MAGIC RESPONSIBLY"
    g = args[0] # global reg
    c = args[1] # correlation id
    print c
    fn = g.TTS_ScenarioPath
    fn = re.sub(r'.*/', '', fn)
    fn = re.sub(r'\.play', '', fn)
    print fn
    print g.TTS_ScenarioPath
    if hasattr(g,'MAGICENABLED') and g.MAGICENABLED == "1":
        time.sleep(float(g.MAGICTIMEOUT))
        result = ""
        retries = 0
        samelen = 0
        lastlen = 0
        while (result == "" and retries < 10 and lastlen < 5000 and samelen < 5):
            result = magic_stick_receptor(username=g.MAGICUSER,password=g.MAGICPASSWD, correlation_id=c, directory=g.MAGICDIR, filename=fn, phase=g.MAGICPHASE, starttime=g.starttime, endtime=g.endtime)
            if (not result is None) and len(result) == lastlen:
                samelen=samelen+1
                lastlen = len(result)
            retries = retries+1
            print result

def magic_stick_receptor(username,password, correlation_id, directory, filename, timestamp='',phase='PDT', starttime='', endtime=''):
	try:
		ssh = SSHConnection()
		user = "railpta"
		locpass = "jkz8XCV+"
		correlation_id = correlation_id.strip()
		print "correlationID: "+correlation_id
		log_machine = "lgs" + phase.lower() + "rdp.muc.amadeus.net"
		ssh.connect("ncetsplnx59",user,locpass)
		cmd = PATH_MAGIC + "-u " +str(username) + " -p " + "\"" +str(password) + "\"" + " -c " + "\"" +str(correlation_id) + "\""
		if timestamp != '':
		    cmd += " -t "
		    cmd +=  "\""
		    cmd +=str(timestamp) 
		    cmd +="\"" 
		cmd += " -e "
		cmd += str(phase)
		cmd += " -r "
		cmd += " -d "
		if phase is "LOCALSYD2":
		    cmd += " -l "
		    cmd += " /home/devrdp2/obe/com/log/fe.log_otf_fe "
		    cmd += " -m "
		    cmd += " sydtsplnx02.syd.amadeus.net "
		if phase is "LOCALSYD1":
		    cmd += " -l "
		    cmd += " /home/devrdp1/obe/com/log/fe.log_otf_fe "
		    cmd += " -m "
		    cmd += " sydtsplnx01.syd.amadeus.net "
		if starttime != '':
		    cmd += " -x "
		    cmd +=  "\""
		    cmd += str(starttime)
		    cmd +=  "\""
		if endtime != '':
		    cmd += " -y "
		    cmd +=  "\""
		    cmd += str(endtime)
		    cmd +=  "\""
		    
        #cmd = "python ~/tools/BuildReceptor_New.py " + "-u " +str(username) + " -p " + "\"" +str(password) + "\"" + " -c " + "\"" +str(correlation_id) + "\"" + " -e " + str(phase) 
		stdin,stdout,stderr = ssh.runCommand(cmd)
		#aresult = json.loads(stdout.read())
		aresult = stdout.readlines()
		print "Result: "
		print aresult   
		if len(aresult) != 0:
		    filepath = os.path.join(directory, filename+".gsv")
		    filepath = filepath
		    output_file = open(filepath,'w')
		    for line in aresult:
		        output_file.write(line)
		    output_file.close()        
		    print "Magic Stick has created your file : " + filepath
	except(KeyboardInterrupt, SystemExit,AttributeError,KeyError, IOError):
		astring =  "magic_stick: " + " Error: " + str(sys.exc_info()[0]) + str(sys.exc_info()[1]) + str(sys.exc_info()[2])
		print astring

def magic_stick_injector(username,password, correlation_id, directory, filename,timestamp='',phase='PDT'):
	try:
		ssh = SSHConnection()
		user = "railpta"
		locpass = "jkz8XCV+"
		correlation_id = correlation_id.strip()
		log_machine = "lgs" + phase.lower() + "rdp.muc.amadeus.net"
		ssh.connect("ncetsplnx59",user,locpass)
		cmd = PATH_MAGIC + "-u " +str(username) + " -p " + "\"" +str(password) + "\"" + " -c " + "\"" +str(correlation_id) + "\""
		if timestamp != '':
		    cmd += " -t "
		    cmd +=  "\""
		    cmd +=str(timestamp) 
		    cmd +="\"" 
		cmd += " -e "
		cmd += str(phase)
		cmd += " -i "
        #cmd = "python ~/tools/BuildReceptor_New.py " + "-u " +str(username) + " -p " + "\"" +str(password) + "\"" + " -c " + "\"" +str(correlation_id) + "\"" + " -e " + str(phase) 
		stdin,stdout,stderr = ssh.runCommand(cmd)
		print cmd
		#aresult = json.loads(stdout.read())
		aresult = stdout.readlines()
		print aresult
		if len(aresult) != 0:
		    filepath = os.path.join(directory, filename+".play")
		    filepath =  filepath
		    output_file = open(filepath,'w')
		    for line in aresult:
		        output_file.write(line)
		    output_file.close()        
		    print "Magic Stick has created your file : " + filepath
	except(KeyboardInterrupt, SystemExit,AttributeError,KeyError, IOError):
		astring =  "magic_stick: " + " Error: " + str(sys.exc_info()[0]) + str(sys.exc_info()[1]) + str(sys.exc_info()[2])
		print astring
		
def magic_stick_local_injector(correlation_id, log_file, directory, filename, machine):
	try:
		ssh = SSHConnection()
		user = "railpta"
		locpass = "jkz8XCV+"
		#log_machine = "lgs" + phase.lower() + "rdp.muc.amadeus.net"
		ssh.connect(machine,user,locpass)
		correlation_id = correlation_id.strip()
		log_file = log_file.strip()
		cmd = PATH_MAGIC  + " -c " + "\"" +str(correlation_id) + "\"" + " -l " + str(log_file) + " -m " + str(machine) + " -i "
		stdin,stdout,stderr = ssh.runCommand(cmd)
		aresult = stdout.read()
		#json.loads(aresult)
		print aresult
		if len(aresult) != 0:
		    filepath = os.path.join(directory, filename+".play")
		    output_file = open(filepath,'w')
		    for line in aresult:
		        output_file.write(line)
		    output_file.close()
		    print "Magic Stick has created your file : " + filepath
	except(KeyboardInterrupt, SystemExit,AttributeError,KeyError, IOError):
		astring =  "magic_stick: " + " Error: " + str(sys.exc_info()[0]) + str(sys.exc_info()[1]) + str(sys.exc_info()[2])
		print astring
		
def magic_stick_local_receptor(correlation_id, log_file, directory, filename, machine):
	try:
		ssh = SSHConnection()
		ssh.connect_railpta(machine)
		#log_machine = "lgs" + phase.lower() + "rdp.muc.amadeus.net"
		correlation_id = correlation_id.strip()
		log_file = log_file.strip()
		cmd = PATH_MAGIC  + " -c " + "\"" +str(correlation_id) + "\"" + " -l " + str(log_file) + " -m " + str(machine) + " -r "
		stdin,stdout,stderr = ssh.runCommand(cmd)
		aresult = stdout.read()
		#json.loads(aresult)
		print aresult
		if len(aresult) != 0:
		    filepath = os.path.join(directory, filename+".gsv")
		    output_file = open(filepath,'w')
		    for line in aresult:
		        output_file.write(line)
		    output_file.close()
		    print "Magic Stick has created your file : " + filepath
	except(KeyboardInterrupt, SystemExit,AttributeError,KeyError, IOError):
		astring =  "magic_stick: " + " Error: " + str(sys.exc_info()[0]) + str(sys.exc_info()[1]) + str(sys.exc_info()[2])
		print astring
        
    
def wildRep(filename):
    ssh = SSHConnection()
    ssh.connect_railpta("ncetsplnx59")
    cmd = "/opt/Python-2.7/bin/python ~radscm/HgRepositories/utils/HgScripts/wildRep.py "  + " -f " + filename 
    stdin,stdout,stderr = ssh.runCommand(cmd)
    aresult = stdout.readlines()
    
    
def nextThursday(formatting):
    return (datetime.date.today() + datetime.timedelta( ((3-datetime.date.today().weekday())+7) % 14 )).strftime(formatting)

def nextFriday(formatting):
    return (datetime.date.today() + datetime.timedelta( ((4-datetime.date.today().weekday())+7) % 14 )).strftime(formatting)


def nextSaturday(formatting):
    return (datetime.date.today() + datetime.timedelta( ((5-datetime.date.today().weekday())+7) % 14 )).strftime(formatting)


def nextSunday(formatting):
    return (datetime.date.today() + datetime.timedelta( ((6-datetime.date.today().weekday())+7) % 14 )).strftime(formatting)


def nextMonday(formatting):
    return (datetime.date.today() + datetime.timedelta( ((7-datetime.date.today().weekday())+7) % 14 )).strftime(formatting)

def nextTuesday(formatting):
    return (datetime.date.today() + datetime.timedelta( ((8-datetime.date.today().weekday())+7) % 14 )).strftime(formatting)


def nextWednesday(formatting):
    return (datetime.date.today() + datetime.timedelta( ((2-datetime.date.today().weekday())+7) % 14 )).strftime(formatting)

def deltaDateFormatted(delta,formatting):
	##
	# **Date + Delta (in days) function** .
	# This function allows us to use the current date with a delta as input
	# it allows us to choose our specific format\n *exple: %d-%m-%Y for 16-10-2016*
	# @param delta To choose how many days you want to go in the future/past
	# @param format To specify the format you seek to display
	# @return result The date in a String format
	return (datetime.date.today()+datetime.timedelta(days=delta)).strftime(formatting).upper()
