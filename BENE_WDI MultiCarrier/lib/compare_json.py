"""
Utility module for TTS non-regression involving REST transactions with json.

Function `compare_json` can be called in a ''Match callback in order to compare
the json string captured from the response with an expected json string.

Detailed error messages are printed on the standard output in case of failure
for each difference found.

Example of usage in TTS::

GET [...]
''Response:
''[...]
''{%received_json%=.*}
''End of response
''Match:compare_json(received_json, expected_json,
                     TTServer.currentMessage.TTS_MATCH_COMPARISON_OK,
                     TTServer.currentMessage.TTS_MATCH_COMPARISON_FAILURE)

Where `expected_json` is defined, for example, in the ''Initialize block.
It must be a python string representing the expected json. Regexp patterns
can be given instead of fixed field values, as strings enclosed in slashes.

Example::

expected_json = '''{
    "foo": "toto",
    "bar": "/\\d+/",
    "key_with_any_value": "/*/",
    "key_with_any_value_but_not_null": "/+/",
    "/*/": "/*/",
    "array":[
        {first element of the array},
        {"/*/": "/*/"} <---- means to ignore all other elements in the array
    ]
}'''

In this example, the received object we match whenever its `bar` field's
string representation is composed of digits only. Note that we compare
the pattern to the string representation of the field, so the regex here
would match both the string "12" and the litteral integer 12.

The special regex '/*/' matches everything, even objects. This is used
when we don't care about the value of a field/level at all.

The key/value pair '/*/': '/*/' allows to accept any additional keys (with
any value in them) in that level.

Beware to double the backslashes in the patterns, otherwise json's decoder
interprets them as escapes and the json parsing fails.
"""

import json
import re



def _compare_level(received, expected, parent_key):
    """Internal recursive function, do not use outside this package!

    Compare two python objects decoded from a json string. Expected may be a
    regex pattern. If the objects are dictionaries or lists, iterate recursively
    on their contents.

    :param received: the object received
    :param expected: the expected object. Can be a regex pattern: '/pattern/'
    :param str parent_key: the key for the current level. Used to report errors.

    :return: True if the comparison failed.
    """

    comparison_failure = False

    if expected == '/+/':
        if received is None:
            print "Match error: value can not be null for key {}. ".format(parent_key)
            comparison_failure = True
        return comparison_failure
        
    if expected == '/*/':
        # accept everything
        comparison_failure = False
        return comparison_failure



    if isinstance(received, dict):
        if not isinstance(expected, dict):
            print "Match error on key '{}': received a dict but expected a {}.".format(parent_key,
                                                                                       type(expected).__name__)
            comparison_failure = True
            # We cannot compare further, so return
            return comparison_failure

        # Both received and expected are dicts, compare their contents
        rec_keys = set(received.keys())
        exp_keys = set(filter(lambda x: x != '/*/', expected.keys()))
        if rec_keys - exp_keys and '/*/' not in expected:
            print "Match error on key '{}': unexpected keys {}.".format(parent_key, list(rec_keys - exp_keys))
            comparison_failure = True
        if exp_keys - rec_keys:
            print "Match error on key '{}': missing keys {}.".format(parent_key, list(exp_keys - rec_keys))
            comparison_failure = True

        # Recursively compare the contents of all common fields
        for key in rec_keys & exp_keys:
            comparison_failure |= _compare_level(received[key], expected[key], "{}.{}".format(parent_key, key))

    elif isinstance(received, list):
        if not isinstance(expected, list):
            print "Match error on key '{}': received a list but expected a {}.".format(parent_key,
                                                                                       type(expected).__name__)
            comparison_failure = True
            # We cannot compare further, so return
            return comparison_failure
        if len(expected) ==2 and expected[1].get('/*/') == "/*/":  
            comparison_failure |= _compare_level(received[0], expected[0], "{}[{}]".format(parent_key, 0))
        elif len(expected) ==2 and expected[1].get('/loop/') == "/all/":
            # Recursively compare the elements in the list
            for i in xrange(len(received)):
                comparison_failure |= _compare_level(received[i], expected[0], "{}[{}]".format(parent_key, i))

        elif len(received) != len(expected): 
            print "Match error on key '{}': received {} elements, expected {}.".format(parent_key,
                                                                                       len(received), len(expected))
            comparison_failure = True
        else:    
            # Recursively compare the elements in the list
            for i in xrange(min(len(received), len(expected))):
                comparison_failure |= _compare_level(received[i], expected[i], "{}[{}]".format(parent_key, i))

    elif isinstance(expected, basestring) and expected.startswith('/') and expected.endswith('/'):
        # regular expression: '/pattern/'
        if not isinstance(received, basestring):
            # Compare the string representation of the object to the pattern
            received = str(received)

        if re.match(expected[1:-1], received) is None:
            print "Match error on key '{}': '{}' does not match regular expression '{}'.".format(parent_key,
                                                                                                 received,
                                                                                                 expected[1:-1])
            comparison_failure = True
    else:
        # Not a dict, not a list, not a regex... Simply compare both values then!           
        if received != expected:
            print "Match error: value differs for key '{}': received '{}' instead of '{}'.".format(parent_key,
                                                                                                   received, expected)
            comparison_failure = True

    return comparison_failure


def clean_json_from_tts(received):
    """Remove undesired escapes added by TTS on a response json string.

    :param str received: the received json string captured by TTS.

    :return: a clean copy of the json string, parsable by json package.
    """
    return received.replace('\\{', '{').replace('\\}', '}').replace('\\:', ':').replace('\\*', '*').replace('\\\\"', '\\"').replace("\\'", "\\\\'").replace('\\%', '%').replace('\\&', '&')


def compare_json(received_json, expected_json, response_ok, response_ko):
    """Compare two json.

    Clean the received_json, load the received and expected json strings into
    python objects and recursively compare them. Regular expression patterns
    can be specified in the expected_json, as strings starting and ending with
    slashes (e.g. '{"boardDate": "/\\d{4}-\\d{2}-\\d{2}/"}'). Double the
    backslashes so that the json module does not interpret them as escapes.

    If differences are found, detailed messages are printed in the standard
    output. This does not stop the comparison, so all differences are printed.

    :param received_json: the json string captured by TTS in the reply.
    :param expected_json: the json string we expect to get.
    :param response_ok: value to return if the comparison is successful.
    :param response_ko: value to return if the comparison fails.

    :type received_json: str
    :type expected_json: str
    """
    received_json = clean_json_from_tts(received_json)
    try:
        json_received = json.loads(received_json)
    except Exception, e:
        print 'Error while decoding received json: {}'.format(e)
        print received_json[:20], '...', received_json[-20:]
        return response_ko

    try:
        json_expected = json.loads(expected_json)
    except Exception, e:
        print 'Error while decoding expected json: {}'.format(e)
        print expected_json[:20], '...', expected_json[-20:]
        return response_ko

    comparison_failure = _compare_level(json_received, json_expected, '')

    if comparison_failure:
        return response_ko
    else:
        return response_ok
        
        

def compare_emails(email, Email1, response_ok, response_ko):
    if all(x in email for x in Email1) and len(email)==len(Email1):
         return response_ok
    else:
         return response_ko