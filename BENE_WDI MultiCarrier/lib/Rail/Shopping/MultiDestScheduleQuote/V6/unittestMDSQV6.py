import unittest
from MultiDestScheduleQuoteV6 import *

class TestShoppingMDSQV6(unittest.TestCase):
    def test_upper(self):
        railContext = {
            "officeID":                 "NCE1A0950",
            "corrID":                   "LKJSDHAFQ347HFOER8UHFO8EWH5RQI8EUHROW",
            "language":              "EN",
            "journey":                  {"schedule":[{
                                                                "inventorySystem":"FRR", 
                                                                "serviceProvider":"SNF",
										                        "segmentTID":"SEG_1",
										                        "locationCodeType":"UIC",
                                                                "origin":"8768600",
                                                                "destination":"8772319",
                                                                "trainNumber":"6607", 
                                                                "proposal": "602",
                                                                "startDate":"%Y-%m-%dT08:59:00"
                                                                }]
                                        },
            "pricingList":              [{
                                        "bookingClass":"BF",
                                        "bookingClassTID":"BC_1",
                                        "quotingRule":"FA00",
                                        "quotingRuleTID":"QR_1",
                                        "quotingRuleType":"001",
										"paxAssociation":"PAX_1",
										"segmentAssociation":"SEG_1"
                                        }],
            "ticketingOption":    {
                                        "type":"006",
                                        "distributionType":"002"
                                        },
            "passengerList":            [ {
                                        "firstName":"FRODO",
                                        "lastName":"BAGGINS",
                                        "dateOfBirth":"1980-01-01",
                                        "PassengerCategoryType":"PT00AD",
                                        "PassengerCategoryCode":"006",
                                        "PassengerCategoryContext":"1A",
                                        "tattoo":"2",
                                        "emailAddress":"YANICE.CHERRAK@AMADEUS.COM"
                                    }]
        }
        mdsq = '''<AMA_RailMultiDestScheduleQuoteRQ ReplyOptions="NoCache Services Descriptions" Version="6.0" CorrelationID="LKJSDHAFQ347HFOER8UHFO8EWH5RQI8EUHROW" PrimaryLangID="EN"><Schedule TID="1"><start dateTime="%Y-%m-%dT08:59:00"><locationCode type="UIC">8768600</locationCode></start><end><locationCode type="UIC">8772319</locationCode></end></Schedule><Passenger TID="2" BirthDate="1980-01-01"></Passenger><InventorySystem Code="FRR" Context="1A"/><AdditionalData><Data Name="ProviderOfficeId">NCE1A0950</Data></AdditionalData></AMA_RailMultiDestScheduleQuoteRQ>'''
        gcar = '''<AMA_RailGetAccommodationsRQ Version="5.4" PrimaryLangID="EN" xmlns="http://xml.amadeus.com/RAI/2009/10"><POS><Source><RequestorID Type="30" ID_Context="1A" ID="NCE1A0950"/></Source></POS><ProposalRPH RPH="602"/></AMA_RailGetAccommodationsRQ>'''
        self.assertEqual(MultiDestScheduleQuoteV6(railContext), mdsq)
        self.assertEqual(GetAccommodationsV5(railContext, railContext["journey"]["schedule"][0]["proposal"]), gcar)

if __name__ == "__main__":
    unittest.main()
