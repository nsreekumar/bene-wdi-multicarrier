import jinja2, os, sys 

MultiDestScheduleQuote="""<AMA_RailMultiDestScheduleQuoteRQ ReplyOptions="NoCache Services Descriptions" Version="6.0" CorrelationID="{{ myContext["corrID"] }}" PrimaryLangID="{{ myContext["language"] }}">
    {% for schedule in journey %}
    {% set scheduleLoop = loop %}
    <Schedule TID="{{ scheduleLoop.index }}">
    {% for segment in journey[schedule]%}
    {% set segmentLoop = loop %}
    <start dateTime="{{ journey[schedule][segmentLoop.index-1]["startDate"] }}">
        <locationCode type="{{  journey[schedule][segmentLoop.index-1]["locationCodeType"] }}">{{ journey[schedule][segmentLoop.index-1]["origin"] }}</locationCode>
    </start>
    <end{% if journey[schedule][segmentLoop.index-1]["endDate"] %} dateTime="{{ journey[schedule][segmentLoop.index-1]["endDate"] }}"{% endif %}>
        <locationCode type="{{  journey[schedule][segmentLoop.index-1]["locationCodeType"] }}">{{ journey[schedule][segmentLoop.index-1]["destination"] }}</locationCode> 
    </end>
    {% if journey[schedule][segmentLoop.index-1]["maxChanges"] %}<SearchCriteria maxChanges="{{ journey[schedule][segmentLoop.index-1]["maxChanges"] }}"/>{% endif %}
    {% endfor %}
    </Schedule>
    {% endfor %}
    {% for passenger in passengerList %}
    <Passenger TID="{{ passenger["tattoo"] }}"{% if passenger["dateOfBirth"] %} BirthDate="{{ passenger["dateOfBirth"] }}"{% endif %}>
        {% if passenger["corporateCode"] %}
        <Reduction>
            <Corporate Code="{{ passenger["corporateCode"] }}"></Corporate>
        </Reduction>
        {% endif %}
    </Passenger>
    {% endfor %}
    {% for iS in inventorySystems %}
    {% set inventorySystemLoop = loop %}
    <InventorySystem Code="{{ iS }}" Context="1A"/>
    {% endfor %}
    <AdditionalData>
    <Data Name="ProviderOfficeId">{{ myContext["officeID"] }}</Data>
    </AdditionalData>
</AMA_RailMultiDestScheduleQuoteRQ>"""

GetAccommodations="""<AMA_RailGetAccommodationsRQ Version="5.4" PrimaryLangID="{{ myContext["language"] }}" xmlns="http://xml.amadeus.com/RAI/2009/10">
    <POS>
        <Source>
            <RequestorID Type="30" ID_Context="1A" ID="{{ myContext["officeID"] }}"/>
        </Source>
    </POS>
    <ProposalRPH RPH="{{ myProposal }}"/>
</AMA_RailGetAccommodationsRQ>"""

def MultiDestScheduleQuoteV6(context):
    loader = jinja2.FileSystemLoader(os.getcwd())
    jenv = jinja2.Environment(trim_blocks=True, lstrip_blocks=True)
    t2 = jenv.from_string(MultiDestScheduleQuote)
    inventorySystemsList = []
    for schedule in context["journey"]:
        for segment in context["journey"][schedule]:
            if not(segment["inventorySystem"] in inventorySystemsList):
                inventorySystemsList.append(segment["inventorySystem"])
    return "".join(map(unicode.strip,t2.render(myContext = context,journey = context["journey"],passengerList = context["passengerList"], inventorySystems = inventorySystemsList).splitlines()))

def GetAccommodationsV5(context, proposal):
    loader = jinja2.FileSystemLoader(os.getcwd())
    jenv = jinja2.Environment(trim_blocks=True, lstrip_blocks=True)
    t2 = jenv.from_string(GetAccommodations)
    return "".join(map(unicode.strip,t2.render(myContext=context,myProposal=proposal).splitlines()))
