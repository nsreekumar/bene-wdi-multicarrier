import xml.etree.ElementTree as ET
from datetime import date,datetime

def findpaxrs(root, context):
    listpax = list()
    for pax in root.iter('Passenger'):
        listpax.append({"PAX_ID": pax.attrib['ID'], "PAX_TID": pax.attrib['TID']})
    print(listpax)
    return listpax

def fetchoutwardsegdata(root, context):
    outward_segment_details = []
    #SSL_ID = ""
    outward_segment_data = []
    sslfound = False
    check = False
    for sh in root.iter('Schedule'):
        if sh.attrib['Direction'] == "Outward":
            val = context['Outward']
            print("Val is : ", val)
            print("Val len is : ", len(val))
            for i in range(len(val)):
                print("I inside the loop is :", i)
                for seg in sh.findall('Segment'):
                    if seg[0].find('locationCode').text.strip("\n") == val[i]['startlocation'] and seg[1].find(
                            'locationCode').text.strip("\n") == val[i]['endlocation'] and seg.find('vehicle').attrib[
                            'Category'] == val[i]['trainType']:
                        print("Entered")
                        check = True
                        outward_segment_details.append({"SEG_ID": seg.attrib['ID'],
                                                        "Seg_railID": seg.find('identifier').text.strip("\n"),
                                                        "start_loc_name": seg.find('start').attrib['locationName'],
                                                        "end_loc_name": seg.find('end').attrib['locationName'],
                                                        "start_date_time": seg.find('start').attrib['dateTime'],
                                                        "end_date_time": seg.find('end').attrib['dateTime'],
                                                        "startlocation": seg[0].find('locationCode').text.strip("\n"),
                                                        "endlocation": seg[1].find('locationCode').text.strip("\n")})
                        if i + 1 == len(val) and check == True:
                            print("Entering the check")
                            break
            # print("I is :", i)

            # print("len is :", len(val))


    print("Outward Seg data is :", outward_segment_details)

    for sh in root.iter('Schedule'):
        if sh.attrib['Direction'] == "Outward":
            for sol in sh.findall('Solution'):
                for val in outward_segment_details:
                    if val.get('SEG_ID') in sol.attrib['RefIDs']:
                        sslfound = True
                if sslfound:
                    SSL_ID = sol.attrib['ID']
                    paxlist = findpaxrs(root,context)
                    outward_segment_data.append({"SSL_ID" : SSL_ID , "SegmentDetails": outward_segment_details, "paxdetails": paxlist})
                    print("SSL IS: ", SSL_ID)
                    break
                else:
                    print("NO SSL FOUND")
    return outward_segment_data

def fetchinwardsegdata(root, context):
    inward_segment_details = []
    inward_segment_data = []
    sslfound = False
    check = False
    for sh in root.iter('Schedule'):
        if sh.attrib['Direction'] == "Inward":
            val = context['Inward']
            print("Val is : ", len(val))
            for i in range(len(val)):
                for seg in sh.findall('Segment'):
                    if seg[0].find('locationCode').text.strip("\n") == val[i]['startlocation'] and seg[1].find(
                        'locationCode').text.strip("\n") == val[i]['endlocation'] and seg.find('vehicle').attrib[
                    'Category'] == val[i]['trainType']:
                        check = True
                        inward_segment_details.append({"SEG_ID": seg.attrib['ID'],
                                                       "Seg_railID": seg.find('identifier').text.strip("\n"),
                                                       "start_loc_name": seg.find('start').attrib['locationName'],
                                                       "end_loc_name": seg.find('end').attrib['locationName'],
                                                       "start_date_time": seg.find('start').attrib['dateTime'],
                                                       "end_date_time": seg.find('end').attrib['dateTime'],
                                                       "startlocation": seg[0].find('locationCode').text.strip("\n"),
                                                       "endlocation": seg[1].find('locationCode').text.strip("\n")})

                        if i+1 == len(val) and check == True:
                            print("Val is : ", len(val))
                            print("Its True")
                            break
    print("Inward Seg data is :", inward_segment_details)
    for sh in root.iter('Schedule'):
        if sh.attrib['Direction'] == "Inward":
            for sol in sh.findall('Solution'):
                #count = 1
                for val in inward_segment_details:
                    if val.get('SEG_ID') in sol.attrib['RefIDs']:
                        sslfound = True
                        #count= count=1
                if sslfound:
                    SSL_ID = sol.attrib['ID']
                    paxlist = findpaxrs(root, context)
                    inward_segment_data.append({"SSL_ID" : SSL_ID, "SegmentDetails": inward_segment_details, "paxdetails": paxlist})
                    print("SSL IS: ", SSL_ID)
                    break
                else:
                    print("NO SSL FOUND")
    return inward_segment_data

def segmentdetails(root, context):
    print("Context: ", context['IsRoundTrip'])
    segment_data = []
    if (context['IsRoundTrip'] == "N") or ((context['IsRoundTrip'] == "Y" and 'TwoStepShopping' in context) and context['TwoStepShopping'] == "Y"):
        for sh in root.iter('Schedule'):
            if sh.attrib['Direction'] == "Outward":
                data = fetchoutwardsegdata(root, context)
                segment_data.append({"Outward": data})
                break
            elif sh.attrib['Direction'] == "Inward" and context['IsRoundTrip'] != "N":
                data = fetchinwardsegdata(root, context)
                segment_data.append({"Inward": data})
                break

    elif context['IsRoundTrip'] == "Y" and ('TwoStepShopping' in context and context['TwoStepShopping'] == "N"):
        out_data = fetchoutwardsegdata(root, context)
        in_data = fetchinwardsegdata(root, context)
        print("Inward Data ###MAIN### is : ", in_data )
        segment_data.append({"Outward": out_data, "Inward": in_data})
    return segment_data


def findPaxList(paxDetailsRQ, paxDetailsRS):
    paxlist = []
    for i in range(len(paxDetailsRQ)):
        for j in range(len(paxDetailsRS)):
            if paxDetailsRQ[i]["tattoo"] == paxDetailsRS[j]["PAX_TID"]:
                paxlist.append({"PAX_ID": paxDetailsRS[j]["PAX_ID"], "DOB": paxDetailsRQ[i]["dateOfBirth"]})
    return paxlist

def getage(DOB):
    today = date.today()
    DOB = datetime.strptime(DOB, '%Y-%m-%d').date()
    age = today.year - DOB.year - ((today.month, today.day) < (DOB.month, DOB.day))
    return age


def assigncategory(paxlist, inventory):
    if inventory == "FRR":
        for pax in paxlist:
            age = getage(pax.get('DOB'))
            print(age)
            if 26 <= age <= 59:
                print("Inside Adult case")
                pax['CATEGORY'] = "006"
            elif 0 <= age <= 3:
                pax['CATEGORY'] = "001"
            elif 4 <= age <= 11:
                pax['CATEGORY'] = "002"
            elif 60 <= age <= 99:
                pax['CATEGORY'] = "005"
            elif 12 <= age <= 25:
                pax['CATEGORY'] = "003"
            else:
                print("Pax Category Assignment Not Found for FRR")
                break


def fetchbookinglcass(context,root):
    BookingClass = []
    bkgclasslist = []
    if 'bookingClass' in context:
        for k in root.iter("BookingClass"):
            if context['bookingClass'] == k.attrib['Class']:
                BC_ID = k.attrib['ID']
                BookingClass.append(BC_ID)
                bkgclasslist.append(k.attrib)
                #print("Value for BC: ",k.attrib)

    else:
        for k in root.iter("BookingClass"):
            if '001' == k.attrib['Class']:
                BC_ID = k.attrib['ID']
                BookingClass.append(BC_ID)
                bkgclasslist.append(k.attrib)
    return BookingClass,bkgclasslist


def fetchquotingrules(context, root):
    QuotingRule = []
    quotingrulelist = []
    if 'fareCode' in context:
        for l in root.iter("QuotingRule"):
            if l.attrib['Code'] == context['fareCode'] and l.attrib["Flexibility"] == context['flexibility']:
                    QR_ID = l.attrib['ID']
                    QuotingRule.append(QR_ID)
                    quotingrulelist.append(l.attrib)
    else:
        for l in root.iter("QuotingRule"):
            if l.attrib["Flexibility"] == context['flexibility']:
                    QR_ID = l.attrib['ID']
                    QuotingRule.append(QR_ID)
                    quotingrulelist.append(l.attrib)
    return QuotingRule, quotingrulelist


def fetchreductions(root, paxList, context):
    paxReductions = []
    reductionlist = []

    for red in root.iter("Reduction"):
        for n in red.findall("PassengerCategory"):
            reductionlist.append({'Description': red.attrib['Description'], 'ID': red.attrib['ID'], 'paxcatcode': n.attrib['Code']})
        if red[0].tag == "Corporate":
            #print("Value of red list code: ", red[0].get('Code'))
            reductionlist.append(
                {'ID': red.attrib['ID'],'corporatecode': red[0].get('Code'), 'vendorcompcode': red[1].get('Code')})
    for i in range(len(paxList)):
        if paxList[i]["CATEGORY"] == "005":
            reduction_ids_sr = []
            for red in root.iter("Reduction"):
                for n in red.findall("PassengerCategory"):
                    if n.attrib['Code'] == paxList[i]["CATEGORY"]:
                        RED_ID = red.attrib['ID']
                        reduction_ids_sr.append(RED_ID)
        if paxList[i]["CATEGORY"] == "001":
            reduction_ids_inf = []
            for red in root.iter("Reduction"):
                for n in red.findall("PassengerCategory"):
                    if n.attrib['Code'] == paxList[i]["CATEGORY"]:
                        RED_ID = red.attrib['ID']
                        reduction_ids_inf.append(RED_ID)
        if paxList[i]["CATEGORY"] == "002":
            reduction_ids_chd = []
            for red in root.iter("Reduction"):
                for n in red.findall("PassengerCategory"):

                    if n.attrib['Code'] == paxList[i]["CATEGORY"]:
                        RED_ID = red.attrib['ID']
                        reduction_ids_chd.append(RED_ID)
        if paxList[i]["CATEGORY"] == "003":
            reduction_ids_yth = []
            for red in root.iter("Reduction"):
                for n in red.findall("PassengerCategory"):
                    if n.attrib['Code'] == paxList[i]["CATEGORY"]:
                        RED_ID = red.attrib['ID']
                        reduction_ids_yth.append(RED_ID)
        if paxList[i]["CATEGORY"] == "006":
            reduction_ids_adt = []
            for red in root.iter("Reduction"):
                for n in red.findall("PassengerCategory"):
                    if n.attrib['Code'] == paxList[i]["CATEGORY"]:
                        RED_ID = red.attrib['ID']
                        reduction_ids_adt.append(RED_ID)
    if 'reduction_ids_sr' in locals():
        for i in range(len(paxList)):
            if paxList[i]["CATEGORY"] == "005":
                paxReductions.append({"PAXID" : paxList[i]["PAX_ID"],"RED_IDS": reduction_ids_sr,"CATEGORY":"005"})
    if 'reduction_ids_inf' in locals():
        for i in range(len(paxList)):
            if paxList[i]["CATEGORY"] == "001":
                paxReductions.append({"PAXID" : paxList[i]["PAX_ID"], "RED_IDS": reduction_ids_inf,"CATEGORY":"001"})
    if 'reduction_ids_chd' in locals():
        for i in range(len(paxList)):
            if paxList[i]["CATEGORY"] == "002":
                paxReductions.append({"PAXID" :paxList[i]["PAX_ID"], "RED_IDS": reduction_ids_chd,"CATEGORY":"002"})
    if 'reduction_ids_yth' in locals():
        for i in range(len(paxList)):
            if paxList[i]["CATEGORY"] == "003":
                paxReductions.append({"PAXID" :paxList[i]["PAX_ID"], "RED_IDS": reduction_ids_yth,"CATEGORY":"003"})
    if 'reduction_ids_adt' in locals():
        for i in range(len(paxList)):
            if paxList[i]["CATEGORY"] == "006":
                paxReductions.append({"PAXID" : paxList[i]["PAX_ID"],"RED_IDS": reduction_ids_adt,"CATEGORY":"006"})
    if context['paxCategoryDifferent'] == 1:
        print("Inside POP PAX REDS")
        for i in range(len(paxReductions)):
            print("I is : ", i)
            print(len(paxReductions))
            for j in range(i+1, len(paxReductions)):
                print("I Value is: ", paxReductions[i]['CATEGORY'])
                print("I Value is: ", paxReductions[j]['CATEGORY'])
                if paxReductions[i]['CATEGORY'] == paxReductions[j]['CATEGORY']:
                    paxReductions[i]['PAXID'] += " "+paxReductions[j]['PAXID']
                    paxReductions.pop(j)
                if len(paxReductions)-1 == j:
                    break
    print("Pax Reds are : ", paxReductions)
    return paxReductions, reductionlist


def filtersolutions (root, SSL_ID):
    solutionData = []
    print("In This Loop we filter solution")
    for m in root.iter("Solution"):
        if m.attrib['RefIDs'] == SSL_ID:
            for n in m.findall("Quote"):
                    if n.attrib['Type'] == "Fare":
                        for l in n.findall("Product"):
                            if l.attrib['Type'] == "Segment":
                                solutionData.append({"BookingToken": m.attrib['BookingToken'], "REF_IDS_" + n.attrib['RefIDs']+"_"+l.attrib['ID']: l.attrib[
                                                    'RefIDs'], "SSL_ID": m.attrib['RefIDs'],
                                                    "PaxID": n.attrib['RefIDs']})
    return solutionData


def resolvesolutions(solutionData):
    update_s_dict = {}
    for s in solutionData:
        if s['BookingToken'] not in update_s_dict:
            update_s_dict[s['BookingToken']] = {}
    i = 1
    while len(solutionData) != 0:
        solution = solutionData.pop(0)
        update_s_dict[solution['BookingToken']]['PaxID_' + str(i)] = solution.get('PaxID', "")
        if solution['BookingToken'] == update_s_dict[solution['BookingToken']].get('BookingToken'):
            i += 1
        else:
            i = 1
        del solution["PaxID"]
        update_s_dict[solution['BookingToken']].update(solution)
    return update_s_dict


def fetchBookingData(solutionDict, BookingClass, QuotingRule, paxReductions, context):
    print("inside fetchBookingData")
    bookingData = []
    if context['paxCategoryDifferent'] == 0:
        for key, value in solutionDict.items():
            check, check2 = False, False
            for key1, value1 in value.items():
                if "REF_IDS" in key1:
                    listRefs = value1.split(" ")
                    print(listRefs)
                    check = any(item in BookingClass for item in listRefs)
                    if check:
                        print("Inside Booking Class Check")
                        check2 = any(item in QuotingRule for item in listRefs)

                    if not check or not check2:
                        break
            if check and check2:
                print("######Checks for Booking class and QR CODE done, No Reduction Checks Requested#######")
                bookingData.append({"BookingToken": key})
    elif context['paxCategoryDifferent'] == 1:
        for key, value in solutionDict.items():
            check, check2, check3 = False, False, False
            for key1, value1 in value.items():
                if "REF_IDS" in key1:
                    listRefs = value1.split(" ")
                    print(listRefs)
                    check = any(item in BookingClass for item in listRefs)
                    if check:
                        print("Inside Booking Class Check")
                        check2 = any(item in QuotingRule for item in listRefs)
                        if check2:
                            print("Inside Quoting Rule Check")
                            for pax in paxReductions:
                                if pax.get("PAXID") in key1:
                                    pax.get('RED_IDS')
                                    check3 = any(item in pax.get('RED_IDS') for item in listRefs)

                    if not check or not check2 or not check3:
                        break
            if check and check2 and check3:
                print("#######Checks for Booking class, QR CODE  and Reduction code done#######")
                bookingData.append({"BookingToken": key})
    return bookingData


def additionalbookingdata(bookingtoken, bkgclasslist, quotingrulelist, reductionlist,solutuiondict):
    for key, value in solutuiondict.items():
        if key == bookingtoken:
            filteredsolution = value
            break
    print("Filtered Solution data is : ", filteredsolution)
    filtered_booking_list = []
    filtered_quoting_list = []
    filtered_reduction_list = []
    bookinginputlist = []
    for key, value in filteredsolution.items():
        if "REF_IDS" in key:
            listRefs = value.split(" ")
            for i in range(len(listRefs)):
                for n in range(len(bkgclasslist)):
                    if listRefs[i] == bkgclasslist[n]['ID']:
                        if len(filtered_booking_list) != 0:
                            p = len(filtered_booking_list) - 1
                            if filtered_booking_list[p]['ID'] != bkgclasslist[n]['ID']:
                                filtered_booking_list.append(bkgclasslist[n])
                        else:
                            filtered_booking_list.append(bkgclasslist[n])

                for n in range(len(quotingrulelist)):
                    if listRefs[i] == quotingrulelist[n]['ID']:
                        if len(filtered_quoting_list) != 0:
                            p = len(filtered_quoting_list) - 1
                            if filtered_quoting_list[p]['ID'] != quotingrulelist[n]['ID']:
                                filtered_quoting_list.append(quotingrulelist[n])
                        else:
                            filtered_quoting_list.append(quotingrulelist[n])

                for n in range(len(reductionlist)):
                    if listRefs[i] == reductionlist[n]['ID']:
                        if len(filtered_reduction_list) != 0:
                            p = len(filtered_reduction_list) - 1
                            if filtered_reduction_list[p]['ID'] != reductionlist[n]['ID']:
                                filtered_reduction_list.append(reductionlist[n])
                        else:
                            filtered_reduction_list.append(reductionlist[n])

    bookinginputlist.append(
        {"filtered_booking_list": filtered_booking_list, "filtered_quoting_list": filtered_quoting_list,
         "filtered_reduction_list": filtered_reduction_list})

    return bookinginputlist




def MultiDestScheduleQuoteRS3(res,context,file_path, inventory,paxDetailsRQ):
    res=res.replace('rail:','')
    f = open("temp.xml", "w")
    L = ["<AMA_RailMultiDestScheduleQuoteRS>", res]
    f.writelines(L)
    print "Response is: "+res
    f.close()
    with open("temp.xml", 'r+') as f:
        s = f.read()
        s = s.replace('>', '>\n')
        f.seek(0,0)
        f.write(s)
    tree = ET.parse(file_path)
    root = tree.getroot()
    bookingdata= []
    filtered_bkg_data = []
    segment_data = segmentdetails(root, context)
    print("Segment Details are: ", segment_data)

    paxlist = findPaxList(paxDetailsRQ, segment_data[0]['Outward'][0]['paxdetails'])
    print(paxlist)
    assigncategory(paxlist, inventory)
    print("##############PAX LIST WITH CATEGORY ASSIGNED IS################: ", paxlist)
    BookingClass, bkgclasslist = fetchbookinglcass(context, root)
    print("List of Booking Class: ", bkgclasslist)
    QuotingRule, quotingrulelist = fetchquotingrules(context, root)
    print("List of Quoting Rules: ", quotingrulelist)

    if context['paxCategoryDifferent'] == 1:
        paxReductions, reductionlist = fetchreductions(root, paxlist, context)
        print("Pax Recutions is ########:", paxReductions)
        print("Recutions List ########:", reductionlist)
    else:
        paxReductions, reductionlist = fetchreductions(root, paxlist, context)
        paxReductions = ""
        print("Recutions List ########:", reductionlist)

    if context['IsRoundTrip'] == "Y" and ('TwoStepShopping' in context and context['TwoStepShopping'] == "N"):
        print("################################### VALUES FOR OUTWARD JOURNEY ##########################")
        print("Entered this loop Outward SSL IS: ", segment_data[0]['Outward'][0]['SSL_ID'])
        outwardsolutionData = filtersolutions(root, segment_data[0]['Outward'][0]['SSL_ID'])
        outwardsolutionDict = resolvesolutions(outwardsolutionData)
        print("Outward Solution Dict Is ############### : ", outwardsolutionDict)
        outwardbookingtoken = fetchBookingData(outwardsolutionDict, BookingClass, QuotingRule, paxReductions, context)
        print("Outward Booking Token Matching All criteria is ############### : ", outwardbookingtoken)


        print("################################### VALUES FOR INWARD JOURNEY ##########################")
        print("Entered this loop Return SSL IS: ", segment_data[0]['Inward'][0]['SSL_ID'])
        inwardsolutionData = filtersolutions(root, segment_data[0]['Inward'][0]['SSL_ID'])
        inwardsolutionDict = resolvesolutions(inwardsolutionData)
        print("Inward Solution Dict Is ############### : ", inwardsolutionDict)
        inwardbookingtoken = fetchBookingData(inwardsolutionDict, BookingClass, QuotingRule, paxReductions,context)
        print("Inward Booking Token Matching All criteria is ############### : ", inwardbookingtoken)

        outward_filtered_bkg_data = additionalbookingdata(outwardbookingtoken[0].get('BookingToken'), bkgclasslist, quotingrulelist, reductionlist, outwardsolutionDict)
        inward_filtered_bkg_data = additionalbookingdata(inwardbookingtoken[0].get('BookingToken'), bkgclasslist,
                                                      quotingrulelist, reductionlist, inwardsolutionDict)

         #filtered_bkg_data.append({"Outward": outward_filtered_bkg_data, "Inward": inward_filtered_bkg_data})

        bookingdata.append({"Outward": {"bookingtoken": outwardbookingtoken[0].get('BookingToken'),
                                        "segmentdetails": segment_data[0]['Outward'][0]['SegmentDetails'],
                                        "paxdata": segment_data[0]['Outward'][0]['paxdetails'], "additionalbkgdata" :outward_filtered_bkg_data}, "Inward": {"bookingtoken":
                                        inwardbookingtoken[0].get('BookingToken'), "segmentdetails":
                                        segment_data[0]['Inward'][0]['SegmentDetails'],
                                        "paxdata": segment_data[0]['Inward'][0]['paxdetails'], "additionalbkgdata" : inward_filtered_bkg_data}})
        #print("Filtered Booking Data for Ourward & Inward is: ", filtered_bkg_data)
    elif (context['IsRoundTrip'] == "N") or ((context['IsRoundTrip'] == "Y" and 'TwoStepShopping' in context) and context['TwoStepShopping'] == "Y"):
        SSL_ID = ""
        for sh in root.iter('Schedule'):
            if sh.attrib['Direction'] == "Outward":
                SSL_ID = segment_data[0]['Outward'][0]['SSL_ID']
                print("Entered this loop with SSL_ID: ", SSL_ID)

                solutionData = filtersolutions(root, SSL_ID)
                solutionDict = resolvesolutions(solutionData)
                print("Solution Dict Is ############### : ", solutionDict)
                bookingtoken = fetchBookingData(solutionDict, BookingClass, QuotingRule, paxReductions, context)
                print("Booking Token Matching All criteria is ############### : ", bookingtoken)
                filtered_bkg_data = additionalbookingdata(bookingtoken[0].get('BookingToken'), bkgclasslist,
                                                          quotingrulelist,
                                                          reductionlist,
                                                          solutionDict)
                bookingdata.append({"Outward": {"bookingtoken": bookingtoken[0].get('BookingToken'),
                                                "segmentdetails": segment_data[0]['Outward'][0]['SegmentDetails'],
                                                "paxdata": segment_data[0]['Outward'][0]['paxdetails'], "additionalbkgdata" : filtered_bkg_data}})
                break
            elif sh.attrib['Direction'] == "Inward":
                SSL_ID = segment_data[0]['Inward'][0]['SSL_ID']
                print("Entered this loop with SSL_ID: ", SSL_ID )
                solutionData = filtersolutions(root, SSL_ID)
                solutionDict = resolvesolutions(solutionData)
                print("Solution Dict Is ############### : ", solutionDict)
                bookingtoken = fetchBookingData(solutionDict, BookingClass, QuotingRule, paxReductions,context)
                print("Booking Token Matching All criteria is ############### : ", bookingtoken)
                filtered_bkg_data = additionalbookingdata(bookingtoken[0].get('BookingToken'), bkgclasslist,
                                                          quotingrulelist,
                                                          reductionlist,
                                                          solutionDict)
                bookingdata.append({"Inward": {"bookingtoken": bookingtoken[0].get('BookingToken'),
                                    "segmentdetails": segment_data[0]['Inward'][0]['SegmentDetails'],
                                    "paxdata": segment_data[0]['Inward'][0]['paxdetails'], "additionalbkgdata" : filtered_bkg_data}})
                break
    return bookingdata
