import xml.etree.ElementTree as ET
import itertools
from datetime import date,datetime


def findPaxList(paxDetailsRQ, paxDetailsRS):
    paxlist = []
    for i in range(len(paxDetailsRQ)):
        for j in range(len(paxDetailsRS)):
            if paxDetailsRQ[i]["tattoo"] == paxDetailsRS[j]["PAX_TID"]:
                paxlist.append({"PAX_ID": paxDetailsRS[j]["PAX_ID"], "DOB": paxDetailsRQ[i]["dateOfBirth"]})
    return paxlist
    
def getage(DOB):
    today = date.today()
    DOB = datetime.strptime(DOB, '%Y-%m-%d').date()
    age = today.year - DOB.year - ((today.month, today.day) < (DOB.month, DOB.day))
    return age


def assigncategory(paxlist, inventory):
    if inventory == "FRR":
        for pax in paxlist:
            age = getage(pax.get('DOB'))
            print(age)
            if 26 <= age <= 59:
                print("Inside Adult case")
                pax['CATEGORY'] = "006"
            elif 0 <= age <= 3:
                pax['CATEGORY'] = "001"
            elif 4 <= age <= 11:
                pax['CATEGORY'] = "002"
            elif 60 <= age <= 99:
                pax['CATEGORY'] = "005"
            elif 12 <= age <= 25:
                pax['CATEGORY'] = "003"
            else:
                print("Pax Category Assignment Not Found for FRR")
                break


#assigncategory(paxlist, "FRR")
#print("After Category Assignement: ", paxlist)


def fetchbookinglcass(context,root):
    BookingClass = []
    bkgclasslist = []
    if 'bookingClass' in context:
        for k in root.iter("BookingClass"):
            if context['bookingClass'] == k.attrib['Class']:
                BC_ID = k.attrib['ID']
                BookingClass.append(BC_ID)
                bkgclasslist.append(k.attrib)
                #print("Value for BC: ",k.attrib)

    else:
        for k in root.iter("BookingClass"):
            if '001' == k.attrib['Class']:
                BC_ID = k.attrib['ID']
                BookingClass.append(BC_ID)
                bkgclasslist.append(k.attrib)
    return BookingClass,bkgclasslist


# BookingClass = fetchbookinglcass(context,root)
# print(BookingClass)


def fetchquotingrules(context, root):
    QuotingRule = []
    quotingrulelist = []
    if 'fareCode' in context:
        for l in root.iter("QuotingRule"):
            if l.attrib['Code'] == context['fareCode'] and l.attrib["Flexibility"] == context['flexibility']:
                    QR_ID = l.attrib['ID']
                    QuotingRule.append(QR_ID)
                    quotingrulelist.append(l.attrib)
    else:
        for l in root.iter("QuotingRule"):
            if l.attrib["Flexibility"] == context['flexibility']:
                    QR_ID = l.attrib['ID']
                    QuotingRule.append(QR_ID)
                    quotingrulelist.append(l.attrib)
    return QuotingRule, quotingrulelist

def fetchreductions(root, paxList, context):
    paxReductions = []
    reductionlist = []

    for red in root.iter("Reduction"):
        for n in red.findall("PassengerCategory"):
            reductionlist.append({'Description': red.attrib['Description'], 'ID': red.attrib['ID'], 'paxcatcode': n.attrib['Code']})
        if red[0].tag == "Corporate":
            #print("Value of red list code: ", red[0].get('Code'))
            reductionlist.append(
                {'ID': red.attrib['ID'],'corporatecode': red[0].get('Code'), 'vendorcompcode': red[1].get('Code')})
    for i in range(len(paxList)):
        if paxList[i]["CATEGORY"] == "005":
            reduction_ids_sr = []
            for red in root.iter("Reduction"):
                for n in red.findall("PassengerCategory"):
                    if n.attrib['Code'] == paxList[i]["CATEGORY"]:
                        RED_ID = red.attrib['ID']
                        reduction_ids_sr.append(RED_ID)
        if paxList[i]["CATEGORY"] == "001":
            reduction_ids_inf = []
            for red in root.iter("Reduction"):
                for n in red.findall("PassengerCategory"):
                    if n.attrib['Code'] == paxList[i]["CATEGORY"]:
                        RED_ID = red.attrib['ID']
                        reduction_ids_inf.append(RED_ID)
        if paxList[i]["CATEGORY"] == "002":
            reduction_ids_chd = []
            for red in root.iter("Reduction"):
                for n in red.findall("PassengerCategory"):

                    if n.attrib['Code'] == paxList[i]["CATEGORY"]:
                        RED_ID = red.attrib['ID']
                        reduction_ids_chd.append(RED_ID)
        if paxList[i]["CATEGORY"] == "003":
            reduction_ids_yth = []
            for red in root.iter("Reduction"):
                for n in red.findall("PassengerCategory"):
                    if n.attrib['Code'] == paxList[i]["CATEGORY"]:
                        RED_ID = red.attrib['ID']
                        reduction_ids_yth.append(RED_ID)
        if paxList[i]["CATEGORY"] == "006":
            reduction_ids_adt = []
            for red in root.iter("Reduction"):
                for n in red.findall("PassengerCategory"):
                    if n.attrib['Code'] == paxList[i]["CATEGORY"]:
                        RED_ID = red.attrib['ID']
                        reduction_ids_adt.append(RED_ID)

    if 'reduction_ids_sr' in locals():
        for i in range(len(paxList)):
            if paxList[i]["CATEGORY"] == "005":
                paxReductions.append({"PAXID" : paxList[i]["PAX_ID"],"RED_IDS": reduction_ids_sr,"CATEGORY":"005"})
    if 'reduction_ids_inf' in locals():
        for i in range(len(paxList)):
            if paxList[i]["CATEGORY"] == "001":
                paxReductions.append({"PAXID" : paxList[i]["PAX_ID"], "RED_IDS": reduction_ids_inf,"CATEGORY":"001"})
    if 'reduction_ids_chd' in locals():
        for i in range(len(paxList)):
            if paxList[i]["CATEGORY"] == "002":
                paxReductions.append({"PAXID" :paxList[i]["PAX_ID"], "RED_IDS": reduction_ids_chd,"CATEGORY":"002"})
    if 'reduction_ids_yth' in locals():
        for i in range(len(paxList)):
            if paxList[i]["CATEGORY"] == "003":
                paxReductions.append({"PAXID" :paxList[i]["PAX_ID"], "RED_IDS": reduction_ids_yth,"CATEGORY":"003"})
    if 'reduction_ids_adt' in locals():
        for i in range(len(paxList)):
            if paxList[i]["CATEGORY"] == "006":
                paxReductions.append({"PAXID" : paxList[i]["PAX_ID"],"RED_IDS": reduction_ids_adt,"CATEGORY":"006"})
    if context['paxCategoryDifferent'] == 1:
        print("Inside POP PAX REDS")
        for i in range(len(paxReductions)):
            print("I is : ", i)
            print(len(paxReductions))
            for j in range(i+1, len(paxReductions)):
                print("I Value is: ", paxReductions[i]['CATEGORY'])
                print("I Value is: ", paxReductions[j]['CATEGORY'])
                if paxReductions[i]['CATEGORY'] == paxReductions[j]['CATEGORY']:
                    paxReductions[i]['PAXID'] += " "+paxReductions[j]['PAXID']
                    paxReductions.pop(j)
                if len(paxReductions)-1 == j:
                    break
    print("Pax Reds are : ", paxReductions)
    return paxReductions, reductionlist


# paxReductions = fetchreductions(root, paxlist)
# print("Check Senior List", paxReductions)


def filtersolutions (root, SSL_ID,context):
    solutionData =[]
    if context['paxCategoryDifferent'] == 1 and context['multisegment'] == 'N':
        print("In This Loop pax cat diff is 1 & multi segment is N...")
        for m in root.iter("Solution"):
            if m.attrib['RefIDs'] == SSL_ID:
                for n in m.findall("Quote"):
                        if n.attrib['Type'] == "Fare":
                            for l in n.findall("Product"):
                                if l.attrib['Type'] == "Segment":
                                    solutionData.append({"BookingToken": m.attrib['BookingToken'], "REF_IDS_" + n.attrib['RefIDs']+"_"+l.attrib['ID']: l.attrib[
                                                                 'RefIDs'], "SSL_ID": m.attrib['RefIDs'],
                                                                 "PaxID": n.attrib['RefIDs']})
    elif context['multisegment'] == 'Y' and context['paxCategoryDifferent'] == 0:
        print("In This Loop Y & 0...")
        for m in root.iter("Solution"):
            if m.attrib['RefIDs'] == SSL_ID:
                for n in m.findall("Quote"):
                        if n.attrib['Type'] == "Fare":
                            for l in n.findall("Product"):
                                if l.attrib['Type'] == "Segment":
                                    solutionData.append({"BookingToken": m.attrib['BookingToken'],
                                                         "REF_IDS_" + l.attrib['ID']: l.attrib[
                                                        'RefIDs'], "SSL_ID": m.attrib['RefIDs'],
                                                        "PaxID": n.attrib['RefIDs']})
    elif context['multisegment'] == 'Y' and context['paxCategoryDifferent'] == 1:
        print("In This Loop Y and 1 ...")
        for m in root.iter("Solution"):
            if m.attrib['RefIDs'] == SSL_ID:
                for n in m.findall("Quote"):
                        if n.attrib['Type'] == "Fare":
                            for l in n.findall("Product"):
                                    if l.attrib['Type'] == "Segment":
                                        solutionData.append({"BookingToken": m.attrib['BookingToken'],
                                                            "REF_IDS_" + n.attrib['RefIDs']+"_"+l.attrib['ID']:
                                                                l.attrib['RefIDs'], "SSL_ID": m.attrib['RefIDs']
                                                                , "PaxID": n.attrib['RefIDs']})
    else:
        for m in root.iter("Solution"):
            if m.attrib['RefIDs'] == SSL_ID:
                for n in m.findall("Quote"):
                    if n.attrib['Type'] == "Fare":
                        for l in n.findall("Product"):
                            if l.attrib['Type'] == "Segment":
                                solutionData.append({"BookingToken": m.attrib['BookingToken']
                                                            ,"REF_IDS_"+n.attrib['RefIDs']:
                                                                l.attrib['RefIDs'], "SSL_ID": m.attrib['RefIDs']
                                                                    , "PaxID": n.attrib['RefIDs']})
    return solutionData

# SSL_ID = "SSL_256"
# solutionData = filtersolutions(root, SSL_ID)
#
# print("Test 1 :", solutionData)

def resolvesolutions(solutionData):
    update_s_dict = {}
    for s in solutionData:
        if s['BookingToken'] not in update_s_dict:
            update_s_dict[s['BookingToken']] = {}
    # print(update_s_dict)
    i = 1
    while len(solutionData) != 0:
        solution = solutionData.pop(0)
        update_s_dict[solution['BookingToken']]['PaxID_' + str(i)] = solution.get('PaxID', "")
        if solution['BookingToken'] == update_s_dict[solution['BookingToken']].get('BookingToken'):
            i += 1
        else:
            i = 1
        del solution["PaxID"]
        update_s_dict[solution['BookingToken']].update(solution)
    return update_s_dict


# solutionDict = resolvesolutions(solutionData)
#
# print("Updated solution Dict is : ", solutionDict)


def fetchBookingData(solutionDict, BookingClass, QuotingRule, paxReductions,context):
    print("inside fetchBookingData")
    bookingData = []
    if context['paxCategoryDifferent'] == 0 and context['multisegment'] == 'Y' or context['multisegment'] == 'N':
        for key, value in solutionDict.items():
            check, check2 = False, False
            for key1, value1 in value.items():
                if "REF_IDS" in key1:
                    listRefs = value1.split(" ")
                    print(listRefs)
                    check = any(item in BookingClass for item in listRefs)
                    if check:
                        print("Inside Booking Class Check")
                        check2 = any(item in QuotingRule for item in listRefs)

                    if not check or not check2:
                        break
            if check and check2:
                print("Checks for Booking class and QR CODE done")
                bookingData.append({"BookingToken": key})
    elif context['paxCategoryDifferent'] == 1 and context['multisegment'] == 'Y' or context['multisegment'] == 'N':
        for key, value in solutionDict.items():
            check, check2, check3 = False, False, False
            for key1, value1 in value.items():
                if "REF_IDS" in key1:
                    listRefs = value1.split(" ")
                    print(listRefs)
                    check = any(item in BookingClass for item in listRefs)
                    if check:
                        print("Inside Booking Class Check")
                        check2 = any(item in QuotingRule for item in listRefs)
                        if check2:
                            print("Inside Quoting Rule Check")
                            for pax in paxReductions:
                                if pax.get("PAXID") in key1:
                                    pax.get('RED_IDS')
                                    check3 = any(item in pax.get('RED_IDS') for item in listRefs)

                    if not check or not check2 or not check3:
                        break
            if check and check2 and check3:
                print("Checks for Booking class, QR CODE  and Reduction code done")
                testAll = "Passed"
                bookingData.append({"BookingToken": key})
    return bookingData




def findMatchSeg(root,context):  
    SEG_ID = ""
    Seg_railID = ""
    start_loc_name = ""
    end_loc_name = ""
    start_date_time = ""
    end_date_time = ""
    startlocation = ""
    endlocation = ""
    for sg in root.iter("Segment"):
        if sg[0].find('locationCode').text.strip("\n") == context['startlocation'] and sg[1].find('locationCode').text.strip("\n")==(context['endlocation']):
            if ('trainType' in context):
                if sg.find('vehicle').attrib['Category'] == context['trainType']:
                    SEG_ID = sg.attrib['ID']
                    Seg_railID = sg.find('identifier').text.strip("\n")
                    start_loc_name = sg.find('start').attrib['locationName']
                    end_loc_name = sg.find('end').attrib['locationName']
                    start_date_time = sg.find('start').attrib['dateTime']
                    end_date_time = sg.find('end').attrib['dateTime']
                    startlocation = sg[0].find('locationCode').text.strip("\n")
                    endlocation = sg[1].find('locationCode').text.strip("\n")
                break
            else:
                SEG_ID = sg.attrib['ID']
                Seg_railID = sg.find('identifier').text.strip("\n")
                start_loc_name = sg.find('start').attrib['locationName']
                end_loc_name = sg.find('end').attrib['locationName']
                start_date_time = sg.find('start').attrib['dateTime']
                end_date_time = sg.find('end').attrib['dateTime']
                startlocation = sg[0].find('locationCode').text.strip("\n")
                endlocation = sg[1].find('locationCode').text.strip("\n")
                break
    return (SEG_ID, Seg_railID, start_loc_name, end_loc_name, start_date_time, end_date_time, startlocation, endlocation)



def findMatchingSSl(root,SEG_ID,context):
    global SSL_ID
    context_main= context
    New_SEGID = list()
    if context_main['multisegment']=='Y':
        for j in root.iter("Solution"):
            if ('SEG_' in j.attrib['RefIDs'] and ' ' in j.attrib['RefIDs'] and j.attrib['RefIDs'] == SEG_ID):
                New_SEGID.append({"SSL_ID": j.attrib['ID'], "SEG_ID": j.attrib['RefIDs']})
                SSL_ID = j.attrib['ID']
    else:
       for j in root.iter("Solution"):
            if (SEG_ID in j.attrib['RefIDs']):
                SSL_ID=j.attrib['ID']
                break
    return SSL_ID


def findPaxIds(root, context):
    listpax = list()
    for pax in root.iter('Passenger'):
        listpax.append({"PAX_ID":pax.attrib['ID'],"PAX_TID":pax.attrib['TID']})
    print (listpax)
    return listpax


def findsegmentdetails(context, file_path):
    global SSL_ID
    SSL_ID = ""
    global Outward_SegID
    global Return_SegID
    global Seg_railID
    Outward_SegID = ""
    Return_SegID = ""
    Inward_SegID = list()
    New_SEGID = list()
    list_SSLs = list()
    Outward_SSLID = ""
    startlocation = ""
    endlocation = ""
    vehicleCate = ""
    root = ""
    list_Segments = list()
    listpax = list()

    Seg_railID = ""
    list_seg_dict=list()
    ctx_keys=context.keys()
    list_seg_dict=ctx_keys
    #print(list_seg_dict)
    tree = ET.parse(file_path)
    root = tree.getroot()
    count_out=0
    count_in=0
    count_out_seg = 0
    count_return_seg = 0
    context_main = context
    for it in list_seg_dict:
        if (it.startswith('Outward_')):
            count_out_seg=count_out_seg+1
        elif (it.startswith('Return_')):
            count_return_seg=count_return_seg+1

    print("Outward Segment COunt : ", count_out_seg)
    print("Return Segment COunt : ", count_return_seg)

    listpax = findPaxIds(root, context_main)
    for it in sorted(list_seg_dict):
        #seg_val=it
        if ( 'TwoStepShopping' in context_main and context_main['TwoStepShopping'] == 'N'):
            if (it.startswith('Outward_')):
                if count_out < count_out_seg:
                    #print(it + "  Starts with outward")
                    if(it in context_main):
                        context = context_main[it]
                        #print("NEW CONTEXT SELETED : ", context)
                    SEG_ID, Seg_railID, start_loc_name, end_loc_name, start_date_time, end_date_time, startlocation, endlocation = findMatchSeg(root, context)
                    if Outward_SegID != "":
                        Outward_SegID = Outward_SegID + " " + SEG_ID
                    else:
                        Outward_SegID = SEG_ID
                    count_out=count_out+1
                    list_Segments.append({"Outward_Seg": SEG_ID, "Seg_Rail_ID":Seg_railID,"start_loc_name":start_loc_name, "end_loc_name":end_loc_name, "start_date_time":start_date_time, "end_date_time":end_date_time, "startlocation":startlocation, "endlocation": endlocation})
    print("Segment_List",list_Segments)
    Outward_SSLID=findMatchingSSl(root,Outward_SegID,context_main)
    list_SSLs.append({"Outward_SSLID": Outward_SSLID})

    print("Outward_SSL",Outward_SSLID)
    #print(list_Segments)

        #Else if Teostepsho Y
    for it in sorted(list_seg_dict):
        #seg_val=it
        if ( 'TwoStepShopping' in context_main and context_main['TwoStepShopping'] == 'N'):
            if (it.startswith('Return_')):
                if count_in < count_return_seg:
                    #print(it + "  Starts with Return")
                    if(it in context_main):
                        context = context_main[it]
                        #print("NEW CONTEXT SELETED : ", context)
                    SEG_ID, Seg_railID, start_loc_name, end_loc_name, start_date_time, end_date_time, startlocation, endlocation = findMatchSeg(root, context)

                    if Return_SegID != "":
                        Return_SegID = Return_SegID + " " + SEG_ID
                    else:
                        Return_SegID = SEG_ID

                    count_in=count_in+1
                    list_Segments.append({"Return_Seg": SEG_ID, "Seg_Rail_ID": Seg_railID,"start_loc_name":start_loc_name, "end_loc_name":end_loc_name, "start_date_time":start_date_time, "end_date_time":end_date_time, "startlocation":startlocation, "endlocation": endlocation})
                    #print(Return_SegID)
    print(list_Segments)
    Return_SSLID = findMatchingSSl(root, Return_SegID, context_main)
    print("Return_SSL", Return_SSLID)
    list_SSLs.append({"Return_SSLID": Return_SSLID})
    result ={"segmentlist": list_Segments, "SSL_ID": list_SSLs, "paxlist": listpax}
    return result

def updatedsegdata(segment_list):
    outward_segment_data = []
    inward_segment_data = []
    pax_data = []
    for key,value in segment_list.items():
        #print("Key is :", key)
        #print("Value is :", value)
        if key == "segmentlist":
            segmentlist = value
            #print("Value for segments are: ", segmentlist)
            for seg in segmentlist:
                if "Outward_Seg" in seg:
                    outward_segment_data.append({"SegmentID": seg.get("Outward_Seg"), "TrainID": seg.get("Seg_Rail_ID"),"start_loc_name":seg.get("start_loc_name"), "end_loc_name":seg.get("end_loc_name"), "start_date_time":seg.get("start_date_time"), "end_date_time":seg.get("end_date_time"), "startlocation":seg.get("startlocation"), "endlocation": seg.get("endlocation")})
                elif "Return_Seg" in seg:
                    inward_segment_data.append({"SegmentID": seg.get("Return_Seg"), "TrainID": seg.get("Seg_Rail_ID"),"start_loc_name":seg.get("start_loc_name"), "end_loc_name":seg.get("end_loc_name"), "start_date_time":seg.get("start_date_time"), "end_date_time":seg.get("end_date_time"), "startlocation":seg.get("startlocation"), "endlocation": seg.get("endlocation")})
        elif key == "paxlist":
            paxlist = value
            for pax in paxlist:
                pax_data.append({"PaxID": pax.get("PAX_ID")})
    return outward_segment_data, inward_segment_data, pax_data

def filterbookingdata(bookingtoken,bkgclasslist, quotingrulelist, reductionlist,solutuiondict):
    for key, value in solutuiondict.items():
        if key == bookingtoken:
            filteredsolution = value
            break
    print("Filtered Solution data is : ", filteredsolution)
    filtered_booking_list = []
    filtered_quoting_list = []
    filtered_reduction_list = []
    bookinginputlist = []
    for key, value in filteredsolution.items():
        if "REF_IDS" in key:
            listRefs = value.split(" ")

            for i in range(len(listRefs)):

                for n in range(len(bkgclasslist)):
                    if listRefs[i] == bkgclasslist[n]['ID']:
                        #if bkgclasslist[n]['ID'] != filtered_booking_list[n-1]['ID']:
                        filtered_booking_list.append(bkgclasslist[n])

                for n in range(len(quotingrulelist)):
                    if listRefs[i] == quotingrulelist[n]['ID']:
                        filtered_quoting_list.append(quotingrulelist[n])

                for n in range(len(reductionlist)):
                    if listRefs[i] == reductionlist[n]['ID']:
                        filtered_reduction_list.append(reductionlist[n])

    bookinginputlist.append(
        {"filtered_booking_list": filtered_booking_list, "filtered_quoting_list": filtered_quoting_list,
         "filtered_reduction_list": filtered_reduction_list})

    return bookinginputlist

#def filterbookingdata(bookingtoken,bkgclasslist, quotingrulelist, reductionlist)

def MultiDestScheduleQuoteRS2(res,context,file_path, inventory,paxDetailsRQ):
    file_path = "temp.xml"
    res=res.replace('rail:','')
    f = open("temp.xml", "w")
    L = ["<AMA_RailMultiDestScheduleQuoteRS>", res]
    f.writelines(L)
    print "Response is: "+res
    f.close()

    with open("temp.xml", 'r+') as f:
        s = f.read()
        s = s.replace('>', '>\n')
        f.seek(0,0)
        f.write(s)
    bookingdata= []
    filtered_bkg_data = []
    ssl_details = findsegmentdetails(context, file_path)
    print("SSL Details are: ", ssl_details)
    tree = ET.parse(file_path)
    root = tree.getroot()
    paxlist = findPaxList(paxDetailsRQ, ssl_details['paxlist'])
    print(paxlist)
    assigncategory(paxlist, inventory)
    BookingClass,bkgclasslist = fetchbookinglcass(context, root)
    print("List of Booking Class: ",bkgclasslist)
    QuotingRule, quotingrulelist = fetchquotingrules(context, root)
    print("List of Quoting Rules: ", quotingrulelist)
    if context['paxCategoryDifferent'] == 1:
        paxReductions, reductionlist = fetchreductions(root, paxlist,context)
        print("Pax Recutions is ########:", paxReductions)
        print("Recutions List ########:", reductionlist)
    else:
        paxReductions, reductionlist = fetchreductions(root, paxlist,context)
        paxReductions = ""
    if context['TwoStepShopping'] == "N" and context["IsRoundTrip"] == "Y":
        print("Entered Single Step Round Trip")
        val = ssl_details.get('SSL_ID')
        print ("The Val of Val is :", val)
        # print(type(val))

        print("################################### VALUES FOR OUTWARD JOURNEY ##########################")
        print("Entered this loop Outward SSL IS: ", val[0].get('Outward_SSLID'))
        outwardsolutionData = filtersolutions(root, val[0].get('Outward_SSLID'),context)
        outwardsolutionDict = resolvesolutions(outwardsolutionData)
        print("Outward Solution Dict Is ############### : ", outwardsolutionDict)
        outwardbookingtoken = fetchBookingData(outwardsolutionDict, BookingClass, QuotingRule, paxReductions,context)
        print("Outward Booking Token Matching All criteria is ############### : ", outwardbookingtoken)

        print("################################### VALUES FOR INWARD JOURNEY ##########################")
        print("Entered this loop Return SSL IS: ", val[1].get('Return_SSLID'))
        inwardsolutionData = filtersolutions(root, val[1].get('Return_SSLID'), context)
        inwardsolutionDict = resolvesolutions(inwardsolutionData)
        print("Inward Solution Dict Is ############### : ", inwardsolutionDict)
        inwardbookingtoken = fetchBookingData(inwardsolutionDict, BookingClass, QuotingRule, paxReductions,context)
        print("Inward Booking Token Matching All criteria is ############### : ", inwardbookingtoken)

        outward_segment_data, inward_segment_data, pax_data = updatedsegdata(ssl_details)

        outward_filtered_bkg_data = filterbookingdata(outwardbookingtoken[0].get('BookingToken'), bkgclasslist, quotingrulelist, reductionlist, outwardsolutionDict)
        inward_filtered_bkg_data = filterbookingdata(inwardbookingtoken[0].get('BookingToken'), bkgclasslist,
                                                      quotingrulelist, reductionlist, inwardsolutionDict)

        filtered_bkg_data.append({"Outward": outward_filtered_bkg_data,"Inward": inward_filtered_bkg_data})

        print("Filtered Booking Data for Ourward & Inward is: ", filtered_bkg_data)

        #outward_segment_data = outward_segment_data.list()
        print("OUTWARD DATA: ",outward_segment_data)
        print("################# ADDING VALUES TO BOOKING DATA VARIABLE ##################")
        bookingdata.append({"Outward": {"bookingtoken": outwardbookingtoken[0].get('BookingToken'),
                                        "segmentdetails": outward_segment_data,
                                        "paxdata": pax_data}, "Inward": {"bookingtoken":
                                        inwardbookingtoken[0].get('BookingToken'), "segmentdetails":
                                        inward_segment_data, "paxdata": pax_data}})


        #print("Booking Data is :", bookingdata)
    else:
        val = ssl_details.get('SSL_ID')
        #print("Entered this loop: ", val[0].get('Outward_SSLID'))
        print("Entered this loop: ", val[0].get('Outward_SSLID'))
        print(type(val))
        solutionData = filtersolutions(root, val[0].get('Outward_SSLID'),context)
        print "##########Solution Data is############ :", solutionData
        solutionDict = resolvesolutions(solutionData)
        print("Solution Dict Is ############### : ", solutionDict)
        bookingtoken = fetchBookingData(solutionDict, BookingClass, QuotingRule, paxReductions,context)
        print("Booking Token Matching All criteria is ############### : ", bookingtoken)
        outward_segment_data, inward_segment_data, pax_data = updatedsegdata(ssl_details)
        bookingdata.append({"Outward": {"bookingtoken": bookingtoken[0].get('BookingToken'),
                                        "segmentdetails": outward_segment_data,
                                        "paxdata": pax_data}})
        filtered_bkg_data = filterbookingdata(bookingtoken[0].get('BookingToken'), bkgclasslist, quotingrulelist, reductionlist,
                          solutionDict)
        print("Main Data is: ",filtered_bkg_data)
        print("Main Data is: ",bookingdata)