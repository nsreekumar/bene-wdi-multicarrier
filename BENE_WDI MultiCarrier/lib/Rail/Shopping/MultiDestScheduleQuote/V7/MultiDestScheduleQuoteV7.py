import jinja2, os, sys


MultiDestScheduleQuote="""<AMA_RailMultiDestScheduleQuoteRQ xmlns="http://xml.amadeus.com/2010/06/RailShopping_v2" xmlns:rail="http://xml.amadeus.com/2010/06/RailCommonTypes_v3" CorrelationID="2U9vQe298fCGxBjtwp5gIfvPpexGAFB6KvJ_x-WbzbU6r9xwPzQW" Distributor="{{ myContext["distributor"] }}" PrimaryLangID="EN" ReplyOptions="NoCache Services Descriptions" Version="7.0">
    {% for schedule in journey %}
    {% set scheduleLoop = loop %}
    <Schedule TID="{{ scheduleLoop.index }}">
    <rail:start dateTime="{{ schedule["startDate"] }}">
        <rail:locationCode type="{{  schedule["locationCodeType"] }}">{{ schedule["origin"] }}</rail:locationCode>
    </rail:start>
    <rail:end{% if schedule["endDate"] %} dateTime="{{ schedule["endDate"] }}"{% endif %}>
        <rail:locationCode type="{{  schedule["locationCodeType"] }}">{{ schedule["destination"] }}</rail:locationCode>
    </rail:end>
    <rail:SearchCriteria {% if schedule["maxChanges"] %} maxChanges="{{ schedule["maxChanges"] }}" {% endif %} />    
    </Schedule>
    {% endfor %}
    {% for passenger in passengerList %}
    <Passenger TID="{{ passenger["tattoo"] }}" {% if passenger["dateOfBirth"] %} BirthDate="{{ passenger["dateOfBirth"] }}Z"{% endif %}/>
    {% if passenger["corporateCode"] or passenger["vendorCompany"] or passenger["railCard"] %}
        <Reduction>
            {% if passenger["corporateCode"] %}
            <Corporate Code="{{ passenger["corporateCode"] }}"></Corporate>
            {% endif %}
            {% if passenger["railCard"] %}
            <Railcard Code="{{ passenger["railCard"] }}"></Railcard>
            {% endif %}
            {% if passenger["vendorCompany"] %}
            <vendorCompany Code="{{ passenger["vendorCompany"] }}" context="1A" />
            {% endif %}
        </Reduction>
    {% endif%}

    {% endfor %}
    {% if myContext["quotingrule"] %}
    <QuotingRuleOption Family="{{ quotingrule["family"] }}" PreferLevel="{{ quotingrule["preferlevel"] }}"/>
    {% endif %}
    {% for schedule in journey %}
    {% set scheduleLoop = loop %}
    <InventorySystem Code="{{ schedule["inventorySystem"] }}" Context="1A"/>
    {% endfor %}
    {% if myContext["additionalData"] or twoStep %}
    <AdditionalData>
    {% for addData in myContext["additionalData"] %}
    {% if addData["providerOffice"] %}
    <Data Name="ProviderOfficeId">{{ addData["providerOffice"] }}</Data>
    {% endif %}
    {% endfor %}
    {% if twoStep %}
    <Data Name="OutwardPricingSolutionId">{{ twoStep["bookingToken"] }}</Data>
    {% endif %}
    </AdditionalData>
    {% endif %}
</AMA_RailMultiDestScheduleQuoteRQ>"""


GetAccommodations="""<AMA_RailGetAccommodationsRQ Version="5.4" PrimaryLangID="{{ myContext["language"] }}" xmlns="http://xml.amadeus.com/RAI/2009/10">
    <POS>
        <Source>
            <RequestorID Type="30" ID_Context="1A" ID="{{ myContext["officeID"] }}"/>
        </Source>
    </POS>
    <ProposalRPH RPH="{{ myProposal }}"/>
</AMA_RailGetAccommodationsRQ>"""


def MultiDestScheduleQuoteV7(context,*args):
    loader = jinja2.FileSystemLoader(os.getcwd())
    jenv = jinja2.Environment(trim_blocks=True, lstrip_blocks=True)
    t2 = jenv.from_string(MultiDestScheduleQuote)
    if args:
        for i in args:
            return "".join(map(unicode.strip,t2.render(twoStep=i,myContext=context,journey=context["journey"],passengerList=context["passengerList"]).splitlines()))
    else:
            return "".join(map(unicode.strip,t2.render(myContext=context,journey=context["journey"],passengerList=context["passengerList"]).splitlines()))


def GetAccommodationsV5(context, proposal):
    loader = jinja2.FileSystemLoader(os.getcwd())
    jenv = jinja2.Environment(trim_blocks=True, lstrip_blocks=True)
    t2 = jenv.from_string(GetAccommodations)
    return "".join(map(unicode.strip,t2.render(myContext=context,myProposal=proposal).splitlines()))
 






