import xml.etree.ElementTree as ET
import re

def multiSeg():    
    tree = ET.parse("temp.xml")
    root = tree.getroot()
    for i in root.iter("Solution"):
        if (" " in i.attrib['RefIDs']):
            seg_ID = i.attrib['RefIDs']
            SSL_ID = i.attrib['ID']
            break;
    
    return SSL_ID
    
def singleSeg():
    tree = ET.parse("temp.xml")
    root = tree.getroot()
    dur = {}
    for i in root.iter("Segment"):
       dur[i.attrib['ID']] = i.attrib['Duration']
    
    for seg_ID in dur:
        if (dur[seg_ID] == min(dur.values())):
            break;
    
    for j in root.iter("Solution"):
        if (seg_ID in j.attrib['RefIDs']):
            SSL_ID = j.attrib['ID']
            
    return SSL_ID
    
def MultiDestScheduleQuoteRS(res,context):
    res=res.replace('rail:','')
    f = open("temp.xml", "w")
    L = ["<AMA_RailMultiDestScheduleQuoteRS Version=\"7.0\" CorrelationID=\"EPY4ABLHVHZP0RGM#TQSLYG#91\" Distributor=\"DBA\" ReplyOptions=\"Descriptions NoCache Services\">", res]
    f.writelines(L)
    f.close()

    with open("temp.xml", 'r+') as f:
        s = f.read()
        s = s.replace('>', '>\n')
        f.seek(0,0)
        f.write(s)

    tree = ET.parse("temp.xml")
    root = tree.getroot()
    
    if (context['multisegment'] == 'Y'):
        SSL_ID = multiSeg()
    else:
        SSL_ID = singleSeg()
        
    if ('bookingClass' in context):
        for k in root.iter("BookingClass"):
            if (context['bookingClass'] in k.attrib['Description']):
                BC_ID = k.attrib['ID']
    else:
        for k in root.iter("BookingClass"):
            if ('1st' in k.attrib['Description']):
                BC_ID = k.attrib['ID']
              
    if ('fareCode' in context):          
        for l in root.iter("QuotingRule"):
            if (l.attrib['Code'] == context['fareCode'] and l.attrib["Flexibility"] == context['flexCode']):
                FC_ID = l.attrib['ID']
                
    elif ('fareDesc' in context):       
        for l in root.iter("QuotingRule"):
            if (l.attrib['Description'] == context['fareDesc'] and l.attrib["Flexibility"] == context['flexCode']):
                FC_ID = l.attrib['ID']
                
    else:
         for l in root.iter("QuotingRule"):
            if ("PREM" in l.attrib['Description'] and l.attrib["Flexibility"] == context['flexCode']):
                FC_ID = l.attrib['ID']
                
    b_dic = {}
    
    for m in root.iter("Solution"):
        l =(m.findall("Quote/Product"))

        for e in l:
            if (m.attrib['RefIDs'] == SSL_ID and (BC_ID in e.attrib['RefIDs'] or FC_ID in e.attrib['RefIDs'])):
                BToken = m.attrib['BookingToken']

    result = {"SSL ID":SSL_ID,"Booking Class ID": BC_ID,"Fare Class ID": FC_ID, "Booking Token":BToken}
    
    return result
    

    