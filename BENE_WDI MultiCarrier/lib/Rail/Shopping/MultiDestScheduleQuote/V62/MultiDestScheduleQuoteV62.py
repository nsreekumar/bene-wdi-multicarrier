import jinja2, os, sys

MultiDestScheduleQuote="""<AMA_RailMultiDestScheduleQuoteRQ Version="6.2" Distributor="{{ myContext["distributor"] }}" ReplyOptions="NoCache">
    {% for schedule in journey %}
    {% set scheduleLoop = loop %}
    <Schedule TID="{{ scheduleLoop.index }}">
    <start dateTime="{{ journey[schedule][0]["startDate"] }}">
        <locationCode type="{{  journey[schedule][0]["locationCodeType"] }}">{{ journey[schedule][0]["origin"] }}</locationCode>
    </start>
    <end{% if journey[schedule][-1]["endDate"] %} dateTime="{{ journey[schedule][-1]["endDate"] }}"{% endif %}>
        <locationCode type="{{  journey[schedule][-1]["locationCodeType"] }}">{{ journey[schedule][-1]["destination"] }}</locationCode>
    </end>
    </Schedule>
    {% endfor %}
    {% for passenger in passengerList %}
    <Passenger TID="{{ passenger["tattoo"] }}" {% if passenger["dateOfBirth"] %} BirthDate="{{ passenger["dateOfBirth"] }}"{% endif %}>
        {% if passenger["corporateCode"] %}
        <Reduction>
            <Corporate Code="{{ passenger["corporateCode"] }}"></Corporate>
        </Reduction>
        {% endif %}
    </Passenger>
    {% endfor %}
    {% for schedule in journey %}
    {% set scheduleLoop = loop %}
    <InventorySystem Code="{{ journey[schedule][0]["inventorySystem"] }}" Context="1A"/>
    {% endfor %}
</AMA_RailMultiDestScheduleQuoteRQ>"""

GetAccommodations="""<AMA_RailGetAccommodationsRQ Version="5.4" PrimaryLangID="{{ myContext["language"] }}" xmlns="http://xml.amadeus.com/RAI/2009/10">
    <POS>
        <Source>
            <RequestorID Type="30" ID_Context="1A" ID="{{ myContext["officeID"] }}"/>
        </Source>
    </POS>
    <ProposalRPH RPH="{{ myProposal }}"/>
</AMA_RailGetAccommodationsRQ>"""

def MultiDestScheduleQuoteV62(context):
    loader = jinja2.FileSystemLoader(os.getcwd())
    jenv = jinja2.Environment(trim_blocks=True, lstrip_blocks=True)
    t2 = jenv.from_string(MultiDestScheduleQuote)
    return "".join(map(unicode.strip,t2.render(myContext=context,journey=context["journey"],passengerList=context["passengerList"]).splitlines()))

def GetAccommodationsV5(context, proposal):
    loader = jinja2.FileSystemLoader(os.getcwd())
    jenv = jinja2.Environment(trim_blocks=True, lstrip_blocks=True)
    t2 = jenv.from_string(GetAccommodations)
    return "".join(map(unicode.strip,t2.render(myContext=context,myProposal=proposal).splitlines()))
