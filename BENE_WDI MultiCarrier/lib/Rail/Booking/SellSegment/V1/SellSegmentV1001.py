import jinja2, os, sys

SellSegment = """<AMA_RailSellSegmentRQ Version="1.001" CorrelationID="{{ myContext["corrID"] }}" xsi:schemaLocation="http://xml.amadeus.com/2010/06/RailBooking_v1 AMA_RailSellSegmentRQ.xsd" xmlns="http://xml.amadeus.com/2010/06/RailBooking_v1" xmlns:ota="http://www.opentravel.org/OTA/2003/05/OTA2010B" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:rail="http://xml.amadeus.com/2010/06/RailCommonTypes_v2">
    <Passenger>
        {% for passenger in passengerList %}
        {% set paxLoop = loop %}
        <Actor TID="PAX_{{ paxLoop.index }}" {% if passenger["dateOfBirth"] %}DateOfBirth="{{ passenger["dateOfBirth"] }}"{% endif %}>
            <rail:Name FirstName="{{ passenger["firstName"] }}" LastName="{{ passenger["lastName"] }}"/>
        </Actor>
        {% endfor %}
        {% for passenger in passengerList %}
        {% set paxLoop = loop %}
        {% if passenger["phone"] %}
        <Contact TID="CNT_{{ paxLoop.index }}" RefTIDs="PAX_{{ paxLoop.index }}">
        <rail:Telephone Label="00{{ paxLoop.length + paxLoop.index }}">{{ passenger["phone"] }}</rail:Telephone>
         </Contact>
         {% endif %}
         {% endfor %}
        {% for passenger in passengerList %}
        {% set paxLoop = loop %}
        {% if passenger["emailAddress"] %}
        <Contact TID="CNT_{{ paxLoop.index + 10 }}">
            <rail:Email>{{ passenger["emailAddress"] }}</rail:Email>
         </Contact>
         {% endif %}
         {% endfor %}
        {% for passenger in passengerList %}
        {% set paxLoop = loop %}
        {% for loyalty in passenger["loyaltyCards"] %}
        {% set loyaltyLoop = loop %}
		<Contact TID="CNT_{{ paxLoop.index + loyaltyLoop.index + 10 }}" RefTIDs="PAX_{{ paxLoop.index }}">
		<rail:CustomerLoyalty MembershipID="{{ loyalty["number"] }}" ProgramID="{{ loyalty["programID"] }}">
		   	<rail:VendorCompany Code="{{ loyalty["vendorCompany"] }}"></rail:VendorCompany>
		</rail:CustomerLoyalty>
		</Contact>
         {% endfor %}
         {% endfor %}
    </Passenger>
    <Reservation>
        {% for schedule in journey %}
        {% set scheduleLoop = loop %}
        <Schedule TID="SCH_{{ scheduleLoop.index }}">
            <rail:start dateTime="{{ journey[schedule][0]["startDate"] }}">
            <rail:locationCode type="{{ journey[schedule][0]["locationCodeType"] }}">{{ journey[schedule][0]["origin"] }}</rail:locationCode>
            </rail:start>
            <rail:end{% if journey[schedule][-1]["endDate"] %} dateTime="{{ journey[schedule][-1]["endDate"] }}"{% endif %}>
                <rail:locationCode type="{{ journey[schedule][-1]["locationCodeType"] }}">{{ journey[schedule][-1]["destination"] }}</rail:locationCode>
            </rail:end>
        {% for trip in journey[schedule] %}
        {% set tripLoop = loop %}
        <Segment Inventory="{{ trip["inventorySystem"] }}" TID="{{ trip["segmentTID"] }}">
                <rail:start dateTime="{{ trip["startDate"] }}">
                    <rail:locationCode type="{{ trip["locationCodeType"] }}">{{ trip["origin"] }}</rail:locationCode>
                </rail:start>
                <rail:end{% if trip["endDate"] %} dateTime="{{ trip["endDate"] }}"{% endif %}>
                    <rail:locationCode type="{{ trip["locationCodeType"] }}">{{ trip["destination"] }}</rail:locationCode>
                </rail:end>
                <rail:serviceProvider Code="{{ trip["serviceProvider"] }}"/>
                <rail:identifier>{{ trip["trainNumber"] }}</rail:identifier>
            </Segment>
        {% endfor %}
        </Schedule>
        {% endfor %}
        <Pricing>
		    {% for passenger in passengerList %}
		    {% set paxLoop = loop %}
             <rail:Reduction Description="{{ passenger["PassengerCategoryType"] }}" TID="RED_{{ paxLoop.index }}">
                    <rail:PassengerCategory Code="{{ passenger["PassengerCategoryCode"] }}" Context="{{ passenger["PassengerCategoryContext"] }}"/>
            </rail:Reduction>
			{% endfor %}
		    {% for pricing in pricingList %}
		    {% set pricingLoop = loop %}
            {% if pricing["corporateCode"] %}
            <rail:Reduction TID="RED_10">
                 <rail:Corporate Code="{{ pricing["corporateCode"] }}"/>
            </rail:Reduction>
            {% endif %}
            <rail:BookingClass Code="{{ pricing["bookingClass"] }}" TID="{{ pricing["bookingClassTID"] }}" {% if pricing["bookingClassClass"] %} Class="{{ pricing["bookingClassClass"] }}"{% endif %} {% if pricing["bookingClassAccomType"] %}AccomType="{{ pricing["bookingClassAccomType"] }}"{% endif %} {% if pricing["bookingClassMainClass"] %}MainClass="{{ pricing["bookingClassMainClass"] }}"{% endif %}/>
            {% endfor %}
		    {% for pricing in pricingList %}
		    {% set pricingLoop = loop %}
		    <rail:QuotingRule Code="{{ pricing["quotingRule"] }}" Type="{{ pricing["quotingRuleType"] }}" TID="{{ pricing["quotingRuleTID"] }}" {% if pricing["quotingRuleFamily"] %}Family="{{ pricing["quotingRuleFamily"] }}"{% endif %} {% if pricing["quotingRuleFlexibility"] %}Flexibility="{{ pricing["quotingRuleFlexibility"] }}"{% endif %}/>
			{% endfor %}
			<rail:TicketingOption Type="{{ myContext["ticketingOption"]["type"] }}" DistributionType="{{ myContext["ticketingOption"]["distributionType"] }}" TID="TKO_99"/>
            {% for pricing in pricingList %}
		    {% set pricingLoop = loop %}
            <rail:Quote RefTIDs="{{ pricing["paxAssociation"] }}">
            <rail:Product Type="Segment" RefTIDs="{{ pricing["bookingClassTID"] }} {{ pricing["quotingRuleTID"] }} {{ pricing["paxAssociation"]|replace("PAX", "RED") }} {{ pricing["segmentAssociation"] }} {% if pricing.corporateCode %} RED_10{% endif %}"/>
            </rail:Quote>
			{% endfor %}
        </Pricing>
    </Reservation>
    {% if myContext["multiInventory"] %} 
    <AdditionalData> 
        {% for schedule in journey %}
        {% for trip in journey[schedule] %}
        <rail:Data TID="{{ trip["additionalDataTID"] }}" Name="WEBSVCAPI" RefTIDs="{{ trip["segmentTID"] }}">{{ trip["inventorySystem"] }}</rail:Data> 
        {% endfor %} 
        {% endfor %} 
    </AdditionalData> 
    {% endif %} 
</AMA_RailSellSegmentRQ>"""

def SellSegmentV1(context):
    loader = jinja2.FileSystemLoader(os.getcwd())
    jenv = jinja2.Environment(trim_blocks=True, lstrip_blocks=True)
    t2 = jenv.from_string(SellSegment)
    return "".join(map(unicode.strip,t2.render(myContext=context,journey=context["journey"],passengerList=context["passengerList"],pricingList=context["pricingList"]).splitlines()))
