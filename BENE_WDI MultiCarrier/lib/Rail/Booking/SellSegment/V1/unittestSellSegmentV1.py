import unittest
from SellSegmentV1001 import *

class TestBookingSellSegmentV1(unittest.TestCase):
    def test_upper(self):
        railContext = {
        "officeID":                 "NCE1A0950",
        "corrID":                   "TOPUNITTESTCORRID",
        "journey":                  {"schedule1":[{
                                                                "inventorySystem":"FRR", 
                                                                "serviceProvider":"SNF",
										                        "segmentTID":"SEG_1",
										                        "locationCodeType":"UIC",
                                                                "origin":"8768600",
                                                                "destination":"8772319",
                                                                "trainNumber":"6607", 
                                                                "startDate":"2017-09-14T08:59:00"
                                                                }]
                                        },
        "pricingList":              [{
                                        "bookingClass":"BF",
                                        "bookingClassTID":"BC_1",
                                        "quotingRule":"FA00",                          
                                        "quotingRuleTID":"QR_1",
                                        "quotingRuleType":"001",
										"paxAssociation":"PAX_1 PAX_2",
										"segmentAssociation":"SEG_1"
                                        }],
        "ticketingOption":    {
                                        "type":"006",
                                        "distributionType":"002"
                                        },
        "passengerList":            [ {
                                        "firstName":"FRODO",
                                        "lastName":"BAGGINS",
                                        "dateOfBirth":"1980-01-01",
                                        "PassengerCategoryType":"PT00AD",
                                        "PassengerCategoryCode":"006",
                                        "PassengerCategoryContext":"1A",
                                        "tattoo":"2",
                                        "emailAddress":"YANICE.CHERRAK@AMADEUS.COM"
                                    },{
                                        "firstName":"SAM",
                                        "lastName":"GAMGEE",
                                        "dateOfBirth":"1980-01-01",
                                        "PassengerCategoryType":"PT00AD",
                                        "PassengerCategoryCode":"006",
                                        "PassengerCategoryContext":"1A",
                                        "tattoo":"2",
                                        "emailAddress":"SAM.GAMGEE@AMADEUS.COM"
                                    }]
        }
        
        
        railContext2 = {
        "scenarioID":           "MI_FRR_NVS_001_SlSl_MonoPax_RoundTrip",
        "officeID":                 "LONU12210",
        "corrID":                   "TOPUNITTESTCORRID2",
        "multiInventory":       True,
        "journey":                  {"schedule1":[{
                                                                "inventorySystem":"FRR", 
                                                                "serviceProvider":"SNF",
										                        "segmentTID":"SEG_1",
										                        "locationCodeType":"UIC",
                                                                "origin":"8711300",
                                                                "destination":"8014228",
                                                                "trainNumber":"9571", 
                                                                "additionalDataTID":"AD_1",
                                                                "startDate":"2017-09-07T06:35:00"
                                                                },{
                                                                "inventorySystem":"NVS", 
                                                                "serviceProvider":"DBA",
										                        "segmentTID":"SEG_2",
										                        "locationCodeType":"UIC",
                                                                "origin":"8098160",
                                                                "destination":"8000105",
                                                                "trainNumber":"597", 
                                                                "additionalDataTID":"AD_2",
                                                                "startDate":"2017-09-07T08:26:00"
                                                                }],
                                          "schedule2":[{
                                                                "inventorySystem":"NVS", 
                                                                "serviceProvider":"DBA",
										                        "segmentTID":"SEG_3",
										                        "locationCodeType":"UIC",
                                                                "origin":"8000105",
                                                                "destination":"8000152",
                                                                "trainNumber":"772", 
                                                                "additionalDataTID":"AD_3",
                                                                "startDate":"2017-09-14T08:58:00",
                                                                "endDate":"2017-09-14T11:20:00"
                                                                },{
                                                                "inventorySystem":"NVS", 
                                                                "serviceProvider":"DBA",
										                        "segmentTID":"SEG_4",
										                        "locationCodeType":"UIC",
                                                                "origin":"8000152",
                                                                "destination":"8011160",
                                                                "trainNumber":"545", 
                                                                "additionalDataTID":"AD_4",
                                                                "startDate":"2017-09-14T11:31:00",
                                                                "endDate":"2017-09-14T13:22:00"
                                                                },{
                                                                "inventorySystem":"FRR", 
                                                                "serviceProvider":"SNF",
										                        "segmentTID":"SEG_5",
										                        "locationCodeType":"UIC",
                                                                "origin":"8014228",
                                                                "destination":"8711300",
                                                                "trainNumber":"9572", 
                                                                "additionalDataTID":"AD_5",
                                                                "startDate":"2017-09-14T15:32:00"
                                                                }]
                                        },
        "pricingList":              [{
                                        "bookingClassTID":"BC_1",
                                        "bookingClass":"AF",
                                        "quotingRuleTID":"QR_1",
                                        "quotingRule":"GAF1AT1A",
                                        "quotingRuleType":"001",
										"paxAssociation":"PAX_1",
										"segmentAssociation":"SEG_1"
                                        },{
                                        "bookingClassTID":"BC_2",
                                        "bookingClass":"UF",
                                        "bookingClassClass":"001",
                                        "bookingClassAccomType":"002",
                                        "bookingClassMainClass":"U",
                                        "quotingRuleTID":"QR_2",
                                        "quotingRule":"10001F",
                                        "quotingRuleFamily":"001",
                                        "quotingRuleFlexibility":"002",
                                        "quotingRuleType":"001",
										"paxAssociation":"PAX_1",
										"segmentAssociation":"SEG_2"
                                        },{
                                        "bookingClassTID":"BC_3",
                                        "bookingClass":"UF",
                                        "bookingClassClass":"001",
                                        "bookingClassAccomType":"002",
                                        "bookingClassMainClass":"U",
                                        "quotingRuleTID":"QR_3",
                                        "quotingRule":"10001F",
                                        "quotingRuleFamily":"001",
                                        "quotingRuleFlexibility":"002",
                                        "quotingRuleType":"001",
										"paxAssociation":"PAX_1",
										"segmentAssociation":"SEG_3"
                                        },{
                                        "bookingClassTID":"BC_4",
                                        "bookingClass":"UF",
                                        "bookingClassClass":"001",
                                        "bookingClassAccomType":"002",
                                        "bookingClassMainClass":"U",
                                        "quotingRuleTID":"QR_4",
                                        "quotingRule":"10001F",
                                        "quotingRuleFamily":"001",
                                        "quotingRuleFlexibility":"002",
                                        "quotingRuleType":"001",
										"paxAssociation":"PAX_1",
										"segmentAssociation":"SEG_4"
                                        },{
                                        "bookingClassTID":"BC_5",
                                        "bookingClass":"AF",
                                        "quotingRuleTID":"QR_5",
                                        "quotingRule":"GAF1AT1A",
                                        "quotingRuleType":"001",
										"paxAssociation":"PAX_1",
										"segmentAssociation":"SEG_5"
                                        }],
        "ticketingOption":    {
                                        "type":"006",
                                        "distributionType":"002"
                                        },
        "passengerList":            [ {
                                        "firstName":"FRODO",
                                        "lastName":"BAGGINS",
                                        "dateOfBirth":"1980-01-01",
                                        "PassengerCategoryType":"PT00AD",
                                        "PassengerCategoryCode":"006",
                                        "PassengerCategoryContext":"1A",
                                        "tattoo":"2",
                                        "loyaltyCards":[{
                                                "number":"29090109875015405",
                                                "vendorCompany":"SNF",
                                                "programID":"EFT"
                                        }]
                                    }]
        }
        
        sellSegment = '''<AMA_RailSellSegmentRQ Version="1.001" CorrelationID="TOPUNITTESTCORRID" xsi:schemaLocation="http://xml.amadeus.com/2010/06/RailBooking_v1 AMA_RailSellSegmentRQ.xsd" xmlns="http://xml.amadeus.com/2010/06/RailBooking_v1" xmlns:ota="http://www.opentravel.org/OTA/2003/05/OTA2010B" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:rail="http://xml.amadeus.com/2010/06/RailCommonTypes_v2"><Passenger><Actor TID="PAX_1" DateOfBirth="1980-01-01"><rail:Name FirstName="FRODO" LastName="BAGGINS"/></Actor><Actor TID="PAX_2" DateOfBirth="1980-01-01"><rail:Name FirstName="SAM" LastName="GAMGEE"/></Actor><Contact TID="CNT_11"><rail:Email>YANICE.CHERRAK@AMADEUS.COM</rail:Email></Contact><Contact TID="CNT_12"><rail:Email>SAM.GAMGEE@AMADEUS.COM</rail:Email></Contact></Passenger><Reservation><Schedule TID="SCH_1"><rail:start dateTime="2017-09-14T08:59:00"><rail:locationCode type="UIC">8768600</rail:locationCode></rail:start><rail:end><rail:locationCode type="UIC">8772319</rail:locationCode></rail:end><Segment Inventory="FRR" TID="SEG_1"><rail:start dateTime="2017-09-14T08:59:00"><rail:locationCode type="UIC">8768600</rail:locationCode></rail:start><rail:end><rail:locationCode type="UIC">8772319</rail:locationCode></rail:end><rail:serviceProvider Code="SNF"/><rail:identifier>6607</rail:identifier></Segment></Schedule><Pricing><rail:Reduction Description="PT00AD" TID="RED_1"><rail:PassengerCategory Code="006" Context="1A"/></rail:Reduction><rail:Reduction Description="PT00AD" TID="RED_2"><rail:PassengerCategory Code="006" Context="1A"/></rail:Reduction><rail:BookingClass Code="BF" TID="BC_1"   /><rail:QuotingRule Code="FA00" Type="001" TID="QR_1"  /><rail:TicketingOption Type="006" DistributionType="002" TID="TKO_99"/><rail:Quote RefTIDs="PAX_1 PAX_2"><rail:Product Type="Segment" RefTIDs="BC_1 QR_1 RED_1 RED_2 SEG_1 "/></rail:Quote></Pricing></Reservation></AMA_RailSellSegmentRQ>'''
        sellSegment2 = '''<AMA_RailSellSegmentRQ Version="1.001" CorrelationID="TOPUNITTESTCORRID2" xsi:schemaLocation="http://xml.amadeus.com/2010/06/RailBooking_v1 AMA_RailSellSegmentRQ.xsd" xmlns="http://xml.amadeus.com/2010/06/RailBooking_v1" xmlns:ota="http://www.opentravel.org/OTA/2003/05/OTA2010B" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:rail="http://xml.amadeus.com/2010/06/RailCommonTypes_v2"><Passenger><Actor TID="PAX_1" DateOfBirth="1980-01-01"><rail:Name FirstName="FRODO" LastName="BAGGINS"/></Actor><Contact TID="CNT_12" RefTIDs="PAX_1"><rail:CustomerLoyalty MembershipID="29090109875015405" ProgramID="EFT"><rail:VendorCompany Code="SNF"></rail:VendorCompany></rail:CustomerLoyalty></Contact></Passenger><Reservation><Schedule TID="SCH_1"><rail:start dateTime="2017-09-07T06:35:00"><rail:locationCode type="UIC">8711300</rail:locationCode></rail:start><rail:end><rail:locationCode type="UIC">8000105</rail:locationCode></rail:end><Segment Inventory="FRR" TID="SEG_1"><rail:start dateTime="2017-09-07T06:35:00"><rail:locationCode type="UIC">8711300</rail:locationCode></rail:start><rail:end><rail:locationCode type="UIC">8014228</rail:locationCode></rail:end><rail:serviceProvider Code="SNF"/><rail:identifier>9571</rail:identifier></Segment><Segment Inventory="NVS" TID="SEG_2"><rail:start dateTime="2017-09-07T08:26:00"><rail:locationCode type="UIC">8098160</rail:locationCode></rail:start><rail:end><rail:locationCode type="UIC">8000105</rail:locationCode></rail:end><rail:serviceProvider Code="DBA"/><rail:identifier>597</rail:identifier></Segment></Schedule><Schedule TID="SCH_2"><rail:start dateTime="2017-09-14T08:58:00"><rail:locationCode type="UIC">8000105</rail:locationCode></rail:start><rail:end><rail:locationCode type="UIC">8711300</rail:locationCode></rail:end><Segment Inventory="NVS" TID="SEG_3"><rail:start dateTime="2017-09-14T08:58:00"><rail:locationCode type="UIC">8000105</rail:locationCode></rail:start><rail:end dateTime="2017-09-14T11:20:00"><rail:locationCode type="UIC">8000152</rail:locationCode></rail:end><rail:serviceProvider Code="DBA"/><rail:identifier>772</rail:identifier></Segment><Segment Inventory="NVS" TID="SEG_4"><rail:start dateTime="2017-09-14T11:31:00"><rail:locationCode type="UIC">8000152</rail:locationCode></rail:start><rail:end dateTime="2017-09-14T13:22:00"><rail:locationCode type="UIC">8011160</rail:locationCode></rail:end><rail:serviceProvider Code="DBA"/><rail:identifier>545</rail:identifier></Segment><Segment Inventory="FRR" TID="SEG_5"><rail:start dateTime="2017-09-14T15:32:00"><rail:locationCode type="UIC">8014228</rail:locationCode></rail:start><rail:end><rail:locationCode type="UIC">8711300</rail:locationCode></rail:end><rail:serviceProvider Code="SNF"/><rail:identifier>9572</rail:identifier></Segment></Schedule><Pricing><rail:Reduction Description="PT00AD" TID="RED_1"><rail:PassengerCategory Code="006" Context="1A"/></rail:Reduction><rail:BookingClass Code="AF" TID="BC_1"   /><rail:BookingClass Code="UF" TID="BC_2"  Class="001" AccomType="002" MainClass="U"/><rail:BookingClass Code="UF" TID="BC_3"  Class="001" AccomType="002" MainClass="U"/><rail:BookingClass Code="UF" TID="BC_4"  Class="001" AccomType="002" MainClass="U"/><rail:BookingClass Code="AF" TID="BC_5"   /><rail:QuotingRule Code="GAF1AT1A" Type="001" TID="QR_1"  /><rail:QuotingRule Code="10001F" Type="001" TID="QR_2" Family="001" Flexibility="002"/><rail:QuotingRule Code="10001F" Type="001" TID="QR_3" Family="001" Flexibility="002"/><rail:QuotingRule Code="10001F" Type="001" TID="QR_4" Family="001" Flexibility="002"/><rail:QuotingRule Code="GAF1AT1A" Type="001" TID="QR_5"  /><rail:TicketingOption Type="006" DistributionType="002" TID="TKO_99"/><rail:Quote RefTIDs="PAX_1"><rail:Product Type="Segment" RefTIDs="BC_1 QR_1 RED_1 SEG_1 "/></rail:Quote><rail:Quote RefTIDs="PAX_1"><rail:Product Type="Segment" RefTIDs="BC_2 QR_2 RED_1 SEG_2 "/></rail:Quote><rail:Quote RefTIDs="PAX_1"><rail:Product Type="Segment" RefTIDs="BC_3 QR_3 RED_1 SEG_3 "/></rail:Quote><rail:Quote RefTIDs="PAX_1"><rail:Product Type="Segment" RefTIDs="BC_4 QR_4 RED_1 SEG_4 "/></rail:Quote><rail:Quote RefTIDs="PAX_1"><rail:Product Type="Segment" RefTIDs="BC_5 QR_5 RED_1 SEG_5 "/></rail:Quote></Pricing></Reservation><AdditionalData><rail:Data TID="AD_1" Name="WEBSVCAPI" RefTIDs="SEG_1">FRR</rail:Data><rail:Data TID="AD_2" Name="WEBSVCAPI" RefTIDs="SEG_2">NVS</rail:Data><rail:Data TID="AD_3" Name="WEBSVCAPI" RefTIDs="SEG_3">NVS</rail:Data><rail:Data TID="AD_4" Name="WEBSVCAPI" RefTIDs="SEG_4">NVS</rail:Data><rail:Data TID="AD_5" Name="WEBSVCAPI" RefTIDs="SEG_5">FRR</rail:Data></AdditionalData></AMA_RailSellSegmentRQ>'''
        self.assertEqual(SellSegmentV1(railContext), sellSegment)
        self.assertEqual(SellSegmentV1(railContext2), sellSegment2)

if __name__ == "__main__":
    unittest.main()
    
