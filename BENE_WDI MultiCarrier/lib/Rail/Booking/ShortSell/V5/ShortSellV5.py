import jinja2, os, sys

ShortSell="""<AMA_RailShortSellInPnrRQ xmlns="http://xml.amadeus.com/RAI/2009/10" PrimaryLangID="{{ myContext["language"] }}" Version="5.4">
    <POS>
        <Source>
            <RequestorID Type="30" ID_Context="1A" ID="{{ myContext["officeID"] }}"/>
        </Source>
    </POS>
    <Proposals>
        {% for schedule in journey %}
        {% set scheduleLoop = loop %}
        {% for segment in journey[schedule]%}
        {% set segmentLoop = loop %}
        <Proposal RPH="{{ journey[schedule][segmentLoop.index-1]["proposal"] }}">
            {% if journey[schedule][segmentLoop.index-1]["legProposal"] %}
            <Legs>
                <Leg RPH="{{ journey[schedule][segmentLoop.index-1]["legProposal"] }}">
                {% if journey[schedule][segmentLoop.index-1]["accommodation"] %}
                    <RequestedAccommodations>
                        {% for passenger in passengerList %}
                        <RequestedAccommodation RPH="{{ journey[schedule][segmentLoop.index-1]["accommodation"] }}"{% if journey[schedule][segmentLoop.index-1]["coachNumber"] %} CoachNumber="{{ journey[schedule][segmentLoop.index-1]["coachNumber"] }}" PlaceNumber="{{ journey[schedule][segmentLoop.index-1]["seat"] }}" NearTo="true"{% endif %}>
                            <SeatPreference/>
                            <Passenger Tattoo="{{ passenger["tattoo"] }}" RPH="{{ passenger["rph"] }}"/>
                        </RequestedAccommodation>
                        {% endfor %}
                    </RequestedAccommodations>
                {% endif %}
                </Leg>
            </Legs>
            {% endif %}
        </Proposal>
        {% endfor %}
        {% endfor %}
    </Proposals>
    <Passengers>
        {% for passenger in passengerList %}
        <Passenger Tattoo="{{ passenger["tattoo"] }}" RPH="{{ passenger["rph"] }}">
        {% if passenger["type"] %}
        <Reductions>
            <Reduction>
                <PassengerCategory Category="{% if passenger["type"] == "PT00AD" %}014{% endif %}"/>
                <Description>{{ passenger["dateOfBirth"] }}</Description>
            </Reduction>
        </Reductions>
        {% endif %}
        </Passenger>
        {% endfor %}
    </Passengers>
    <TicketOptions>
        <TicketOption TicketType="{{ ticketingOption["type"] }}">
            <DeliveryMethod DistribType="{{ ticketingOption["distributionType"] }}"/>
            {% if not (emailAddresses==[]) %}
            <DeliveryAddresses>
                {% if myContext["globalEmail"] %}
                <DeliveryAddress>
                    <Email>{{ myContext["globalEmail"] }}</Email>
                </DeliveryAddress>
                {% endif %}
                {% for passenger in passengerList %}
                {% if passenger["emailAddress"] %}
                <DeliveryAddress>
                    <Email>{{ passenger["emailAddress"] }}</Email>
                    <Passengers>
                        <Passenger Tattoo="{{ passenger["tattoo"] }}" RPH="{{ passenger["rph"] }}"/>
                    </Passengers>
                </DeliveryAddress>
                {% endif %}
                {% endfor %}
            </DeliveryAddresses>
            {% endif %}
        </TicketOption>
    </TicketOptions>
</AMA_RailShortSellInPnrRQ>"""


def ShortSellV5(context):
    loader = jinja2.FileSystemLoader(os.getcwd())
    jenv = jinja2.Environment(trim_blocks=True, lstrip_blocks=True)
    t2 = jenv.from_string(ShortSell)
    listEmail = []
    for element in context:
        if element == "globalEmail":
            listEmail.append(context[element])
    for pax in context["passengerList"]:
        for attribute in pax:
            if attribute == "emailAddress":
                listEmail.append(pax[attribute])
    return "".join(map(unicode.strip,t2.render(myContext=context,journey=context["journey"],passengerList=context["passengerList"],ticketingOption=context["ticketingOption"],emailAddresses=listEmail).splitlines()))

