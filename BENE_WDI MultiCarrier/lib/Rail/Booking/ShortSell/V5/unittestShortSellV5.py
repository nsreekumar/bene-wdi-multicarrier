import unittest
from ShortSellV5 import *

class TestBookingShortSellV5(unittest.TestCase):
    def test_upper(self):
        railContext = {
            "officeID":                 "NCE1A0950",
            "corrID":                   "LKJSDHAFQ347HFOER8UHFO8EWH5RQI8EUHROW",
            "language":              "EN",
            "journey":                  {"schedule":[{
                                                                "inventorySystem":"FRR", 
                                                                "serviceProvider":"SNF",
										                        "segmentTID":"SEG_1",
										                        "locationCodeType":"UIC",
                                                                "origin":"8768600",
                                                                "destination":"8772319",
                                                                "trainNumber":"6607", 
                                                                "proposal":"602",
                                                                "startDate":"2022-07-15T08:59:00"
                                                                }],
                                                "schedule1":[{
                                                                "inventorySystem":"FRR", 
                                                                "serviceProvider":"SNF",
										                        "segmentTID":"SEG_2",
										                        "locationCodeType":"UIC",
                                                                "origin":"8772319",
                                                                "destination":"8768600",
                                                                "trainNumber":"6608", 
                                                                "proposal":"603",
                                                                "startDate":"2022-07-15T18:59:00"
                                                                }]
                                        },
            "pricingList":              [{
                                        "bookingClass":"BF",
                                        "bookingClassTID":"BC_1",
                                        "quotingRule":"FA00",
                                        "quotingRuleTID":"QR_1",
                                        "quotingRuleType":"001",
										"paxAssociation":"PAX_1",
										"segmentAssociation":"SEG_1"
                                        }],
            "ticketingOption":    {
                                        "type":"006",
                                        "distributionType":"002"
                                        },
            "passengerList":            [ {
                                        "firstName":"FRODO",
                                        "lastName":"BAGGINS",
                                        "dateOfBirth":"1980-01-01",
                                        "PassengerCategoryType":"PT00AD",
                                        "PassengerCategoryCode":"006",
                                        "PassengerCategoryContext":"1A",
                                        "tattoo":"2",
                                        "rph":"100",
                                        "emailAddress":"YANICE.CHERRAK@AMADEUS.COM"
                                    }]
        }
        
        railContext2 = {
            "officeID":                 "NCE1A0950",
            "corrID":                   "LKJSDHAFQ347HFOER8UHFO8EWH5RQI8EUHROW",
            "language":              "EN",
            "globalEmail":          "GLOBAL@EMAIL.COM",
            "journey":                  {"schedule":[{
                                                                "inventorySystem":"FRR", 
                                                                "serviceProvider":"SNF",
										                        "segmentTID":"SEG_1",
										                        "locationCodeType":"UIC",
                                                                "origin":"8768600",
                                                                "destination":"8772319",
                                                                "trainNumber":"6607", 
                                                                "proposal":"602",
                                                                "startDate":"2022-07-15T08:59:00"
                                                                }]
                                        },
            "pricingList":              [{
                                        "bookingClass":"BF",
                                        "bookingClassTID":"BC_1",
                                        "quotingRule":"FA00",
                                        "quotingRuleTID":"QR_1",
                                        "quotingRuleType":"001",
										"paxAssociation":"PAX_1",
										"segmentAssociation":"SEG_1"
                                        }],
            "ticketingOption":    {
                                        "type":"006",
                                        "distributionType":"002"
                                        },
            "passengerList":            [ {
                                        "firstName":"FRODO",
                                        "lastName":"BAGGINS",
                                        "dateOfBirth":"1980-01-01",
                                        "PassengerCategoryType":"PT00AD",
                                        "PassengerCategoryCode":"006",
                                        "PassengerCategoryContext":"1A",
                                        "tattoo":"2",
                                        "rph":"100",
                                        "emailAddress":"YANICE.CHERRAK@AMADEUS.COM"
                                    },
                                    {
                                        "firstName":"FRODITO",
                                        "lastName":"BAGGINSITO",
                                        "dateOfBirth":"2000-01-01",
                                        "PassengerCategoryType":"PT00AD",
                                        "PassengerCategoryCode":"006",
                                        "PassengerCategoryContext":"1A",
                                        "tattoo":"2",
                                        "rph":"100",
                                        "emailAddress":"YANICITO.CHERRAKITO@AMADEUS.COM"
                                    }]
        }
        
        railContext3 = {
            "officeID":                 "NCE1A0950",
            "corrID":                   "LKJSDHAFQ347HFOER8UHFO8EWH5RQI8EUHROW",
            "language":              "EN",
            "journey":                  {"schedule":[{
                                                                "inventorySystem":"FRR", 
                                                                "serviceProvider":"SNF",
										                        "segmentTID":"SEG_1",
										                        "locationCodeType":"UIC",
                                                                "origin":"8768600",
                                                                "destination":"8772319",
                                                                "trainNumber":"6607", 
                                                                "proposal":"602",
                                                                "startDate":"2022-07-15T08:59:00"
                                                                }]
                                        },
            "pricingList":              [{
                                        "bookingClass":"BF",
                                        "bookingClassTID":"BC_1",
                                        "quotingRule":"FA00",
                                        "quotingRuleTID":"QR_1",
                                        "quotingRuleType":"001",
										"paxAssociation":"PAX_1",
										"segmentAssociation":"SEG_1"
                                        }],
            "ticketingOption":    {
                                        "type":"006",
                                        "distributionType":"002"
                                        },
            "passengerList":            [ {
                                        "firstName":"FRODO",
                                        "lastName":"BAGGINS",
                                        "dateOfBirth":"1980-01-01",
                                        "PassengerCategoryType":"PT00AD",
                                        "PassengerCategoryCode":"006",
                                        "PassengerCategoryContext":"1A",
                                        "tattoo":"2",
                                        "rph":"100"
                                    },
                                    {
                                        "firstName":"FRODITO",
                                        "lastName":"BAGGINSITO",
                                        "dateOfBirth":"2000-01-01",
                                        "PassengerCategoryType":"PT00AD",
                                        "PassengerCategoryCode":"006",
                                        "PassengerCategoryContext":"1A",
                                        "tattoo":"2",
                                        "rph":"100"
                                    }]
        }        
        shortSell = '''<AMA_RailShortSellInPnrRQ xmlns="http://xml.amadeus.com/RAI/2009/10" PrimaryLangID="EN" Version="5.4"><POS><Source><RequestorID Type="30" ID_Context="1A" ID="NCE1A0950"/></Source></POS><Proposals><Proposal RPH="603"></Proposal><Proposal RPH="602"></Proposal></Proposals><Passengers><Passenger Tattoo="2" RPH="100"/></Passengers><TicketOptions><TicketOption TicketType="006"><DeliveryMethod DistribType="002"/><DeliveryAddresses><DeliveryAddress><Email>YANICE.CHERRAK@AMADEUS.COM</Email><Passengers><Passenger Tattoo="2" RPH="100"/></Passengers></DeliveryAddress></DeliveryAddresses></TicketOption></TicketOptions></AMA_RailShortSellInPnrRQ>'''
        shortSell2 = '''<AMA_RailShortSellInPnrRQ xmlns="http://xml.amadeus.com/RAI/2009/10" PrimaryLangID="EN" Version="5.4"><POS><Source><RequestorID Type="30" ID_Context="1A" ID="NCE1A0950"/></Source></POS><Proposals><Proposal RPH="602"></Proposal></Proposals><Passengers><Passenger Tattoo="2" RPH="100"/><Passenger Tattoo="2" RPH="100"/></Passengers><TicketOptions><TicketOption TicketType="006"><DeliveryMethod DistribType="002"/><DeliveryAddresses><DeliveryAddress><Email>GLOBAL@EMAIL.COM</Email></DeliveryAddress><DeliveryAddress><Email>YANICE.CHERRAK@AMADEUS.COM</Email><Passengers><Passenger Tattoo="2" RPH="100"/></Passengers></DeliveryAddress><DeliveryAddress><Email>YANICITO.CHERRAKITO@AMADEUS.COM</Email><Passengers><Passenger Tattoo="2" RPH="100"/></Passengers></DeliveryAddress></DeliveryAddresses></TicketOption></TicketOptions></AMA_RailShortSellInPnrRQ>'''
        shortSell3 = '''<AMA_RailShortSellInPnrRQ xmlns="http://xml.amadeus.com/RAI/2009/10" PrimaryLangID="EN" Version="5.4"><POS><Source><RequestorID Type="30" ID_Context="1A" ID="NCE1A0950"/></Source></POS><Proposals><Proposal RPH="602"></Proposal></Proposals><Passengers><Passenger Tattoo="2" RPH="100"/><Passenger Tattoo="2" RPH="100"/></Passengers><TicketOptions><TicketOption TicketType="006"><DeliveryMethod DistribType="002"/></TicketOption></TicketOptions></AMA_RailShortSellInPnrRQ>'''
        self.assertEqual(ShortSellV5(railContext), shortSell)
        self.assertEqual(ShortSellV5(railContext2), shortSell2)
        self.assertEqual(ShortSellV5(railContext3), shortSell3)
        
if __name__ == "__main__":
    unittest.main()
