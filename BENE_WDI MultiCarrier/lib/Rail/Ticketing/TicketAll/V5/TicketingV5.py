import jinja2, os, sys

Ticketing="""<AMA_RailTicketingInPnrRQ xmlns="http://xml.amadeus.com/RAI/2009/10" Version="5.4">
	<TicketingActions>
		<TicketingAction All="true">
		</TicketingAction>
	</TicketingActions>
</AMA_RailTicketingInPnrRQ>"""


def TicketingAllV5(context):
    loader = jinja2.FileSystemLoader(os.getcwd())
    jenv = jinja2.Environment(trim_blocks=True, lstrip_blocks=True)
    t2 = jenv.from_string(Ticketing)
    return ''.join(map(unicode.strip,t2.render(myContext=context).splitlines()))
