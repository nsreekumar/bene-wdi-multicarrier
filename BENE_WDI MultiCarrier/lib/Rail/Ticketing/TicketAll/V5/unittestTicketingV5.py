import unittest
from TicketingV5 import *

class TestTicketingV5(unittest.TestCase):
    def test_upper(self):
        railContext = {
            "officeID":                 "NCE1A0950",
            "corrID":                   "TOPUNITTESTCORRID",
            "journey":                  {"schedule1":[{
                                                                "inventorySystem":"FRR", 
                                                                "serviceProvider":"SNF",
										                        "segmentTID":"SEG_1",
										                        "locationCodeType":"UIC",
                                                                "origin":"8768600",
                                                                "destination":"8772319",
                                                                "trainNumber":"6607", 
                                                                "startDate":"%Y-%m-%dT08:59:00"
                                                                }]
                                        },
            "pricingList":              [{
                                        "bookingClass":"BF",
                                        "bookingClassTID":"BC_1",
                                        "quotingRule":"FA00",
                                        "quotingRuleTID":"QR_1",
                                        "quotingRuleType":"001",
										"paxAssociation":"PAX_1",
										"segmentAssociation":"SEG_1"
                                        }],
            "ticketingOption":    {
                                        "type":"006",
                                        "distributionType":"002"
                                        },
            "passengerList":            [ {
                                        "firstName":"FRODO",
                                        "lastName":"BAGGINS",
                                        "dateOfBirth":"1980-01-01",
                                        "PassengerCategoryType":"PT00AD",
                                        "PassengerCategoryCode":"006",
                                        "PassengerCategoryContext":"1A",
                                        "tattoo":"2",
                                        "emailAddress":"YANICE.CHERRAK@AMADEUS.COM"
                                    }]
        }
        ticketing = '''<AMA_RailTicketingInPnrRQ xmlns="http://xml.amadeus.com/RAI/2009/10" Version="5.4"><TicketingActions><TicketingAction All="true"></TicketingAction></TicketingActions></AMA_RailTicketingInPnrRQ>'''
        self.assertEqual(TicketingAllV5(railContext), ticketing)

if __name__ == "__main__":
    unittest.main()