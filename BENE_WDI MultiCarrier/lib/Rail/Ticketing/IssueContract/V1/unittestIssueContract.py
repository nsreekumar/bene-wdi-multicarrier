import unittest
from IssueContractV1 import *

class TestTicketingV5(unittest.TestCase):
    def test_upper(self):

        railContext = {
                "scenarioID":           "BKG_DBMCP_ROB_001_SingleTrip_TRE_NVS.play",
                "officeID":                 "NCE1A0950",
                "corrID":                   helpers.generate_correlationID(),
                "language":              "EN",
                "journey":                  {"schedule":[{
                                                                        "inventorySystem":"AMA",
                                                                        "serviceProvider":"TRE",
                                                                        "segmentTID":"SEG_1",
                                                                        "locationCodeType":"1A",
                                                                        "origin":"8308409",
                                                                        "destination":"8301700",
                                                                        "trainNumber":"9610",
                                                                        "nbOfResponses":"1",
                                                                        "maxChanges":"3",
                                                                        "startDate":helpers.nextThursday("%Y-%m-%dT12:00:00")
                                                                        }]
                                                },
                "pricingList":              [{
                                                "bookingClass":"BF",
                                                "bookingClassTID":"BC_1",
                                                "quotingRule":"FA00",
                                                "quotingRuleTID":"QR_1",
                                                "quotingRuleType":"001",
                                                "paxAssociation":"PAX_1",
                                                "segmentAssociation":"SEG_1"
                                                }],
                "ticketingOption":    {
                                                "type":"006",
                                                "distributionType":"011"
                                                },
                "passengerList":            [ {
                                                "firstName":"FRODO",
                                                "lastName":"BAGGINS",
                                                "dateOfBirth":"1980-01-01",
                                                "PassengerCategoryType":"PT00AD",
                                                "PassengerCategoryCode":"006",
                                                "PassengerCategoryContext":"1A",
                                                "tattoo":"2",
                                                "phone":"0782210000",
                                                "emailAddress":"PIYUSH.MISHRA@AMADEUS.COM"
                                            }]
                }

        ticketing = '''<AMA_RailTicketingInPnrRQ xmlns="http://xml.amadeus.com/RAI/2009/10" Version="5.4"><TicketingActions><TicketingAction All="true"></TicketingAction></TicketingActions></AMA_RailTicketingInPnrRQ>'''
        self.assertEqual(TicketingAllV5(railContext), ticketing)





railContext = {
                "scenarioID":           "BKG_DBMCP_ROB_001_SingleTrip_TRE_NVS.play",
                "officeID":                 "NCE1A0950",
                "corrID":                   'aaaaa',
                "language":              "EN",
                "rloc":                   "123435",
                "journey":                  {"schedule":[{
                                                                        "inventorySystem":"AMA",
                                                                        "serviceProvider":"TRE",
                                                                        "segmentTID":"SEG_1",
                                                                        "locationCodeType":"1A",
                                                                        "origin":"8308409",
                                                                        "destination":"8301700",
                                                                        "trainNumber":"9610",
                                                                        "nbOfResponses":"1",
                                                                        "maxChanges":"3"
                                                                        }]
                                                },
                "pricingList":              [{
                                                "bookingClass":"BF",
                                                "bookingClassTID":"BC_1",
                                                "quotingRule":"FA00",
                                                "quotingRuleTID":"QR_1",
                                                "quotingRuleType":"001",
                                                "paxAssociation":"PAX_1",
                                                "segmentAssociation":"SEG_1"
                                                }],
                "ticketingOption":    {
                                                "type":"006",
                                                "distributionType":"011"
                                                },
                "passengerList":            [ {
                                                "firstName":"FRODO",
                                                "lastName":"BAGGINS",
                                                "dateOfBirth":"1980-01-01",
                                                "PassengerCategoryType":"PT00AD",
                                                "PassengerCategoryCode":"006",
                                                "PassengerCategoryContext":"1A",
                                                "tattoo":"2",
                                                "phone":"0782210000",
                                                "emailAddress":"PIYUSH.MISHRA@AMADEUS.COM"
                                            }]
                }

if __name__ == "__main__":
    # unittest.main()
    print(IssueContractV1(railContext))