import jinja2, os, sys

IssueContract = """
<AMA_RailIssueContractRQ Version="1.000" CorrelationID="{{ myContext["corrID"] }}" xmlns="http://xml.amadeus.com/2010/06/RailTicketing_v1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:mop="http://xml.amadeus.com/2010/06/PayIssueTypes_v1" xmlns:rail="http://xml.amadeus.com/2010/06/RailCommonTypes_v2" xsi:schemaLocation="http://xml.amadeus.com/2010/06/RailTicketing_v1 ../../../RailMercurial/Grammar/v6_working_TKT/AMA_RaillIssueContractRQ.xsd">&
	<AssociatedRecords TID="RCD_1" Identifier="{{ myContext["rloc"] }}"/>&
	<Issue>&
	<rail:MethodOfPayment TID="MOP_2">&
	<mop:Cash/>&
	</rail:MethodOfPayment>&
	<rail:FormatAndDelivery>&
	<rail:Delivery Type="006" DistributionType="004" TID="TKO_3"/>&
	</rail:FormatAndDelivery>&
	<rail:Contract TID="CTR_1" RefTIDs="RCD_1 MOP_2 TKO_3"/>&
	</Issue>&
</AMA_RailIssueContractRQ>&
"""




def IssueContractV1(context):
    loader = jinja2.FileSystemLoader(os.getcwd())
    jenv = jinja2.Environment(trim_blocks=True, lstrip_blocks=True)
    t2 = jenv.from_string(IssueContract)
    return ''.join(map(unicode.strip,t2.render(myContext=context).splitlines()))
