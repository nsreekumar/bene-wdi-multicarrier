#!/usr/bin/env python

import base64
from ConfigParser import SafeConfigParser
from datetime import datetime
import httplib
from urlparse import urlparse
from urllib import urlencode
import urllib2
import logging
from optparse import OptionParser
import os.path
import re
import socket
import sys
import time

try:
    import json
except:
    try:
        import simplejson as json
    except:
        print >> sys.stderr, 'JSON support required, please install http://pypi.python.org/pypi/simplejson/'
        sys.exit(-1)
       
APPLICATION_JSON = 'application/json'
APPLICATION_XML = 'application/xml'
APPLICATION_FORM_URLENCODED = 'application/x-www-form-urlencoded'
TEXT_PLAIN = 'text/plain'
ACCEPT = 'Accept'
HTTP_AUTHORIZATION = 'Authorization'
HTTP_ACCEPT = 'Accept'
HTTP_CACHE_CONTROL = 'Cache-Control'
HTTP_CONNECTION = 'Connection'
HTTP_CONTENT_TYPE = 'Content-Type'
HTTP_IF_MODIFIED_SINCE = 'If-Modified-Since'
HTTP_LAST_MODIFIED = 'Last-Modified'
HTTP_LOCATION = 'Location'
RFC822_DATE = '%a, %d %b %Y %H:%M:%S %Z'
TIME_PATTERN = '%Y-%m-%d_%H-%M-%S'
VERSION='1.1.0'

class RemoteHTTPException(Exception):
    '''Exception thrown in case ALF returns an HTTP error.'''

    def __init__(self, response):
        '''Create new exception with given status code.

        The method calls response.read().
        response - HTTP response (object returned from httplib call).'''
        
        Exception.__init__(self, 'Server returned HTTP %s(%d): %s'
                           % (response.reason, response.status, response.read()))
        self.status = response.status;
        
class Alf(object):
    '''Interface to ALF REST interface.'''
    def __init__(self, config = '~/.alf', url = None, user = None,
                 password = None, section = 'alf'):
        '''Create REST client.

        The configuration file should have the following format:

        [alf]
        user = <username>
        password = <password>
        url = https://alfwt001.os.amadeus.net:8181/alf/rest

        config - Path to configuration file, defaults to ~/.alf
        url - Base URL.
        user - Username.
        password - Password.
        section - Section to read from configuration file, defaults to "alf".'''
        
        configpath = os.path.expandvars(os.path.expanduser(config))
        if os.path.isfile(configpath):
            cp = SafeConfigParser()
            cp.read(configpath)
            if not user:
                user = cp.get(section, 'user')
            if not password:
                password = cp.get(section, 'password')
            if not url:
                url = cp.get(section, 'url')
        self.url = url
        (self.scheme, self.netloc, self.path) = urlparse(self.url)[:3]
        self.__authheader = 'Basic %s' % base64.encodestring('%s:%s' % (user, password)).strip()
        self.connection = None
        logging.debug('Created ALF connector for %s', self.url)
                        
    def info(self, mediatype = APPLICATION_JSON):
        '''Retrieve list of available log configurations.
        
        mediatype - Expected mime-type, defaults to "application/json".'''
        
        response = self.get(self.path + '/logs', mediatype)
        if response.status == httplib.OK:
            return response.read()
        else:
            raise RemoteHTTPException, response

    def targetInfo(self, targetId, mediatype = APPLICATION_JSON):
        '''Get supported indices and retention period inforamtion for target.
        targetId - Target ID.
        mediatype - Expected mime-type, defaults to "application/json".'''
        
        response = self.get('%s/logs/%d/info' % (self.path, targetId),
                            mediatype)
        if response.status == httplib.OK:
            data = response.read()
            logging.debug("Response body: " + data)
            return data
        else:
            raise RemoteHTTPException, response            
           
    def search(self, id, stime, period, pattern):
        '''
            Search message log.

            In case the pattern contains keys/values the keys are the index
            names, for patterns mapped to an index "word" must be used.
            
            id - Log configuration ID, can be retrieved via info resource.
            stime - Start time of search interval.
            period - Interval length in seconds.
            pattern - Pattern to search, may be a simple strting, a dictionary
                containing key value pairs, or a sequence of key/value patterns.
            In case of success ALF returns "CREATED" and the result URL in the 
            "Location" header of the replie, this URL is the return value.
        '''
        #ts = time.strftime(TIME_PATTERN, stime) --> errors here  with old tts python version
        ts = stime
        query = [ ('time', ts), ('period', period)]
        if isinstance(pattern, basestring):
            query.append(('word', pattern))
        else:
            query.extend(pattern)
        response = self.post(self.path + '/logs/' + str(id), query)
        if response.status == httplib.CREATED:
            body = response.read()
            location = response.getheader(HTTP_LOCATION)
            logging.debug('CREATED %s', location)
            return location
        else:
            raise RemoteHTTPException, response

    def __isComplete(self, response):
        return response.getheader(
            HTTP_CACHE_CONTROL,'').find('must-revalidate') == -1
    
    def result(self, url, mediatype = TEXT_PLAIN, modifiedsince = None,
               timeout = -1):
        '''
            Get result.

            url - Result url as returned by search.
            mediatype - Expected media-type.
            modifiedsince - Return result only if it was modified after this date.
                Usually this value is the outcome of a previous call.
            Returns a tuple (data, complete, lastmodified), data is the result
                in requested format, complete is a boolean indicating if the result
                is complete, lastmodified is the modifiaction date of the result
                on the server and should be used in subsequent calls. data is
                None in case lastmodified was provided and the result was not
                changed on the server.
        '''
        #path = urlparse(url).path  --> errors here with old TTS python version
        path = url
        headers = {}
        if modifiedsince:
            headers[HTTP_IF_MODIFIED_SINCE] = modifiedsince
        response = self.get(path, mediatype, headers,
                            params = [('timeout', timeout)])
            
        if response.status == httplib.OK:
            return response.read(), self.__isComplete(response), \
                   response.getheader(HTTP_LAST_MODIFIED)
        elif response.status == httplib.NOT_MODIFIED:
            response.read()
            return None, self.__isComplete(response), \
                   response.getheader(HTTP_LAST_MODIFIED)  
        else:
            raise RemoteHTTPException, response

    def history(self, mediatype = APPLICATION_JSON):
        '''
            Get user's search history.

            mediatype - Expected format, currently only JSON is supported.
            '''
        return self.getData(self.path + '/results', mediatype)
            
    def connect(self):
        '''
            Open connection.
        '''
        if self.scheme == 'https':
            self.connection = httplib.HTTPSConnection(self.netloc)
            logging.debug('Opened HTTPS conncetion to %s', self.netloc)
        else:
            self.connection = httplib.HTTPConnection(self.netloc)
            logging.debug('Opened HTTP conncetion to %s', self.netloc)
        self.connection.connect()

    def getData(self, url, mediatype):
        '''Send get request to ALF and directly return the body content.

            url - URL to GET.
            mediatype - Expected response format.
            
            If the response is a OK (200) the response.read() is returned.
            Otherwise a RemoteHTTPException is raised.'''
        response = self.get(url, mediatype)
        if response.status == httplib.OK:
            data = response.read()
            logging.debug("Response body: " + data)
            return data
        else:
            raise RemoteHTTPException, response            

    def get(self, path, mediatype = '*/*', headers = {}, params = None):
        '''
            Send get request.

            path - Full resource path on server.
            mediatype - Expected media-type.
            headers - Additional headers to send, this method automatically sets
                the Accept, Content-Type and Authentication headers.
            Returns httplib.HTTPResponse object.
        '''
        if not self.connection:
            self.connect()
        headers[HTTP_AUTHORIZATION] = self.__authheader
        headers[HTTP_ACCEPT] = mediatype
        if params:
            path += '?' + urlencode(params)
        logging.debug('GET %s %s', path, str(headers))
        self.connection.request('GET', path, headers = headers)
        response = self.connection.getresponse()
        logging.debug('Reponse %d, headers: %s', response.status,
                      str(response.getheaders()))
        if response.getheader(HTTP_CONNECTION) == 'close':
            self.connection = None
        return response
    
    def post(self, path, params, mediatype = '*/*', headers = {}):
        '''
            Send POST request to ALF.
            
            path - Resource path on server.
            params - List of name value tuples used to build url-encoded request body.
            mediatype - Accepted reponse format, defaults to '*/*'.
            headers - Additional headers to set, this method automatically sets
                the Accept, Content-Type and Authentication headers.
            Returns httplib.HTTPResponse object.
        '''
        
        if not self.connection:
            self.connect()
        headers[HTTP_AUTHORIZATION] = self.__authheader
        headers[HTTP_ACCEPT] = mediatype
        headers[HTTP_CONTENT_TYPE] = APPLICATION_FORM_URLENCODED 
        data = urlencode(params)
        logging.debug('POST params = %s', params)
        logging.debug('POST %s header = %s, body = %s', path, headers, data)
        self.connection.request('POST', path, data, headers)
        response = self.connection.getresponse()
        if response.getheader(HTTP_CONNECTION) == 'close':
            self.connection = None
        logging.debug('Reponse headers: %s', str(response.getheaders()))
        return response

def configureLogger():
    root = logging.getLogger()
    stderr = logging.StreamHandler(sys.stderr)
    stderr.setFormatter(logging.Formatter("%(message)s"))
    root.addHandler(stderr)
    logging.getLogger().setLevel(logging.INFO)

def infoCommand(alf):
    '''List all log-configurations on stdout.

    alf - ALF REST interface object.'''
    
    info = json.loads(alf.info(APPLICATION_JSON))
    for config in info['logConfig']:
        print "%(id)6d %(name)-40s %(sysType)-6s %(phase)-3s" % config

def targetInfoCommand(alf, targetId):
    '''Get target details.

    alf - ALF REST interface object.
    targetId - Target id as returned by info command.'''
    
    info = json.loads(alf.targetInfo(targetId, APPLICATION_JSON))
    print 'Supported Indices: %s\nLog Retention: %d days' \
          % (info['indices'], info['retentionDays'])

def printResult(data):
    try:
        result = json.loads(data)
        for msg in result['message']:
            try:
                for header in msg['header']:
                    print header
            except KeyError:
                pass
            try:
                print msg['data']
            except UnicodeEncodeError:
                hexdump(msg['data'])
            except KeyError:
                pass
    except KeyError, e:
        logging.info('No messages found')

def hexdump(msg):
    lines = len(msg) / 16
    if lines * 16 < len(msg):
        lines = lines + 1
    for line in range(0, lines):
        address = line * 16
        sys.stdout.write('%08dx' % address)
        for i in range(address, address + 16):
            if i < len(msg):            
                sys.stdout.write('%x ' % ord(msg[i]))
            else:
                sys.stdout.write('   ')
        sys.stdout.write(' ')
        for i in range(address, min(address + 16, len(msg))):
            c = ord(msg[i])
            if c < 32 or c >= 127:
                sys.stdout.write('.')
            else:
                sys.stdout.write(msg[i])
        print

def searchCommand(alf, options, patterns):
    '''Create a new search.

    options - Command line options as returned by OptionParser, requires
        options "time" and "period"
    patterns - Search patterns, indexes should have the format
        <index name>:<index key>.'''
    startTime = datetime.now()
    m = re.match(r'([0-9]+) *([a-z]*)', options.period)
    if not m:
        raise Exception, '%s is not a valid period' % options.period
    period = int(m.group(1))
    if m.group(2).startswith('m'):
        period = period * 60
    elif m.group(2).startswith('h'):
        period = period * 3600
    elif m.group(2).startswith('s'):
        pass
    elif len(m.group(2)) > 0:
        raise Exception, '>>%s<< is not a valid time unit' % m.group(2)
    logging.debug('Time Period %d', period)
    if options.time:
        t = time.strptime(options.time, '%Y-%m-%d %H:%M:%S')
    else:
        t = time.gmtime(time.time() - period)
    patternList = list()
    for pattern in patterns:
        idx = pattern.find(':')
        if idx > 0:
            patternList.append((pattern[:idx], pattern[idx + 1:]))
        else:
            patternList.append(('word', pattern))
    url = alf.search(options.id, t, period, patternList)
    logging.info("Result ID is %s", url[url.rfind('/') + 1:])
    (data, complete, lastmodified) = alf.result(url, APPLICATION_JSON,
                                                timeout = options.wait)
    try:
        while not complete:
            (data, complete, lastmodified) = \
                   alf.result(url, APPLICATION_JSON, lastmodified,
                              timeout = options.wait)
            if data:
                result = json.loads(data)
                logging.info('Complete: %d%%', result['complete'])
            else:
                logging.debug('lastmodified %s', lastmodified)
        printResult(data)
        delta = datetime.now() - startTime
        logging.info("Search completed in %.3f seconds", (delta.seconds + delta.microseconds / 1000000.0))
    except KeyboardInterrupt:
        logging.info('Search interrupted, you may obtain the result later using the command\n\t %s result %s',
                     sys.argv[0], url[url.rfind('/') + 1:])

def resultCommand(alf, uid):
    '''Retrieve existing result.

    alf - ALF REST interface object.
    uid - Result unique ID.'''
    (data, complete, lastmodified) = alf.result(alf.url + '/results/' + uid,
                                                APPLICATION_JSON)
    printResult(data)
def historyCommand(alf):
    
    '''Retrive search history of current user.

    alf - ALF REST interface object.'''
    data = alf.history(APPLICATION_JSON);
    history = json.loads(data)
    if history.has_key('results'):
        for result in history['results']:
            print '%(from)s %(to)s %(pattern)-40s %(uid)s' % result
    
def main():
    usage = '''%prog [--debug] command [command options] [command args]

SUPPORTED COMMANDS

    info - Get information about available message logs.
        List all available message logs (targets):
        
        %prog info

        This is the first entry point to get the ID required in other
        commands.

        Get supported indexes and log retention time for one target:

        %prog info --id=<id>
        
    search - Search message logs
        Search between 13:24:00 and 23:24:00 for messages with
        correlation ID "0001LL93H5L2J7/0904800900075D" using index:
        
        %prog search search --id=210 --time="2010-10-01 13:24:00" --period=10h
            "CorrelID:0001LL93H5L2J7/0904800900075D"

        Note that you can obtain the list of supported indices using the
        "info --id=<id>" command.

    result - Get result of prevoious search requests (search history)
        Get result with result ID "77B8810DAC1A82B00130BC6FA02945EB"

        %prog result 77B8810DAC1A82B00130BC6FA02945EB

    history - List user's search history.
    
        %prog history

FILES
    
    %prog reads its configuration from a file ".alf" located in the user's
    home directory (%USERPROFILE%/.alf on Windows).
    The file has the following format:

        [alf]
        user = <Aproach user ID>
        password = <Aproach password>
        ;; ALF base url
        ;; To target the ALF test system use url:
        ;; https://alfwt001.os.amadeus.net:8443/alf/rest
        url = https://loggingfacility.amadeus.com/alf/rest

    Make sure the file is only readable by user to avoid that others can
    read the password.
    '''
    configureLogger()
    op = OptionParser(usage = usage, version = '%prog ' + VERSION)
    op.add_option('-d', '--debug', dest = 'debug', action = 'store_true',
                  help = 'Enable debug messages', default = False)
    op.add_option('-t', '--time', dest = 'time',
                  metavar = '"yyyy-MM-dd hh:mm:ss"',
                  help = 'Start or search interval', default = None)
    op.add_option('-p', '--period', dest = 'period', metavar = 'PERIOD',
                  help = 'Length of search interval in format 5s, 5m, 5h '
                  + 'to search within a 5 seconds, minutes, hours time interval', default = "60s")
    op.add_option('-i', '--id', dest = 'id', metavar = 'ID', type="int",
                  help = 'Log configuration ID', default = None)
    op.add_option('-q', '--quiet', dest = 'quiet', action = 'store_true',
                  help = 'Do not print status messages', default = False)
    op.add_option('-w', '--wait', dest = 'wait', metavar = 'SECONDS',
                  help = 'Time to wait in HTTP call to retrieve result',
                  default = 1)
    (options, args) = op.parse_args()
                  
    if options.debug:
        logging.getLogger().setLevel(logging.DEBUG)
    elif options.quiet:
        logging.getLogger().setLevel(logging.WARNING)
    try:
        cmd = args[0]
        alf = Alf()
        try:
            if cmd == 'info':
                if options.id:
                    targetInfoCommand(alf, options.id)
                else:
                    infoCommand(alf)
            elif cmd == 'search':
                searchCommand(alf, options, args[1:])
            elif cmd == 'result':
                resultCommand(alf, args[1])
            elif cmd == 'history':
                historyCommand(alf)
        except socket.error, e:
            logging.error('Connection to %s failed: %s', alf.url, str(e))
    except Exception, e:
        if options.debug:
            logging.exception(e)
        else:
            logging.error(str(type(e)) + ',' + str(e))


def performAlfSearch(userName, passWord, searchTime, periodSecods, strToSearch, searchId):
     patterns =  [('word',strToSearch)]
     alfUrl = "https://loggingfacility.amadeus.com/alf/rest"
     alf = Alf("", alfUrl, userName, passWord, "");
     searchUrl = alf.search(searchId, searchTime, periodSecods,patterns)
     print "Alf Result ID is " + searchUrl[searchUrl.rfind('/') + 1:]
     print "sleeping again to wait for ALF..."
     time.sleep(5)
     print "ok...woke up"
         
     (data, complete, lastmodified) = alf.result(searchUrl, APPLICATION_JSON, None, 1)
     while not complete:
        (data, complete, lastmodified) = alf.result(searchUrl, APPLICATION_JSON, lastmodified,1)
        if data:
             result = json.loads(data)
             print 'Complete: ' + str(result['complete']) 

     print "returning results"
     return data

if __name__ == '__main__':
    sys.exit(main())

