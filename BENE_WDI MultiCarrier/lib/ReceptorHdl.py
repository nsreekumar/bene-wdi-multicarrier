import time;
from ttserverlib import *
from os.path import splitext, basename

#####################
### Interfaces to call ###
#####################
def startReceptorEDI_MFNDCQ():   
    aMsg = "MFNDCQ";
    aPort = 40010;
    
    #Start the receptor
    [aConfig, aScenario, aReceptor] = createReceptor(aMsg, aPort);
    context.config_MFNDCQ = aConfig;
    context.scenario_MFNDCQ = aScenario;
    context.server_MFNDCQ = aReceptor;    
    context.server_MFNDCQ.start();

    #Add routing key
    addRoutingKey(aMsg, aPort);
    time.sleep(2);   

def startReceptorEDI_RSFENS():   
    aMsg = "RSFENS";
    aPort = 40011;
    
    #Start the receptor
    [aConfig, aScenario, aReceptor] = createReceptor(aMsg, aPort);
    context.config_RSFENS = aConfig;
    context.scenario_RSFENS = aScenario;
    context.server_RSFENS = aReceptor;    
    context.server_RSFENS.start();

    #Add routing key
    addRoutingKey(aMsg, aPort);
    time.sleep(2);   
    
def stopReceptorEDI():
    context.server_RSFENS.stop();
    del context.server_RSFENS;
    del context.scenario_RSFENS;
    del context.config_RSFENS;
    
    context.server_MFNDCQ.stop();
    del context.server_MFNDCQ;
    del context.scenario_MFNDCQ;
    del context.config_MFNDCQ;
    
    routerConfig = Config();
    routerConfig.host = os.getenv('TTS_ROUTER_HOST', 'TTSRouter.nce.amadeus.net')
    routerConfig.port = int(os.getenv('TTS_ROUTER_ADMIN_PORT', '34350'))
    router = RemoteRouter(routerConfig);
    router.remove(global_regression.ROUTINGKEY+"_EDI_RSFENS");
    router.remove(global_regression.ROUTINGKEY+"_EDI_MFNDCQ");
    time.sleep(1);

def checkMatched_MFNDCQ(nbMsg):
    if not hasattr(context.server_MFNDCQ.context, 'matched_message') or context.server_MFNDCQ.context.matched_message != nbMsg:
        print "ERROR: Only ", context.server_MFNDCQ.context.matched_message, " / ", nbMsg, " MFNDCQ request(s) matched"
        return TTServer.currentMessage.TTS_MATCH_COMPARISON_FAILURE
    else:
        print "MFNDCQ request(s) matched"
        return TTServer.currentMessage.TTS_MATCH_COMPARISON_OK

def checkMatched_RSFENS(nbMsg = 1):
    received_message = 0
    if hasattr(context.server_RSFENS.context, 'matched_message'):
        received_message = context.server_RSFENS.context.matched_message
    if received_message != nbMsg:
        print "ERROR: Only ", context.server_RSFENS.context.matched_message, " / ", nbMsg, " RSFENS request matched"
        return TTServer.currentMessage.TTS_MATCH_COMPARISON_FAILURE
    else:
        if received_message == 0:
            print "RSFENS no request received"
        else:
            print "RSFENS request matched"
        return TTServer.currentMessage.TTS_MATCH_COMPARISON_OK

def checkMatchedEDI(nbMsgMFNDCQ, nbMsgRSFENS = 1):
    mfndcq_out = checkMatched_MFNDCQ(nbMsgMFNDCQ)
    rsfens_out = checkMatched_RSFENS(nbMsgRSFENS)
    
    if mfndcq_out != TTServer.currentMessage.TTS_MATCH_COMPARISON_OK or rsfens_out != TTServer.currentMessage.TTS_MATCH_COMPARISON_OK:
        return TTServer.currentMessage.TTS_MATCH_COMPARISON_FAILURE
    else:
        return TTServer.currentMessage.TTS_MATCH_COMPARISON_OK
    
def incMatchedMessage():
    context.matched_message += 1
    return TTServer.currentMessage.TTS_MATCH_COMPARISON_OK
    
#####################
### Support functions ###
#####################
def createReceptor(iMsg, iPort):
    aFileName = os.path.splitext(os.path.basename(global_regression.TTS_ScenarioPath))[0];
    
    #Config (Edifact/XML...)
    aConfig = Config();
    aConfig.transportHeader = TH_ERPLv2;
    aConfig.host = "localhost";
    aConfig.port = iPort;
    aConfig.tps = 0.0;
    aConfig.timeout = 100.0;
    aConfig.sessionHeader = SH_EDI;
    aConfig.conversationType = CT_Stateless;
    aConfig.ediCharSet = ECS_IATB;
    aConfig.newlineCharacter = NLC_None;
    aConfig.releaseCharacter = "\\";
    aConfig.maxSize = 7168000;
    aConfig.compareMode = CM_AllWithoutDCX;
    aConfig.useMultiLineRegularExpressions = 0;
    aConfig.onTimeout = RTO_Stop;
    
    #Scenario (gsv files)
    injectorFile = os.path.join("tools", aFileName + "_" + iMsg + ".gsv");
    #injectorFile = os.path.join("tools", "test.gsv");
    logFile = injectorFile + ".log";
    resFile = injectorFile + ".res";
    rexFile = injectorFile + ".rex";
    
    aScenario = Scenario(aConfig, injectorFile);
    aScenario.load();
    
    #Receptor
    aReceptor = Server(aConfig, aScenario, logFile, resFile, rexFile);
    
    aReceptor.context.matched_message = 0

    return aConfig, aScenario, aReceptor


def addRoutingKey(iMsg, iPort):
    # Register to the router
    routerConfig = Config();
    routerConfig.host = os.getenv('TTS_ROUTER_HOST', 'TTSRouter.nce.amadeus.net')
    routerConfig.port = int(os.getenv('TTS_ROUTER_ADMIN_PORT', '34350'))
    router = RemoteRouter(routerConfig);
    
    routingConfig = Config();
    routingConfig.fse = "*";
    routingConfig.destin = "*";
    routingConfig.origin = global_regression.ROUTINGKEY;
    routingConfig.svcName = iMsg;
    routingConfig.atid = "*";
    #routingConfig.getDCXconfig().addSecurityData(DCXdata.SEC_USER_ID,"USER ID")
    #routingConfig.getDCXconfig().addSecurityData(DCXdata.SEC_OFFICE_ID,"OFFICE ID")
    routingConfig.host = global_regression.ROUTINGNAME
    routingConfig.port = iPort;

    #sending the message to the router to create your key
    router.addRoutingKey(global_regression.ROUTINGKEY+"_EDI_"+iMsg, routingConfig);
    print "Add router ROUTINGKEYNAME: ", global_regression.ROUTINGKEY+"_EDI_"+iMsg, " ROUTINGKEY: ", routingConfig.origin, " ROUTINGNAME: ", global_regression.ROUTINGNAME, " RECEPTOR_PORT: ", iPort

    
    

