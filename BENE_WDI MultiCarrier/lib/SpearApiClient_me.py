import json
import os
import sys
import urllib2
import paramiko
import sqlite3
from TTServer import  currentMessage



SPEAR_MODE = "REMOTE" # "LOCAL", "REMOTE"
win_uname = "mishrap"
win_pwd = "Amadeus@16"
UDIR = 'D:\\'
FNAME = 'spear.json'
Test_platform = "PDT"

SERVER_NAME = "ncetsplnx59"
SERVER_UNAME = "railpta"
SERVER_PWD = "jkz8XCV+"


class SpearRemote(object):
    def __init__(self):
        self.ssh = paramiko.SSHClient()
        self.ssh.set_missing_host_key_policy(
            paramiko.AutoAddPolicy())
    def connect(self):
        self.ssh.connect(SERVER_NAME, username=SERVER_UNAME,
                password=SERVER_PWD)
        #print(self.execute("cd /remote/tspprojteams/projects/radscm/RailPnrApp/server/"))
    def close(self):
        self.ssh.close()
    def execute(self, command):
        stdin, stdout, stderr = self.ssh.exec_command(command)
        return stdout.read()
    def getPnrData(self, scenario_id):
        self.connect()
        cmd = ("cd /remote/tspprojteams/projects/radscm/RailPnrApp/server/;sqlite3 pnr.db "
               "'select old_rloc, new_rloc, phase, outward_date_time, inward_date_time from railpnrapp_pnrrecord where scenario_id=\"{}\"';"
        ).format(scenario_id)
        stdout = self.execute(cmd)
        #old_rloc, new_rloc, phase, date_out, date_in 
        stdout = stdout.split('|')
        stdout = map(lambda x: '' if x == '\n' else x, stdout)
        return stdout
       



def savePnr(scenario_id, rloc, from_phase = "PDT", savepnr = "TRUE", user_id="LOCAL", comment="NA", datein = "", dateout = ""):
    '''
    Saves the scenario_id and rloc
    '''
    try:
        assert SPEAR_MODE == "LOCAL"
        if os.path.isfile(os.path.join(os.path.expanduser(UDIR), FNAME)):
            fp = open(os.path.join(os.path.expanduser(UDIR), FNAME), mode='r')
            SPEAR_DATA = json.load(fp)
            fp.close()
        else:
            SPEAR_DATA = {}
        #print SPEAR_DATA
        scenario_id = unicode(scenario_id, "utf-8")
        if savepnr.upper() == "FALSE":
            print "Save PNR disabled"
            print "Scenario ID already present."
        elif savepnr.upper() == "TRUE":
            if scenario_id in SPEAR_DATA:
                print "Scenario ID already present. Overwriting"
            fp = open(os.path.join(os.path.expanduser(UDIR), FNAME), mode='w+')
            data = {}
            data[u"orig_rloc"] =  unicode(rloc, "utf-8")
            data[u"date_in"] =  unicode(datein, "utf-8")
            data[u"date_out"] =  unicode(dateout, "utf-8")
            data[u"new_rloc"] = u""
            data[u"phase"] = from_phase
            SPEAR_DATA[scenario_id] = data
            #print SPEAR_DATA
            json.dump(SPEAR_DATA, fp)
            fp.close()
    except Exception as e:
            print e
    return currentMessage.TTS_MATCH_COMPARISON_OK  


def consumePNR(scenario_id, phase = "PDT", user_id = "LOCAL"):
    '''
    Saves the scenario_id and rloc
    '''
    try:
        if SPEAR_MODE == "LOCAL":
            fp = open(os.path.join(os.path.expanduser(UDIR), FNAME), mode='r')
            SPEAR_DATA = json.load(fp)
            fp.close()
            #print SPEAR_DATA
            scenario_id = unicode(scenario_id, "utf-8")
            if scenario_id not in SPEAR_DATA:
                print "Scenario ID not present."
                return None
            else:
                orig_rloc = str(SPEAR_DATA[scenario_id][u"orig_rloc"])
                from_phase = SPEAR_DATA[scenario_id][u'phase']
                return injectPnr(from_phase, phase, orig_rloc)
        elif SPEAR_MODE == "REMOTE":
            spear = SpearRemote()
            old_rloc, new_rloc, from_phase, date_out, date_in = spear.getPnrData(scenario_id)

            return injectPnr(from_phase, phase, old_rloc)

    except Exception as e:
        print e

def injectPnr(from_phase, to_phase, old_rloc):
    try:
        url = 'http://obevt051.muc.amadeus.net:1234/api/?username={0}&password={1}&from={2}&rloc={3}&to={4}'.format(win_uname, win_pwd,
                            from_phase, old_rloc, to_phase)
        print url
        data = json.loads(urllib2.urlopen(url).read())
        print data[u'result'][unicode(old_rloc)][u"new_rloc"]
        return str(data[u'result'][unicode(old_rloc)][u"new_rloc"])
    except Exception as e:
        print e

def get_inward_datetime(scenario_id, from_phase, user_id="TEST"):
    try:
        if SPEAR_MODE == "LOCAL":
            fp = open(os.path.join(os.path.expanduser(UDIR), FNAME), mode='r')
            SPEAR_DATA = json.load(fp)
            fp.close()
            scenario_id = unicode(scenario_id, "utf-8")
            return str(SPEAR_DATA[scenario_id][u'date_in'])
        elif SPEAR_MODE == "REMOTE":
            spear = SpearRemote()
            old_rloc, new_rloc, from_phase, date_out, date_in = spear.getPnrData(scenario_id)
            return date_in
    except Exception:
        pass



def get_outward_datetime(scenario_id, from_phase, user_id="TEST"):
    try:
        if SPEAR_MODE == "LOCAL":
            fp = open(os.path.join(os.path.expanduser(UDIR), FNAME), mode='r')
            SPEAR_DATA = json.load(fp)
            fp.close()
            scenario_id = unicode(scenario_id, "utf-8")
            return str(SPEAR_DATA[scenario_id][u'date_out'])
        elif SPEAR_MODE == "REMOTE": 
            spear = SpearRemote()
            old_rloc, new_rloc, from_phase, date_out, date_in = spear.getPnrData(scenario_id)
            return date_out
    except Exception:
        pass

def extract_date_time(datetime):
    try:
        if datetime is not None and datetime != "NA":
            date, time = datetime.split("T")
            return (date.replace('-',''),time.replace(':','')[:4])
    except Exception:
        pass



def main():
    #save_rloc("BKG_6", "36K3GL", "2016-12-01", "2016-12-05" )
   # print get_date_in("22")
    #print get_date_out("22")
    #print gen_rloc("BKG_6", "PDT")
    scid = "FRR_RDP_5_4_2_BOOKING_WDI25_BKG_001_A"
    print consumePNR(scid)
   

if __name__ == "__main__":
    main()
