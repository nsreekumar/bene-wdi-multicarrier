#!/usr/bin/env python

import sys
import time
import datetime
import simplejson as json
import alfLib




# This function will search the alf logs between 'timeBegin' and 'timeBegin+nSeconds' for all SeiProtocolWrapperRequest messages.
# If will return the list of messages, or None in case of error / nothing found
#parameters:
#  * alfUser         - a valid alf user
#  * alfPassword - alf password
#  * timeBegin     - time to begin a search. String in the format '%Y-%m-%d_%H:%M:%S'
#  * nSeconds     - number of seconds for the validity of the search
def alfSearchSeiProtocolWrapperRequestMessages(alfUser, alfPassword, timeBegin, nSeconds):
    if alfUser is None:
        print 'Error: Please provide a valid alf user'
        return None

    if alfPassword is None:
        print 'Error: Please provide a valid alf password'
        return None

    try:
        # PDT value id
        searchId = 1015
        print 'Performing a search in Alf logs with searchId='+str(searchId)
        data = alfLib.performAlfSearch(alfUser, alfPassword, timeBegin, nSeconds, "<SeiProtocolWrapperRequest", searchId)
    except: # catch *all* exceptions
        print "ERROR: exception caught!"
        print str(sys.exc_info())
        print str(sys.exc_info()[0])
        exit()
    
    if data is None:
        print "**** Alf search is empty! Nothing found!"
        return None

    dataJason = json.loads(data)
    listMessagesJson = dataJason['message']
    outputList = []
    for message in listMessagesJson:
       if message['data'].find('SeiProtocolWrapperRequest') != -1:
           outputList.append(message['data'])
               
    return outputList;






# This function will search the alf logs between 'timeBegin' and 'timeBegin+nSeconds' for all messages containing the string 'search'.
# If will return true if and only if:
#        - in the messages retrieved fromAlf, there is at least a SeiProtocolWrapperRequest message containing '/ama-ers/21_0/services/BP'
# Else, it will return False!
#parameters:
#  * alfUser         - a valid alf user
#  * alfPassword - alf password
#  * timeBegin     - time to begin a search. String in the format '%Y-%m-%d_%H:%M:%S'
#  * nSeconds     - number of seconds for the validity of the search
#  * search          - pattern to search in Alf. For example, the ATID
def alfLogContainsSeiProtocolWrapperToEurostar(alfUser, alfPassword, timeBegin, nSeconds, search):
    if alfUser is None:
        print 'Error: Please provide a valid alf user'
        return False

    if alfPassword is None:
        print 'Error: Please provide a valid alf password'
        return False

    try:
        # PDT value id
        searchId = 1015
        print 'Performing a search in Alf logs with searchId='+str(searchId)
        data = alfLib.performAlfSearch(alfUser, alfPassword, timeBegin, nSeconds, search, searchId)
    except: # catch *all* exceptions
        print "ERROR: exception caught!"
        print str(sys.exc_info())
        print str(sys.exc_info()[0])
        exit()
    
    if data is None:
        print "**** Alf search is empty! Nothing found!"
        return False

    dataJason = json.loads(data)
    #print "dataJason = "+str(dataJason)
    
    listMessages = dataJason['message']
    for message in listMessages:
       if message['data'].find('SeiProtocolWrapperRequest') != -1:
           if message['data'].find('/ama-ers/21_0/services/BP') != -1:
               print "Found in Alf logs a 'SeiProtocolWrapperRequest' with '/ama-ers/21_0/services/BP'"
               return True;
    
    print "**** Could not find  in Alf logs a 'SeiProtocolWrapperRequest' with '/ama-ers/21_0/services/BP'"
    return False;
