import tarfile
import datetime
import os

routerConfig = Config();
routerConfig.host = "ttsrouter.nce.amadeus.net";
#routerConfig.port = global_regression.ROUTERPORT;
phase = global_regression.test_platform
port = 34350
if phase == "FRT":
    port = 34351

routerConfig.port = port;
router = RemoteRouter(routerConfig);
router.remove(global_regression.ROUTINGNAME+"FR54");
router.remove(global_regression.ROUTINGNAME+"FR60");
router.remove('1A09B569C7_EDI_PCOMGQ')
router.remove('1AREGSGBRAILTRE54')
router.remove('IEVL545SJ54')
router.remove('1A09AA0735FR54')
router.remove('BLRL76524FR54')
print os.path.abspath(os.path.dirname(__file__))

now = datetime.datetime.now()
y = str(now.year)
if len(str(now.month)) == 1:
    m = "0"+str(now.month)
else:
    m = str(now.month)
if len(str(now.day)) == 1:
    d = "0"+str(now.day)
else:
    d = str(now.day)