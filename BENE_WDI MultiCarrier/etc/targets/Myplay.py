# Create the configuration

regression.config = Config();
regression.config.transportHeader = TH_CIL;
regression.config.host = "194.156.170.117";
regression.config.port = 8051;
#regression.config.port = 18011;
regression.config.timeout = 12.0;
regression.config.tps = 0.0;
regression.config.cps = 0.0;
regression.config.sessionHeader = SH_XML;
regression.config.conversationType = CT_Stateful;
regression.config.ediCharSet = ECS_XML;
regression.config.newlineCharacter = NLC_None;
regression.config.releaseCharacter = "\\";
regression.config.maxSize = 409600;
regression.config.compareMode = CM_AllWithoutDCX;
regression.config.origin = global_regression.FROM;
regression.config.destin = global_regression.EDIFACTSI;
regression.config.carf = global_regression.ATID;
regression.config.useMultiLineRegularExpressions = 0;
regression.config.onTimeout = RTO_Stop;
regression.config.initContextScript = "";
regression.config.encodeMessageFunction = "";
regression.config.decodeMessageFunction = "";

if global_regression.test_platform == 'PRD':
	regression.config.transportHeader = TH_ERPLv2;
	regression.config.host = "194.156.170.117";
	regression.config.port = 8051;
	regression.config.timeout = 10.0;
	regression.config.sessionHeader = SH_EDI;
	regression.config.conversationType = CT_Stateless;
	regression.config.ediCharSet = ECS_IATB;
elif global_regression.test_platform == 'PDT':	
	regression.config.transportHeader = TH_ERPLv2;
	regression.config.host = "194.156.170.117";
	regression.config.port = 8051;
	regression.config.timeout = 100.0;
	regression.config.tps = 0.0;
	regression.config.cps = 0.0;
	regression.config.sessionHeader = SH_EDI;
	regression.config.conversationType = CT_Stateless;
	regression.config.ediCharSet = ECS_IATB;
elif global_regression.test_platform == 'LOCALPDT':	
	regression.config.transportHeader = TH_ERPLv2;
	regression.config.host = "194.156.170.117";
	regression.config.port = 8051;
	regression.config.timeout = 100.0;
	regression.config.tps = 0.0;
	regression.config.cps = 0.0;
	regression.config.sessionHeader = SH_EDI;
	regression.config.conversationType = CT_Stateless;
	regression.config.ediCharSet = ECS_IATB;
elif global_regression.test_platform == 'MIG':	
	regression.config.transportHeader = TH_ERPLv2;
	regression.config.host = "194.156.170.117";
	regression.config.port = 8051;
	regression.config.timeout = 10.0;
	regression.config.sessionHeader = SH_EDI;
	regression.config.conversationType = CT_Stateless;
	regression.config.ediCharSet = ECS_IATB;
elif global_regression.test_platform == 'FRT':
	regression.config.transportHeader = TH_ERPLv2;
	regression.config.host = "194.156.170.117";
	regression.config.port = 8051;
	regression.config.timeout = 10.0;
	regression.config.sessionHeader = SH_EDI;
	regression.config.conversationType = CT_Stateless;
	regression.config.ediCharSet = ECS_IATB;