import socket;

# Create the configuration
regression.config = Config();
regression.config.transportHeader = TH_ERPLv2;
regression.config.host = "localhost";
regression.config.port = 61529;
#regression.config.port = 34680;
regression.config.tps = 0.0;
regression.config.timeout = 80.0;
regression.config.sessionHeader = SH_XML;
regression.config.ediCharSet = ECS_XML;
regression.config.newlineCharacter = NLC_None;
regression.config.releaseCharacter = "\\";
regression.config.maxSize = 409600;
regression.config.compareMode = CM_AllWithoutDCX;

# Create the server
regression.server = Server(regression.config);
regression.server.browsingMode = SBM_Sequential;
regression.config.useMultiLineRegularExpressions = 0;
regression.config.onTimeout = RTO_Stop;

# Register to the router
routerConfig = Config();
routerConfig.host = "TTSRouter.nce.amadeus.net";
routerConfig.port = 34350;
router = RemoteRouter(routerConfig);
router.add(global_regression.ROUTINGNAME+"FR54", global_regression.ROUTINGKEY,socket.gethostbyname(socket.gethostname()), 61529);
# router.remove(global_regression.ROUTINGNAME+"FR54");
# router.remove("NCEL31625FR54FR54");
