# Create the configuration

regression.config = Config();
regression.config.transportHeader = TH_ERPLv2;
regression.config.timeout = 30.0;
regression.config.tps = 0.0;
regression.config.cps = 0.0;
regression.config.sessionHeader = SH_EDI;
regression.config.conversationType = CT_Stateful;
regression.config.ediCharSet = ECS_IATB;
regression.config.newlineCharacter = NLC_None;
regression.config.releaseCharacter = "\\";
regression.config.maxSize = 409600;
regression.config.origin = "1A0DSPPG3";
regression.config.destin = "1ASRCDSPBP1";
regression.config.useMultiLineRegularExpressions = 0;
regression.config.onTimeout = RTO_Stop;


regression.config.host = "194.156.170.207";
regression.config.port = 8150;
