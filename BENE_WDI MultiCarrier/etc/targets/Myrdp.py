# Create the configuration


regression.config = Config();
regression.config.transportHeader = TH_CIL;
regression.config.host = "194.156.170.117";
regression.config.port = 18011;
regression.config.timeout = 30.0;
regression.config.tps = 0.0;
regression.config.cps = 0.0;
regression.config.sessionHeader = SH_XML;
regression.config.conversationType = CT_Stateful;
regression.config.ediCharSet = ECS_XML;
regression.config.newlineCharacter = NLC_None;
regression.config.releaseCharacter = "\\";
regression.config.maxSize = 409600;
regression.config.compareMode = CM_AllWithoutDCX;
regression.config.origin = global_regression.FROM;		#not in PRD
regression.config.destin = global_regression.XMLSI;		#not in PRD
regression.config.svcID = "2";
regression.config.carf = global_regression.ATID;		#not in PDT, MIG, FVT
regression.config.useMultiLineRegularExpressions = 0;
regression.config.onTimeout = RTO_Stop;
regression.config.initContextScript = "";
regression.config.encodeMessageFunction = "";
regression.config.decodeMessageFunction = "";

if ( global_regression.test_platform == 'PRD' ):
	regression.config.host = "172.17.29.68";
	regression.config.port = 18011;
	regression.config.timeout = 10.0;
	regression.config.origin = "";
	regression.config.destin = "";
	regression.config.svcName = "Rail_PingInPNR";
	regression.config.svcID = "1";
elif ( global_regression.test_platform == 'PDT' ):
	regression.config.timeout = 100.0;
	regression.config.svcName = "Rail_PingInPNR";
	regression.config.svcID = "1";
elif ( global_regression.test_platform == 'MIG' ):
	regression.config.timeout = 10.0;
	regression.config.svcName = "Rail_PingInPNR";
	regression.config.svcID = "1";
elif ( global_regression.test_platform == 'FRT' ):
	regression.config.timeout = 80.0;
	regression.config.svcName = "Rail_PingInPNR";
	regression.config.svcID = "1";
elif ( global_regression.test_platform == 'LOCALPDT' ):
	regression.config.timeout = 100.0;
	regression.config.svcName = "Rail_PingInPNR";
	regression.config.svcID = "1";
	regression.config.origin = global_regression.ROUTINGKEY;
	regression.config.destin = "1ASIXRDPLOC";


