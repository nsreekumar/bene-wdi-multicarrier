# Create the configuration
regression.config = ConfigWEB();
regression.config.web_host = "test.webservices.amadeus.com";
regression.config.web_port = "443";
regression.config.web_method = "GET";
regression.config.timeout =30;
regression.config.web_useHTTPS = 1;
regression.config.useMultiLineRegularExpressions = 0;
regression.config.onTimeout = 30;
regression.config.maxSize = 40960000;

#regression.config.onTimeout = RTO_Stop;