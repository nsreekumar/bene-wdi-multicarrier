# Create the configuration
regression.config = Config();
regression.config.transportHeader = TH_CIL;
regression.config.host = "172.17.246.37";
regression.config.port = 18005;
regression.config.timeout = 50.0;
regression.config.tps = 0.0;
regression.config.cps = 0.0;
regression.config.sessionHeader = SH_XML;
regression.config.conversationType = CT_Stateful;
regression.config.ediCharSet = ECS_XML;
regression.config.newlineCharacter = NLC_None;
regression.config.releaseCharacter = "\\";
regression.config.maxSize = 1024000;
regression.config.compareMode = CM_AllWithoutDCX;
regression.config.origin = global_regression.FROM;
regression.config.destin = global_regression.XMLSI;
regression.config.svcID = "2";
regression.config.carf = global_regression.ATID;
regression.config.useMultiLineRegularExpressions = 0;
regression.config.onTimeout = RTO_Stop;
regression.config.initContextScript = "";
regression.config.encodeMessageFunction = "";
regression.config.decodeMessageFunction = "";


