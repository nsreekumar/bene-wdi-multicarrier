# Create the configuration
regression.config = Config();
regression.config.transportHeader = TH_CIL;
regression.config.host = global_regression.TICKETACK;
regression.config.port = 47101;
regression.config.timeout = 10.0;
regression.config.tps = 0.0;
regression.config.cps = 0.0;
regression.config.sessionHeader = SH_Raw;
regression.config.conversationType = CT_Stateful;
regression.config.ediCharSet = ECS_XML;
regression.config.newlineCharacter = NLC_None;
regression.config.releaseCharacter = "\\";
regression.config.maxSize = 409600;
regression.config.compareMode = CM_AllWithoutDCX;
regression.config.origin = "";
regression.config.destin = "";
regression.config.useMultiLineRegularExpressions = 1;
regression.config.onTimeout = RTO_Stop;

