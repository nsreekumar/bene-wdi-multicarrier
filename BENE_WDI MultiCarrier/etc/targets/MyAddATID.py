# Create the configuration

regression.config = Config(); 
regression.config.transportHeader = TH_CIL;
regression.config.timeout = 10.0;
regression.config.tps = 0.0; 
regression.config.cps = 0.0; 
regression.config.sessionHeader = SH_XML;
regression.config.conversationType = CT_Stateful; 
regression.config.ediCharSet = ECS_XML;
regression.config.newlineCharacter = NLC_None; 
regression.config.releaseCharacter = "\\"; 
regression.config.maxSize = 409600;
regression.config.compareMode = CM_AllWithoutDCX;
regression.config.origin = global_regression.FROM
regression.config.destin =  global_regression.XMLSI;
regression.config.svcName = "Rail_PingInPNR";                          # not in DEV and UAT
regression.config.svcID = "1";
regression.config.useMultiLineRegularExpressions = 0; 
regression.config.onTimeout = RTO_Stop; 

if global_regression.test_platform == 'PRD':
	regression.config.host = "194.156.170.117";
	regression.config.port = 18011;
elif global_regression.test_platform == 'PDT' or global_regression.test_platform == 'LOCALPDT':
	regression.config.host = "194.156.170.117";
	regression.config.port = 18011;
elif global_regression.test_platform == 'MIG':
	regression.config.host = "194.156.170.117";
	regression.config.port = 18011;
elif global_regression.test_platform == 'FRT':
	regression.config.host = "194.156.170.117";
	regression.config.port = 18011;
elif global_regression.test_platform == 'DEV':
	regression.config.host = "194.156.170.117";
	regression.config.port = 18011;
	regression.config.timeout = 30.0;
	regression.config.svcID = "2";
	regression.config.carf = global_regression.ATID;
	regression.config.initContextScript = "";
	regression.config.encodeMessageFunction = "";
	regression.config.decodeMessageFunction = "";
elif global_regression.test_platform == 'UAT':
	regression.config.host = "194.156.170.117";
	regression.config.port = 18011;
	regression.config.timeout = 30.0;
	regression.config.svcID = "2";
	regression.config.carf = global_regression.ATID;
	regression.config.initContextScript = "";
	regression.config.encodeMessageFunction = "";
	regression.config.decodeMessageFunction = "";