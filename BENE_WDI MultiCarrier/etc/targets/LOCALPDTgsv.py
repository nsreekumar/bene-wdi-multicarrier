import socket;

# Create the configuration
regression.config = Config();
regression.config.transportHeader = TH_ERPLv2;
regression.config.host = "localhost";
regression.config.port = 61528;
regression.config.tps = 0.0;
regression.config.sessionHeader = SH_XML;
regression.config.ediCharSet = ECS_XML;
regression.config.newlineCharacter = NLC_None;
regression.config.releaseCharacter = "\\";
regression.config.maxSize = 409600;
regression.config.compareMode = CM_AllWithoutDCX;
regression.config.useMultiLineRegularExpressions = 0;
regression.config.onTimeout = RTO_Stop;
# 
# Create the server
regression.server = Server(regression.config);
regression.server.browsingMode = SBM_Sequential;

# Register to the router
routerConfig = Config();
routerConfig.host = "TTSRouter.nce.amadeus.net";
routerConfig.port = 34350;
router = RemoteRouter(routerConfig);
router.add(global_regression.ROUTINGNAME+"FR54", global_regression.ROUTINGKEY, socket.gethostbyname(global_regression.ROUTINGNAME), 61528);


# Check that all messages are received by the receptor:
#regression.config.waitForAllMessages = 1;
