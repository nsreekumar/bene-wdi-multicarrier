# Create the configuration

regression.config = Config();
regression.config.transportHeader = TH_ERPLv2;
regression.config.host = "194.156.170.207";
regression.config.port = 8150;
# regression.config.host = "172.17.201.71";
# regression.config.port = 8001;
regression.config.timeout = 120.0;
regression.config.tps = 0.0;
regression.config.cps = 0.0;
regression.config.sessionHeader = SH_EDI;
regression.config.conversationType = CT_Stateless;
regression.config.ediCharSet = ECS_IATB;
regression.config.newlineCharacter = NLC_None;
regression.config.releaseCharacter = "\\";
regression.config.maxSize = 409600;
regression.config.compareMode = CM_AllWithoutDCX;
regression.config.origin = global_regression.FROM;
regression.config.destin = "1ASIPAP";
regression.config.carf = global_regression.ATID;
regression.config.useMultiLineRegularExpressions = 0;
regression.config.onTimeout = RTO_Stop;


if global_regression.test_platform == 'MIG':
#	regression.config.host = "194.156.170.117";
#	regression.config.port = 8051;
	regression.config.timeout = 60.0;
	regression.config.destin = "1ASIPAPM";
	regression.config.initContextScript = "";
	regression.config.encodeMessageFunction = "";
	regression.config.decodeMessageFunction = "";
elif ( global_regression.test_platform == 'DEV' ):
	regression.config.destin = "1ASIGSBRD";
elif ( global_regression.test_platform == 'FRT' ):
	regression.config.timeout = 60.0;
	regression.config.destin = "1ASIPAPF";
	regression.config.initContextScript = "";
	regression.config.encodeMessageFunction = "";
	regression.config.decodeMessageFunction = "";