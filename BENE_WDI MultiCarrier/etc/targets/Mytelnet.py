# Create the configuration
regression.config = ConfigSH();
regression.config.sh_login = "rdponcal";
regression.config.timeout = 60;
regression.config.sh_useSSH = 1;
regression.config.useMultiLineRegularExpressions = 0;
regression.config.onTimeout = RTO_Stop;

if global_regression.test_platform == 'UAT':
	regression.config.sh_host = "lgsuatrdp.muc.amadeus.net";
	regression.config.sh_cryptedPassword = "YW1hZGV1czE=";
elif global_regression.test_platform == 'DEV':
	regression.config.sh_host = "lgsdevrdp.muc.amadeus.net";
	regression.config.sh_cryptedPassword = "YW1hZGV1cw==";
elif global_regression.test_platform == 'FVT':
	regression.config.sh_host = "lgsfvtrdp.muc.amadeus.net";
	regression.config.sh_cryptedPassword = "YW1hZGV1czE=";
elif global_regression.test_platform == 'MIG':
	regression.config.sh_host = "lgsmigrdp.muc.amadeus.net";
	regression.config.sh_cryptedPassword = "YW1hZGV1czE=";
elif global_regression.test_platform == 'PDT' or global_regression.test_platform == 'LOCALPDT':
	regression.config.sh_host = "lgspdtrdp.muc.amadeus.net";
	regression.config.sh_cryptedPassword = "YW1hZGV1czE=";
elif global_regression.test_platform == 'PRD':
	regression.config.sh_host = "lgsaaardp.muc.amadeus.net";
	regression.config.sh_cryptedPassword = "YW1hZGV1czE=";