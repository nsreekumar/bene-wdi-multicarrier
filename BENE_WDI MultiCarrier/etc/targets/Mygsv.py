import socket;

if global_regression.test_platform == 'PDT' or global_regression.test_platform == 'LOCALPDT':
	regression.config = Config();
	regression.config.transportHeader = TH_ERPLv2;
	regression.config.host = "localhost";
	regression.config.port = 61528;
	regression.config.tps = 0.0;
	regression.config.sessionHeader = SH_XML;
	regression.config.ediCharSet = ECS_XML;
	regression.config.newlineCharacter = NLC_None;
	regression.config.releaseCharacter = "\\";
	regression.config.maxSize = 409600;
	regression.config.compareMode = CM_AllWithoutDCX;
	regression.config.useMultiLineRegularExpressions = 0;
	regression.config.onTimeout = RTO_Stop;

#	Create the server
	regression.server = Server(regression.config);
	regression.server.browsingMode = SBM_Sequential;

#	Register to the router
	routerConfig = Config();
	routerConfig.host = "TTSRouter.nce.amadeus.net";
	routerConfig.port = 34350;
	router = RemoteRouter(routerConfig);

#	router.add(global_regression.ROUTINGNAME+"FR54", global_regression.ROUTINGKEY, global_regression.ROUTINGNAME, 61528);
	router.add(global_regression.ROUTINGNAME+"FR54", global_regression.ROUTINGKEY, socket.gethostbyname(global_regression.ROUTINGNAME), 61528);
#	router..remove("NCEL1544FR54");
	
if global_regression.test_platform == 'UAT':
#	Create the configuration
	regression.config = Config();
	regression.config.transportHeader = TH_ERPLv2;
	regression.config.host = "localhost";
	regression.config.port = 40000;
	regression.config.tps = 0.0;
	regression.config.sessionHeader = SH_XML;
	regression.config.ediCharSet = ECS_XML;
	regression.config.newlineCharacter = NLC_None;
	regression.config.releaseCharacter = "\\";
	regression.config.maxSize = 409600;
	regression.config.compareMode = CM_AllWithoutDCX;
	regression.config.useMultiLineRegularExpressions = 0;
	regression.config.onTimeout = RTO_Stop;

#	Create the server
	regression.server = Server(regression.config);
	regression.server.browsingMode = SBM_Sequential;

#	Register to the router
	routerConfig = Config();
	routerConfig.host = "TTSRouter.nce.amadeus.net";
	routerConfig.port = 34350;
	router = RemoteRouter(routerConfig);
#	router.add(global_regression.ROUTINGNAME+"FR54", global_regression.ROUTINGKEY, global_regression.ROUTINGNAME, 40000);
	router.add(global_regression.ROUTINGKEY+"FR54", global_regression.ROUTINGKEY, socket.gethostbyname(global_regression.ROUTINGNAME), 40000);
#	router.remove(global_regression.ROUTINGNAME+"FR54");

elif global_regression.test_platform == 'DEV':
#	Create the configuration
	regression.config = Config();
	regression.config.transportHeader = TH_ERPLv2;
	regression.config.host = "localhost";
	regression.config.port = 40000;
	regression.config.tps = 0.0;
	regression.config.sessionHeader = SH_XML;
	regression.config.ediCharSet = ECS_XML;
	regression.config.newlineCharacter = NLC_None;
	regression.config.releaseCharacter = "\\";
	regression.config.maxSize = 409600;
	regression.config.compareMode = CM_AllWithoutDCX;
	regression.config.useMultiLineRegularExpressions = 0;
	regression.config.onTimeout = RTO_Stop;

#	Create the server
	regression.server = Server(regression.config);
	regression.server.browsingMode = SBM_Sequential;

#	Register to the router
	routerConfig = Config();
	routerConfig.host = "TTSRouter.nce.amadeus.net";
	routerConfig.port = 34350;
	router = RemoteRouter(routerConfig);
#	router.add(global_regression.ROUTINGNAME+"FR54", global_regression.ROUTINGKEY, global_regression.ROUTINGNAME, 40000);
	router.add(global_regression.ROUTINGKEY+"FR54", global_regression.ROUTINGKEY, socket.gethostbyname(global_regression.ROUTINGNAME), 40000);
#	router.remove(global_regression.ROUTINGNAME+"FR54");

elif global_regression.test_platform == 'FRT':
#	Create the configuration
	regression.config = Config();
	regression.config.transportHeader = TH_ERPLv2;
	regression.config.host = "localhost";
	regression.config.port = 40000;
	regression.config.tps = 0.0;
	regression.config.sessionHeader = SH_XML;
	regression.config.ediCharSet = ECS_XML;
	regression.config.newlineCharacter = NLC_None;
	regression.config.releaseCharacter = "\\";
	regression.config.maxSize = 409600;
	regression.config.compareMode = CM_AllWithoutDCX;

#	Create the server
	regression.server = Server(regression.config);
	regression.server.browsingMode = SBM_Sequential;
	regression.config.useMultiLineRegularExpressions = 0;
	regression.config.onTimeout = RTO_Stop;

#	Register to the router
	routerConfig = Config();
	routerConfig.host = "TTSRouter.nce.amadeus.net";
	routerConfig.port = global_regression.ROUTERPORT;
	router = RemoteRouter(routerConfig);
	router.add(global_regression.ROUTINGNAME+"FR54", global_regression.ROUTINGKEY, socket.gethostbyname(global_regression.ROUTINGNAME), 40000);
#	router.add(global_regression.ROUTINGNAME+"FR54", global_regression.ROUTINGKEY, global_regression.ROUTINGNAME, 40000);
#	router.remove(global_regression.ROUTINGNAME+"FR54");
#  router.remove("1A09B5820F");







elif global_regression.test_platform == 'MIG':
#	Create the configuration
	regression.config = Config();
	regression.config.transportHeader = TH_ERPLv2;
	regression.config.host = "localhost";
	regression.config.port = 34543;
	regression.config.tps = 0.0;
	regression.config.sessionHeader = SH_XML;
	regression.config.ediCharSet = ECS_XML;
	regression.config.newlineCharacter = NLC_None;
	regression.config.releaseCharacter = "\\";
	regression.config.maxSize = 409600;
	regression.config.compareMode = CM_AllWithoutDCX;
	regression.config.useMultiLineRegularExpressions = 0;
	regression.config.onTimeout = RTO_Stop;

#	Create the server
	regression.server = Server(regression.config);
	regression.server.browsingMode = SBM_Sequential;

#	Register to the router
	routerConfig = Config();
	routerConfig.host = "TTSRouter.nce.amadeus.net";
	routerConfig.port = 34350;
	router = RemoteRouter(routerConfig);
#	router.add(global_regression.ROUTINGNAME+"FR54", global_regression.ROUTINGKEY, global_regression.ROUTINGNAME, 34543);
	router.add(global_regression.ROUTINGKEY+"FR54", global_regression.ROUTINGKEY, socket.gethostbyname(global_regression.ROUTINGNAME), 34543);

elif global_regression.test_platform == 'PRD':
	
#	Create the configuration
	regression.config = Config();
	regression.config.transportHeader = TH_ERPLv2;
	regression.config.host = "localhost";
	regression.config.port = 61528;
#	regression.config.port = 34543;
	regression.config.tps = 0.0;
	regression.config.sessionHeader = SH_XML;
	regression.config.ediCharSet = ECS_XML;
	regression.config.newlineCharacter = NLC_None;
	regression.config.releaseCharacter = "\\";
	regression.config.maxSize = 409600;
	regression.config.compareMode = CM_AllWithoutDCX;
	regression.config.useMultiLineRegularExpressions = 0;
	regression.config.onTimeout = RTO_Stop;

#	Create the server
	regression.server = Server(regression.config);
	regression.server.browsingMode = SBM_Sequential;

#	Register to the router
	routerConfig = Config();
	routerConfig.host = "TTSRouter.nce.amadeus.net";
	routerConfig.port = 34350;
	router = RemoteRouter(routerConfig);
#	router.add(global_regression.ROUTINGNAME+"FR54", global_regression.ROUTINGKEY, global_regression.ROUTINGNAME, 61528);
	router.add(global_regression.ROUTINGKEY+"FR54", global_regression.ROUTINGKEY, socket.gethostbyname(global_regression.ROUTINGNAME), 61528);
#	router.add(global_regression.ROUTINGNAME+"FR53", global_regression.ROUTINGKEY, global_regression.ROUTINGNAME, 34543);

