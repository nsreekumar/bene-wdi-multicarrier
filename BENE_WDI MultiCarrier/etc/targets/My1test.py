# Create the configuration
# config used in back office AIR tests
# Use this configuration to send cryptic entries to TPF BT environments.
# You are accessing to OBE world.
# You do NOT need to enter 1TEST and 1PDT at the beginning of your scripts as you are directly connected in PDT.
# You are connected exactly as the customer in production.

# Create the configuration
regression.config = Config();
regression.config.transportHeader = TH_TN3270;
regression.config.host = "sitst.tn3270.1a.amadeus.net";
if global_regression.BACKEND=='MIG':
    regression.config.port = 16063;
elif global_regression.BACKEND=='UAT': 
    regression.config.port = 16062;
elif global_regression.BACKEND=='FRT': 
    regression.config.port = 16067;
else: #PDT
    regression.config.port = 16002;
regression.config.timeout = 15.0;
regression.config.tps = 0.0;
regression.config.cps = 0.0;
regression.config.sessionHeader = SH_TN3270;
regression.config.conversationType = CT_Stateless;
regression.config.ediCharSet = ECS_Raw;
regression.config.newlineCharacter = NLC_None;
regression.config.releaseCharacter = "\\";
regression.config.maxSize = 409600;
regression.config.compareMode = CM_AllWithoutDCX;
regression.config.origin = global_regression.ATID;
regression.config.destin = "";
regression.config.useMultiLineRegularExpressions = 0;
regression.config.keepContext = 0;
regression.config.onTimeout = RTO_Continue;