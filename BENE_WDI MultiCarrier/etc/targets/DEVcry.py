# This configuration sends cryptic scenarios to BT regions through the SI.
# The origin parameter must contain the user's ATID.

# Create the configuration
regression.config = Config();
regression.config.transportHeader = TH_TN3270;
regression.config.host = "194.156.170.98";
regression.config.port = 23;
regression.config.timeout = 10.0;
regression.config.tps = 0.0;
regression.config.cps = 0.0;
regression.config.sessionHeader = SH_TN3270;
regression.config.conversationType = CT_Stateless;
regression.config.ediCharSet = ECS_Raw;
regression.config.releaseCharacter = "\\";
regression.config.maxSize = 409600;
regression.config.compareMode = CM_AllWithoutDCX;
regression.config.origin = global_regression.ATID;
regression.config.destin = "";
regression.config.useMultiLineRegularExpressions = 0;
regression.config.onTimeout = RTO_Continue;

