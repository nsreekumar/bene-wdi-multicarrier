# Create the configuration
regression.config = Config();
regression.config.transportHeader = TH_ERPLv2;
regression.config.host = "localhost";
regression.config.port = 40000;
regression.config.tps = 0.0;
regression.config.sessionHeader = SH_XML;
regression.config.ediCharSet = ECS_XML;
regression.config.newlineCharacter = NLC_None;
regression.config.releaseCharacter = "\\";
regression.config.maxSize = 409600;
regression.config.compareMode = CM_AllWithoutDCX;
regression.config.useMultiLineRegularExpressions = 0;
regression.config.onTimeout = RTO_Stop;
# 
# Create the server
regression.server = Server(regression.config);
regression.server.browsingMode = SBM_Sequential;

# Register to the router
routerConfig = Config();
routerConfig.host = "TTSRouter.nce.amadeus.net";
routerConfig.port = 34350;
router = RemoteRouter(routerConfig);
routingConfiguration = Config();
routingConfiguration.atid = global_regression.ATID;
routingConfiguration.host = socket.gethostbyname(global_regression.ROUTINGNAME);
routingConfiguration.port = 40000;
routingConfiguration.origin = "*";
routingConfiguration.destin = "*";
#router.add(global_regression.ROUTINGKEY+"FR54", global_regression.ROUTINGKEY, socket.gethostbyname(global_regression.ROUTINGNAME), 40000);
router.remove(global_regression.ROUTINGKEY + "FR54");
router.remove("1A09CC1125FR54");
router.remove("1AGOYALFR54");
router.addRoutingKey(global_regression.ROUTINGKEY + "FR54", routingConfiguration);