# Create the configuration

regression.config = ConfigSQL();				#not in DEV
regression.config.db_login = "rdponcall";

regression.config.db_keepConnection = 0;
regression.config.timeout = 10;
regression.config.useMultiLineRegularExpressions = 0;
regression.config.onTimeout = RTO_Continue;
regression.config.initContextScript = "";		#not in DEV, UAT
regression.config.encodeMessageFunction = "";	#not in DEV, UAT
regression.config.decodeMessageFunction = "";	#not in DEV, UAT
regression.config.decodeMode = DM_None;			#not in DEV, UAT


if global_regression.test_platform == 'MIG':
	regression.config.db_cryptedPassword = "YW1hZGV1czE=";
	regression.config.db_host = "172.17.45.64";
	regression.config.db_port = "31164";
	regression.config.db_sid = "RDPMIG";
elif ( global_regression.test_platform == 'FRT' ):
	regression.config.db_password = "Kuz643_7cc43211xy";
	regression.config.db_host = "172.17.193.108";
	regression.config.db_port = "33552";
	regression.config.db_sid = "RDPFVT";
elif ( global_regression.test_platform == 'DEV' ):
	regression.config.db_host = "172.17.16.170";
	regression.config.db_port = "31164";
	regression.config.db_sid = "RDPDEV";
	regression.config.db_login = "rdpprd";
	regression.config.db_cryptedPassword = "REVWX3NoZWxsMDQ=";
	regression.config.onTimeout = RTO_Stop;
elif ( global_regression.test_platform == 'UAT' ):
	regression.config.db_cryptedPassword = "YW1hZGV1czE=";
	regression.config.db_host = "172.17.45.64";
	regression.config.db_port = "31166";
	regression.config.db_sid = "RDPUAT";
	regression.config.onTimeout = RTO_Stop;
elif ( global_regression.test_platform == 'PDT' or global_regression.test_platform == 'LOCALPDT' ):
#	regression.config.db_cryptedPassword = "YW1hZGV1czE=";
	regression.config.db_password = "eErxy0_43tgb8823";
	regression.config.db_host = "172.17.197.162";
	regression.config.db_port = "33556";
	regression.config.db_sid = "RDPPDT";
