# Create the configuration
regression.config = Config();
regression.config.transportHeader = TH_CIL;
regression.config.host = "172.17.29.68";
regression.config.port = 18011;
regression.config.timeout = 50.0;
regression.config.tps = 0.0;
regression.config.cps = 0.0;
regression.config.sessionHeader = SH_XML;
regression.config.conversationType = CT_Stateful;
regression.config.ediCharSet = ECS_XML;
regression.config.newlineCharacter = NLC_None;
regression.config.releaseCharacter = "\\";
regression.config.maxSize = 409600;
regression.config.compareMode = CM_AllWithoutDCX;
regression.config.origin = "From";
regression.config.destin = global_regression.XMLSI;
regression.config.svcName = "Rail_PingInPNR";
regression.config.svcID = "1";
regression.config.useMultiLineRegularExpressions = 0;
regression.config.onTimeout = RTO_Stop;
regression.config.initContextScript = "";
regression.config.encodeMessageFunction = "";
regression.config.decodeMessageFunction = "";

