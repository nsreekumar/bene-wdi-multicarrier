# Create the configuration

regression.config = ConfigWEB();
regression.config.web_host = "test.webservices.amadeus.com";
regression.config.web_port = "443";
regression.config.web_method = "GET";
regression.config.web_useHTTPS = 1;
regression.config.useMultiLineRegularExpressions = 0;

regression.config.maxSize = 40960000;

if global_regression.test_platform == 'PDT' or global_regression.test_platform == 'SKL' or global_regression.test_platform == 'FRT':
	regression.config.timeout =30;	
	regression.config.onTimeout = 30;
elif global_regression.test_platform == 'UAT':	
	regression.config.timeout = 12;
	regression.config.onTimeout = 15;
