# Create the configuration
regression.config = Config();
regression.config.transportHeader = TH_ERPLv2;
regression.config.host = "194.156.170.207";
regression.config.port = 8180;
regression.config.timeout = 90.0;
regression.config.tps = 0.0;
regression.config.cps = 0.0;
regression.config.sessionHeader = SH_EDI;
regression.config.conversationType = CT_Stateless;
regression.config.ediCharSet = ECS_IATB;
regression.config.newlineCharacter = NLC_None;
regression.config.releaseCharacter = "\\";
regression.config.maxSize = 409600;
regression.config.compareMode = CM_AllWithoutDCX;
regression.config.origin = global_regression.FROM;
regression.config.destin = "1ASRRFDU";
regression.config.carf = global_regression.ATID;
regression.config.useMultiLineRegularExpressions = 0;
regression.config.onTimeout = RTO_Stop;
regression.config.initContextScript = "";
regression.config.encodeMessageFunction = "";
regression.config.decodeMessageFunction = "";

