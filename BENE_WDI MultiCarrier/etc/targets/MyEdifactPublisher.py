# config used to retrieve the back office AIR
# Create the configuration
regression.config = Config();
regression.config.transportHeader = TH_CIL;
regression.config.host = "194.156.170.207";

#select the right conf depending on the Backend:

if global_regression.BACKEND == 'FRT':
    print "configuration for EDI : FRT"
    regression.config.port = 8250;
    regression.config.destin = "1ASIPUBF"
elif global_regression.BACKEND == 'DEV':
    print "configuration for EDI : DEV"
    regression.config.port = 8250;
    regression.config.destin = "1ASIPUBD"
elif global_regression.BACKEND == 'MIG':
    print "configuration for EDI : MIG"
    regression.config.port = 8250;
    regression.config.destin = "1ASIPUBM"
elif global_regression.BACKEND == 'UAT':
    print "configuration for EDI : UAT"
    regression.config.port = 10208;
    regression.config.destin = "1ASIPUBU"
else:
    print "configuration for EDI : PDT"
    regression.config.destin = "1ASIPUB";
    regression.config.port = 10208;

regression.config.timeout = 100.0;
regression.config.tps = 0.0;
regression.config.cps = 0.0;
regression.config.sessionHeader = SH_EDI;
regression.config.conversationType = CT_Stateless;
regression.config.ediCharSet = ECS_IATB;
regression.config.newlineCharacter = NLC_None;
regression.config.releaseCharacter = "\\";
regression.config.maxSize = 409600;
regression.config.origin = "1APPC";
regression.config.carf = global_regression.ATID;
regression.config.useMultiLineRegularExpressions = 0;
regression.config.onTimeout = RTO_Stop;