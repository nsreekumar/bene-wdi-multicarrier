# Create the configuration
regression.config = Config();
regression.config.transportHeader = TH_CIL;
regression.config.timeout = 10.0;
regression.config.tps = 0.0;
regression.config.cps = 0.0;
regression.config.sessionHeader = SH_Raw;
regression.config.conversationType = CT_Stateful;
regression.config.ediCharSet = ECS_XML;
regression.config.newlineCharacter = NLC_None;
regression.config.releaseCharacter = "\\";
regression.config.maxSize = 409600;
regression.config.compareMode = CM_AllWithoutDCX;
regression.config.origin = "";
regression.config.destin = "";
regression.config.useMultiLineRegularExpressions = 1;
regression.config.onTimeout = RTO_Stop;

if global_regression.test_platform == 'PRD':
	regression.config.host = "172.31.21.125";
	regression.config.port = 41101;
elif global_regression.test_platform == 'PDT':	
	regression.config.host = "172.31.21.125";
	regression.config.port = 41101;
elif global_regression.test_platform == 'UAT':	
	regression.config.host = "172.31.21.125";
	regression.config.port = 45101;
elif global_regression.test_platform == 'FRT':
	regression.config.host = global_regression.TICKETACK;
	regression.config.port = 47101;
elif global_regression.test_platform == 'DEV':
	regression.config.host = "172.31.21.247";
	regression.config.port = 15801;
