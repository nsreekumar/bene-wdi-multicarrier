# This configuration sends cryptic scenarios to BT regions through the SI.
# The origin parameter must contain the user's ATID.

#Create the configuration
regression.config = Config();
regression.config.transportHeader = TH_CIL;
regression.config.host = "172.17.204.58";
#172.17.204.58 ALtea 2
#172.17.202.66 ALtea 1
regression.config.port = 10453;
regression.config.timeout = 20.0;
regression.config.tps = 10.0;
regression.config.cps = 0.0;
regression.config.sessionHeader = SH_EDI;
regression.config.conversationType = CT_Stateful;
regression.config.ediCharSet = ECS_IATB;
regression.config.newlineCharacter = NLC_None;
regression.config.releaseCharacter = "\\";
regression.config.maxSize = 409600;
regression.config.compareMode = CM_AllWithoutDCX;
regression.config.origin = "1A0DSPPG3";
regression.config.destin = global_regression.RPISAP;
regression.config.useMultiLineRegularExpressions = 0;
regression.config.onTimeout = RTO_Stop;
regression.config.initContextScript = "";
regression.config.encodeMessageFunction = "";
regression.config.decodeMessageFunction = "";