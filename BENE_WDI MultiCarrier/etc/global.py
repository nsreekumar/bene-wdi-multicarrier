#########################################################
#                                                       #
#               GLOBAL configuration file               #
#                                                       #
#########################################################


# Library Import

import os
import sys
import socket
from time import *
from datetime import *

# Adding the egg for simplejson
sys.path.append(os.path.join(global_regression.TTREG_rootDir,'lib\\simplejson-1.7-py2.4.egg'))
#Import to use Spear
sys.path.append(os.path.join(global_regression.TTREG_rootDir,'lib'))

# Grammer version of the RDP web service used
global_regression.WEBSERVICE_VERSION = "5" 
global_regression.WEBSERVICE_SUB_VERSION = "5.4" 
print 'WEBSERVICE_VERSION '  + global_regression.WEBSERVICE_VERSION
print 'WEBSERVICE_SUB_VERSION '  + global_regression.WEBSERVICE_SUB_VERSION

# Setting of the origin field (by default the ATID)
global_regression.ORIGIN = global_regression.ATID

# Deafult WDI Version (Latest v28)
global_regression.WDI_VERSION = "28"

#Setting of ROUTINGNAME
global_regression.ROUTINGNAME = socket.gethostname()
print global_regression.ROUTINGNAME

# SPEAR save PNR flag (Default: TRUE)
global_regression.save_pnr = "TRUE"




# SIGN configuration
global_regression.SIGN1 = global_regression.SIGN[:4]
print 'SIGN1 ' + global_regression.SIGN1
global_regression.SIGN2 = global_regression.SIGN[4:]
print 'SIGN2 ' + global_regression.SIGN2;



                                                            


if global_regression.test_platform == "PDT":
    global_regression.BACKEND = "BT0PDT"
    global_regression.XMLSI = "1ASIXRDP"
    global_regression.EDIFACTSI = "1ASIRDP"
    global_regression.EDIFACTPF = "1ASRCRDPSSS"
    global_regression.FROMTPF = "1A0DSPPG3"
    global_regression.TICKETACK = "172.31.21.125"
    global_regression.ROUTERPORT = 34350
    global_regression.LogFile = "PDT"
    global_regression.FROM = "1A0TRDP"
    global_regression.SNCFsynchroTarget = "30"
    global_regression.EDIFACTTPFSAP = "1ASRCDSPBF1"
    global_regression.RPISAP = "1ASRCRDPSSS"
    global_regression.OfficeID = "NCE1A0950"
    global_regression.OfficeID2 = "LONU12210"
    global_regression.OfficeIDBKG = "NCE1A0950"
    global_regression.OfficeIDRFD = "NCE1A0950"
    global_regression.OfficeIDEURSTAR = "NCE1A0950"
    global_regression.OfficeIDBENE = "BRU1A7100"
    global_regression.OfficeIDCXL ="LONU12210"
    global_regression.OfficeIDCLM ="LONU12210"
elif global_regression.test_platform == "FRT": 
    global_regression.BACKEND = "BT0FRT"
    global_regression.XMLSI = "1ASIXRDPF"
    global_regression.EDIFACTSI = "1ASIRDPF"
    global_regression.TICKETACK = "172.31.21.26"
    global_regression.ROUTERPORT = 34351
    global_regression.LogFile = "FVT"
    global_regression.FROM = "1A0TRDP"
    global_regression.SNCFsynchroTarget = "33"
    global_regression.OfficeID = "NCE1A0950"
    global_regression.OfficeID2 = "LONU12210"
    global_regression.OfficeIDBKG = "NCE1A0950"
    global_regression.OfficeIDRFD = "NCE1A0950"
    global_regression.OfficeIDEURSTAR = "NCE1A0950"
    global_regression.OfficeIDBENE = "BRU1A7100"
    global_regression.OfficeIDCXL ="LONU12210"
    global_regression.OfficeIDCLM ="LONU12210"
elif global_regression.test_platform == "DEV": 
    global_regression.BACKEND = "BT0DEV"
    global_regression.XMLSI = "1ASIXRDPD"
    global_regression.EDIFACTSI = "1ASIRDPD"
    global_regression.TICKETACK = "172.31.21.125"
    global_regression.ROUTERPORT = 34350
    global_regression.LogFile = "DEV"
    global_regression.FROM = "1A0TRDP"
    global_regression.OfficeIDBKG = "NCE1A0950"
    global_regression.SNCFsynchroTarget = "35"
elif global_regression.test_platform == "SKL":
    global_regression.BACKEND = "BT0SKL"
    global_regression.XMLSI = "1ASIXRDPK"
    global_regression.EDIFACTSI = "1ASIRDPK"
    global_regression.TICKETACK = "TO BE DETERMINED !!!"
    global_regression.LogFile = "SKL"
    global_regression.FROM = "1A0TRDP"
    global_regression.SNCFsynchroTarget = "36"
    global_regression.ROUTERPORT = "TO BE DETERMINED !!!"
elif global_regression.test_platform == "UAT":
    global_regression.BACKEND = "BT0UAT"
    global_regression.XMLSI = "1ASIXRDPU"
    global_regression.EDIFACTSI = "1ASIRDPU"
    global_regression.TICKETACK = "TO BE DETERMINED !!!"
    global_regression.LogFile = "UAT"
    global_regression.FROM = "1A0TRDP"
    global_regression.SNCFsynchroTarget = "32"
    global_regression.OfficeID = "NCE1A0950"
    global_regression.OfficeID2 = "LONU12210"
    global_regression.OfficeIDBKG = "NCE1A0950"
    global_regression.OfficeIDRFD = "NCE1A0950"
    global_regression.OfficeIDEURSTAR = "NCE1A0950"
    global_regression.OfficeIDBENE = "BRU1A7100"
    global_regression.OfficeIDCXL ="LONU12210"
    global_regression.OfficeIDCLM ="LONU12210"
elif global_regression.test_platform == "SKL":
    global_regression.BACKEND = "BT0SKLNA"
    global_regression.XMLSI = "1ASIXRDPK"
    global_regression.EDIFACTSI = "1ASIRDPK"
    global_regression.TICKETACK = "TO BE DETERMINED !!!"
    global_regression.LogFile = "SKL"
    global_regression.FROM = "1A0TRDP"
    global_regression.SNCFsynchroTarget = "32"
elif global_regression.test_platform == "PRD": 
    global_regression.BACKEND = "BT0PRD"
    global_regression.XMLSI = "1ASIXRDPR"
    global_regression.EDIFACTSI = "1ASIRDPR"
    global_regression.TICKETACK = "172.31.21.26"
    global_regression.ROUTERPORT = 62101
    global_regression.LogFile = "PRD"
    global_regression.FROM = "1A0TRDP"
    global_regression.SNCFsynchroTarget = "34"
elif global_regression.test_platform == "QRT": 
    global_regression.BACKEND = "BT0QRT"
    global_regression.XMLSI = "1ASIXSELQ"
    global_regression.EDIFACTSI = "1ASISELQ"
    global_regression.TICKETACK = "172.31.21.25"
    global_regression.ROUTERPORT = 62101
    global_regression.LogFile = "QRT"
    global_regression.FROM = "1A0TRDP"
elif global_regression.test_platform == "MIG":
    global_regression.BACKEND = "8"
    global_regression.XMLSI = "1ASIXRDPM"
    global_regression.EDIFACTSI = "1ASIRDPM"
    global_regression.TICKETACK = "172.31.21.125"
    global_regression.ROUTERPORT = 34350
    global_regression.LogFile = "MIG"
    global_regression.FROM = "1A0TRDP"
    global_regression.SNCFsynchroTarget = "37"
elif global_regression.test_platform == "LOCALPDT":
    global_regression.BACKEND = "BT0PDT"
    global_regression.XMLSI = "1ASIXRDPLOC"
    global_regression.EDIFACTSI = "1ASIRDP"
    global_regression.EDIFACTPF = "1ASRCRDPSSS"
    global_regression.FROMTPF = "1A0DSPPG3"
    global_regression.TICKETACK = "172.31.21.125"
    global_regression.ROUTERPORT = 34350
    global_regression.LogFile = "PDT"
    global_regression.FROM = global_regression.ROUTINGKEY
    global_regression.FSE = "TTR"
    global_regression.SNCFsynchroTarget = "30"



# ------- Automated date definition --------


##################### Today's Date #####################################

TodayDateFormatted = date(gmtime().tm_year,gmtime().tm_mon,gmtime().tm_mday);
print str(TodayDateFormatted);
global_regression.TODAY = str(TodayDateFormatted);
dayDelta = timedelta(days=1);

###################### Outbound Date Definition ########################

# Departure date (defaulted to 5 weeks in advance) 
weekDelta= timedelta(days=7);
OutboundDateFormatted = TodayDateFormatted + weekDelta;
OutboundDatePlusOneFormatted = OutboundDateFormatted + dayDelta;

# Departure date 8 weeks in advance
weekDelta2= timedelta(weeks=8);
OutboundDateFormatted2 = TodayDateFormatted + weekDelta2;

# Departure date 12 weeks in advance
weekDelta3= timedelta(weeks=12);
OutboundDateFormatted3 = TodayDateFormatted + weekDelta3;

# Departure date 16 weeks in advance
weekDelta4= timedelta(weeks=16);
OutboundDateFormatted4 = TodayDateFormatted + weekDelta4;

# Departure date 20 weeks in advance
weekDelta5= timedelta(weeks=20);
OutboundDateFormatted5 = TodayDateFormatted + weekDelta5;

# Departure date 24 weeks in advance
weekDelta6= timedelta(weeks=24);
OutboundDateFormatted6 = TodayDateFormatted + weekDelta6;

# global_regression.DEPARTURE_DATE = "2009-08-01"
str_OutboundDateFormatted = str(OutboundDateFormatted);
global_regression.DEPARTURE_DATE_TIME = str_OutboundDateFormatted + "T08:30:00Z";

# Departure date - 1 month out
global_regression.DEPARTURE_DATE = str(OutboundDateFormatted);
print 'Departure date + 1 month ' + global_regression.DEPARTURE_DATE

# Departure date - 2 months out
global_regression.DEPARTURE_DATE_2MONTHS = str(OutboundDateFormatted2);
print 'Departure date + 2 months ' + global_regression.DEPARTURE_DATE_2MONTHS

# Departure date - 3 months out
global_regression.DEPARTURE_DATE_3MONTHS = str(OutboundDateFormatted3);
print 'Departure date + 3 months ' + global_regression.DEPARTURE_DATE_3MONTHS

# Departure date - 4 months out
global_regression.DEPARTURE_DATE_4MONTHS = str(OutboundDateFormatted4);
print 'Departure date + 4 months ' + global_regression.DEPARTURE_DATE_4MONTHS

# Departure date - 5 months out
global_regression.DEPARTURE_DATE_5MONTHS = str(OutboundDateFormatted5);
print 'Departure date + 5 months ' + global_regression.DEPARTURE_DATE_5MONTHS

# Departure date - 6 months out
global_regression.DEPARTURE_DATE_6MONTHS = str(OutboundDateFormatted6);
print 'Departure date + 6 months ' + global_regression.DEPARTURE_DATE_6MONTHS

# Departure Date - Extended (to be replaced with the above option)
global_regression.DEPARTURE_DATE_EXT = str(OutboundDateFormatted6);
print 'Departure date extended ' + global_regression.DEPARTURE_DATE_EXT

# Departure date - a Day After return date
arrivalDelta = timedelta(days=1);
InwardDatePlusOneFormatted = OutboundDateFormatted + arrivalDelta;
global_regression.DEPARTURE_DATE_PLUS_ONE = str(InwardDatePlusOneFormatted);
print 'Departure date +1 (day after return date) ' + global_regression.DEPARTURE_DATE_PLUS_ONE

# Departure date - a Day After
global_regression.DEPARTURE_DATE_PLUS_ONE_DAY = str(OutboundDatePlusOneFormatted);
print 'Departure date +1  ' + global_regression.DEPARTURE_DATE_PLUS_ONE_DAY

# Departure date - a Week After
global_regression.DEPARTURE_DATE_PLUS_WEEK = str(OutboundDateFormatted2);
print 'Departure date +1 week ' + global_regression.DEPARTURE_DATE_PLUS_WEEK

# Date 1 day before the departure date
TTLDelta = timedelta(days=-1);
TTLDate = OutboundDateFormatted + TTLDelta;

global_regression.DEPARTURE_DATE_MINUS_ONE = str(TTLDate);
print 'Departure date -1 ' + global_regression.DEPARTURE_DATE_MINUS_ONE

# Date Outbound
global_regression.DEPARTURE_DATE2 = str(OutboundDateFormatted2);
print 'departure date ' + global_regression.DEPARTURE_DATE


###################### Inbound Date Definition ########################

# Return date (defaulted to 4 days from default departure date) 
returnDelta = timedelta(days=4);
InwardDateFormatted = OutboundDateFormatted + returnDelta;

# Return date (defaulted to 4 days from the respective departure date) 
InwardDateFormatted2 = OutboundDateFormatted2 + returnDelta;

# Return date (defaulted to 4 days from the respective departure date) 
InwardDateFormatted3 = OutboundDateFormatted3 + returnDelta;

# Return after return is by default 10 days 
returnDelta2 = timedelta(days=10);
SecondInwardDateFormatted = InwardDateFormatted + returnDelta2;

# Arrival Day After
arrivalDelta = timedelta(days=1);
InwardDatePlusOneFormatted = OutboundDateFormatted + arrivalDelta;
InwardArrivalDatePlusOneFormatted = InwardDateFormatted + arrivalDelta;

# Arrival Two Days After
InwardDatePlusTwoFormatted = InwardDatePlusOneFormatted + returnDelta;

# Return dates - Extended
InwardDateExtFormatted = OutboundDateFormatted6 + returnDelta;

#One day before deaprture (usefull for TTL)
TTLDelta = timedelta(days=-1);
TTLDate = OutboundDateFormatted + TTLDelta;

#Tomorrow
Tomorrow = TodayDateFormatted + arrivalDelta;
global_regression.TOMORROW = str(Tomorrow);

# Date Inward
str_InwardDateFormatted = str(InwardDateFormatted);
global_regression.ARRIVAL_DATE_TIME = str_InwardDateFormatted+ "T19:00:00Z";

global_regression.ARRIVAL_DATE = str(InwardDateFormatted);
print 'Arrival date ' + global_regression.ARRIVAL_DATE

global_regression.INWARD_ARRIVAL_DATE_PLUS_ONE = str(InwardArrivalDatePlusOneFormatted);
print 'Arrival date + 1 ' + global_regression.INWARD_ARRIVAL_DATE_PLUS_ONE

# Arrival date - a Day After
global_regression.ARRIVAL_DATE_PLUS_ONE = str(InwardDatePlusTwoFormatted);
print 'Arrival date ' + global_regression.ARRIVAL_DATE_PLUS_ONE

# Arrival date - 2 months out
global_regression.ARRIVAL_DATE_2MONTHS = str(InwardDateFormatted2);
print 'Arrival date + 2 months ' + global_regression.ARRIVAL_DATE_2MONTHS

# Arrival date - 3 months out
global_regression.ARRIVAL_DATE_3MONTHS = str(InwardDateFormatted3);
print 'Arrival date + 3 months ' + global_regression.ARRIVAL_DATE_3MONTHS

# Arrival Date - Extended
global_regression.ARRIVAL_DATE_EXT = str(InwardDateExtFormatted);
print 'Arrival date extended ' + global_regression.ARRIVAL_DATE_EXT

#global_regression.ARRIVAL_DATE = "2009-08-05"
global_regression.ARRIVAL_DATE2 = str(InwardDateFormatted2);
print 'arrival date ' + global_regression.ARRIVAL_DATE2

#global_regression.ARRIVAL_DATE = "2009-08-05"
global_regression.ARRIVAL_DATE1 = str(InwardDateFormatted2);
print 'arrival date ' + global_regression.ARRIVAL_DATE1

# Date Second Trip (For Return_AfterReturn scenario)
# global_regression.SECOND_DATE = "2009-08-15"
global_regression.SECOND_DATE = str(SecondInwardDateFormatted);
print 'Second date ' + global_regression.SECOND_DATE











###################### Other Date Definition ########################

# Saturday in one month
monthDelta = timedelta(days=5);
monthDateFormatted = TodayDateFormatted + monthDelta;
saturdayDelta= timedelta(days=33 - monthDateFormatted.weekday());
SaturdayFormatted = monthDateFormatted + saturdayDelta;
global_regression.DEPARTURE_DATE_SATURDAY = str(SaturdayFormatted);

# WDI date format
global_regression.DEPARTURE_DATE_OUT_WDI  = datetime.strftime(OutboundDateFormatted, "%Y%m%d") 
global_regression.DEPARTURE_DATE_IN_WDI  = datetime.strftime(InwardDateFormatted, "%Y%m%d") 


# Date Mixed PNR
AIRDateFormatted = strftime("%d%b%y", gmtime()).upper()
global_regression.AIR_DATE = AIRDateFormatted[:5]
print 'AIRDate ' + global_regression.AIR_DATE

#Year+Month for Berth
BerthDateFormatted = global_regression.DEPARTURE_DATE[:7]
global_regression.BERTH_DATE = BerthDateFormatted
print 'berth date ' + global_regression.BERTH_DATE


#cryptic date (1 week ahead)
global_regression.DEPARTURE_DATA_CRYTPTIC = datetime.strftime(TodayDateFormatted + timedelta(weeks=1), "%d%b")
print 'departure xxxxxxxxxx ' + global_regression.DEPARTURE_DATA_CRYTPTIC

#booking 6 weeks ahead
weekDelta2= timedelta(weeks=6);
OutboundDateFormatted2 = TodayDateFormatted + weekDelta2;
#return is by default 4 days 
returnDelta2 = timedelta(days=4);
InwardDateFormatted2 = OutboundDateFormatted2 + returnDelta2

#Outbound departure dates
weekDelta= timedelta(weeks=2);
OutboundDateFormatted = TodayDateFormatted + weekDelta;

aDay = OutboundDateFormatted.weekday()
delta = 6 - aDay

if delta == 0:
	global_regression.DATE_SUNDAY = str(OutboundDateFormatted)
	global_regression.DATE_SATURDAY = str(OutboundDateFormatted - timedelta(days=1))
	global_regression.DATE_MONDAY = str(OutboundDateFormatted + timedelta(days=1))
else: 
	global_regression.DATE_SUNDAY = str(OutboundDateFormatted + timedelta(days=delta))
	global_regression.DATE_SATURDAY = str(OutboundDateFormatted + timedelta(days=delta-1))
	global_regression.DATE_MONDAY = str(OutboundDateFormatted + timedelta(days=delta+1))
	
	
ext2WeekDelta=timedelta(weeks=8)
OutboundDateExt2Formatted = TodayDateFormatted + ext2WeekDelta;
global_regression.DEPARTURE_DATE_EXT2 = str(OutboundDateExt2Formatted);
global_regression.ARRIVAL_DATE_PLUS= str(InwardDateFormatted + timedelta(days=1));
